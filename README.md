# README #

Here I try to give a short introdution to git involving the most important commands.

#git pull
get the latest changes from the online repo

#git status
displays any files which have been changed, added or deleted

#git add myfile.txt
adds a file which has been changed, added or deleted to the next commit
git add -A adds everything

#git commit -m "useful comment"
stages all things that have been added using "git add ..." and makes them ready to be pushed online
You can also type 
> git commit -am "my message"
which commits all changed files but adds no new or deleted files to the commit

#git push
pushes the latest commit(s) online

### IMPORTANT ###
Don't mix up this order:
> git pull
> git status
> git add  (x times for x files or using a *.pdf or something like that)
> git status (any time to check what you added etc.)
> git commit -m "type a useful message"
	

All machine-generated files should end up in the .gitignore file, so they are not pushed into the online repo.

