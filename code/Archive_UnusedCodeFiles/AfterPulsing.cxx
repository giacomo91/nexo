#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"

#include "Waveform.h"
#include "PulseFinder.h"
#include "SetupParameter.h"
#include "WfmFile.h"

#include <cmath>
#include <cstdlib>
#include <iostream>

double Func2Exp(double* x, double* par){
  if(x[0]<par[0]) return 0.;
  return par[1]*(1.-exp(-(x[0]-par[0])/par[2]))*exp(-(x[0]-par[0])/par[3]);
    
}

int main(int argc, char** argv){
  //__________________________________________________________________________
  // --- Parameters
  int aRun = 1;
  //int aNEvent=100000000;  
  int aNEvent=1000;  
  const char* aLog = "RunInfo.txt";
  if(argc>1) aRun = atoi(argv[1]);
  if(argc>2) aLog = argv[2];
  if(argc>3) aNEvent = atoi(argv[3]);
  std::cout << "here" << std::endl;  
  SetupParameter* setupPar =  SetupParameter::instance(aLog,aRun);
  std::cout << "here2" << std::endl;  

 //__________________________________________________________________________
  // --- Init pulse finder0
  PulseFinder* pulseFinder = PulseFinder::instance();
  pulseFinder->readResp(setupPar->AvRun, setupPar->AvScale); 
  pulseFinder->setParameters(setupPar->LowFitRange,
			     setupPar->HighFitRange,
			     setupPar->SNMin,
			     setupPar->Chi2OverNDoFMax);

  // pulseFinder->setSimpleParameters(setupPar->SNMin, 5, 2);
  //pulseFinder->setFitParameters(setupPar->LowFitRange,
  //                            setupPar->HighFitRange,
  //                            setupPar->Chi2OverNDoFMax);
  pulseFinder->buildRespInterpolater(setupPar->IntRangeMin,
				     setupPar->IntRangeMax); 
  pulseFinder->setMask(setupPar->MaskMin,setupPar->MaskMax);

  //__________________________________________________________________________
  // --- Open data file
  DataFile* dataFile = new WfmFile(setupPar->getFilename(), setupPar->Run,
      setupPar->FileCount, 1e9, 1000);
  //dataReader.setWFTimeOffset(0,50);
  //dataReader.setWFTimeOffset(1,50);
  //dataReader.setDiffWF(1,0);
  //dataReader.setSecondOrderVernier();
  Waveform* WF = dataFile->getWaveform(0);
  WF->setBaselineLimits(setupPar->BaselineMin,
			setupPar->BaselineMax,
			setupPar->SigMin,
			setupPar->SigMax);
  //WF->setNegPolarity();

  TF1 f2Exp("f2Exp",Func2Exp,0.,5000.,4);
  f2Exp.SetParameters(0.,1.,0.8,23.);
  double scale = f2Exp.Integral(0.,2000.);
  f2Exp.SetParameter(1,1./scale);
  //dataReader.getWaveform()->setFilter(f2Exp);

  int NEvent = dataFile->getWaveformCount();
  if(NEvent>aNEvent) NEvent=aNEvent;

  //__________________________________________________________________________
  // --- Open output file
  char OutFileName[200];
  sprintf(OutFileName,"AP%i.root",aRun);
  TFile OutFile(OutFileName,"RECREATE");
  TNtuple ntp("ntp","","ievt:tPF:hPF:QAfter:tFit:hFit:CL:chi2:ndf:nfit:Pbase:Wbase:noise");

  //__________________________________________________________________________
  // ---
  for(int ti=0; ti<NEvent; ti++){
    std::cout << "ti: " << ti << std::endl;
    if(ti%1000==0) std::cout << ti << std::endl;
    WF = dataFile->getWaveform(ti);
    int NPulse = pulseFinder->findPulse(WF);
    for(int tj=0; tj<NPulse; tj++){
      ntp.Fill(ti,
	       pulseFinder->getPulse(tj).tPF,
	       pulseFinder->getPulse(tj).hPF,
	       pulseFinder->getPulse(tj).qint,
	       pulseFinder->getPulse(tj).tFit,
	       pulseFinder->getPulse(tj).hFit,
	       pulseFinder->getPulse(tj).CL,
	       pulseFinder->getPulse(tj).chi2,
	       pulseFinder->getPulse(tj).NDF,
	       pulseFinder->getPulse(tj).fit,
	       pulseFinder->getPulse(tj).baseline,
	       WF->getMeanBaseline(),
	       WF->getSigmaBaseline()
	       );
    }
  }
  std::cout << "ugh" << std::endl;
  ntp.Write();
  std::cout << "really, it's crashing here???" << std::endl;
  OutFile.Close();
  std::cout << "bob" << std::endl;
  delete dataFile;
  std::cout << "bob2" << std::endl;
}
