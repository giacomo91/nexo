#include "LecroyHdfFile.h"
#include "TFile.h"
#include "Waveform.h"

int main(){
	LecroyHdfFile data("DAQSoftware/LeCrunch/20C_new_405_65-18_200ns_100ps.hdf5");
	TFile fOut("test.root","RECREATE");
	data.getWaveform(1)->Write();
}


/*
	H5File file("DAQSoftware/LeCrunch/20C_new_405_65-18_200ns_100ps.hdf5", 
	H5F_ACC_RDONLY );	
	DataSet dataSet = file.openDataSet("channel1");
	//Attribute attr = dataset.openAttribute("wave_array_count");


	DataSpace dataSpace = dataSet.getSpace();
	//int rank = dataspace.getSimpleExtentNdims();
	hsize_t dataDim[2];
	int nDataDim = dataspace.getSimpleExtentDims(dataDim, NULL);
	hsize_t nEvents = dataDim[0];
	hsize_t nBins = dataDim[1];
	cout << "nEvents= " << nEvents << " | nBins= " << nBins << endl;

	// --- select memory for 1 event
	hsize_t dataOffset[2]; // hyperslab offset in the file
	hsize_t dataCount[2]; // size of the hyperslab in the file
	dataOffset[0] = 0; // event offset
	dataOffset[1] = 0;
	dataCount[0] = 1;
	dataCount[1] = nBins;
	dataspace.selectHyperslab( H5S_SELECT_SET, dataCount, dataOffset );

 // --- create memory space for event
 hsize_t nDim=1;
 hsize_t dimOut[1]; 
 dimOut[0] = nBins;
 DataSpace memspace( nDim, dimOut );
 hsize_t offsetMem[1]; // hyperslab offset in the file
 hsize_t countMem[1]; // size of the hyperslab in the file
 offsetMem[0] = 0;
 countMem[0] = nBins;
 memspace.selectHyperslab( H5S_SELECT_SET, countMem, offsetMem );
 int* data_out = new int[nBins];
 
 dataset.read( data_out, PredType::NATIVE_INT, memspace, dataspace );
 for(int ii=0; ii<nBins; ii++){
	 std::cout << data_out[ii] << " ";
 }
 std::cout << std::endl;

 delete[] data_out;

	DataSpace dataspace = dataset.getSpace();
	H5T_class_t type_class = dataset.getTypeClass();
	IntType intype = dataset.getIntType();


	dataset.read( data_out, PredType::NATIVE_INT, memspace, dataspace );

	std::cout << file.getObjCount() << " " 
						<< dataset.getNumAttrs() <<  " " 
						<< dataset.attrExists("wave_array_count") << " " 
						<< dataspace.getSimpleExtentNpoints() << " " 
						<< type_class << " " 
						<< intype.getSize() << std::endl;
*/

