/*******************************************************************************
* DistributionFinder.cxx
*
* Description:
* Finds pulses in a set of input waveforms and produces a distribution based on
* them
*
* History:
* v0.1  2011/12/14  Initial file (Kyle Boone, kyboone@gmail.com)
*******************************************************************************/

#include <fstream>
#include <iostream>
#include <cmath>

#include "TNtuple.h"
#include "TH1.h"

#include "DistributionFinder.h"
#include "Waveform.h"

// Settings for the distribution finder... these are values that seem to
// work well for all waveforms so they aren't in the SetupParameter file
// yet. That might need to be changed for new devices.
const double DistributionFinder::kPulseThreshold = 0.015;   // V
const double DistributionFinder::kCfdRatio = 0.2;

DistributionFinder::DistributionFinder(double pulseStart, double max1peMin,
    double max1peMax, double pulseSlopeThreshold, double triggerLevel)
  : mPulseStart(pulseStart), mEventCount(0), mWaveform(NULL),
  mMax1peMin(max1peMin), mMax1peMax(max1peMax),
  mPulseSlopeThreshold(pulseSlopeThreshold), mTriggerLevel(triggerLevel),
  mDerivativeWaveform(NULL), mFwhm(0), mFwhmError(0) {
  mRiseTimeWaveform = new TH1F("HRiseTime", "", 200, 0, 20);
  mPeakSlopeWaveform = new TH1F("HPeakSlope", "", 200, 0, 0.1);
  mPeakSlopeSingleWaveform = new TH1F("HPeakSlopeSingle", "", 200, 0, 0.1);
  mTime = new TH1F("HTime","",1000,0.,1000.);

  // TODO: remove qPost, qEarly? Add in qFirst?
  mNtupleAll = new TNtuple("ntpAll", "",
      "ievt:qPre:q:qPost:max:tmax:tth:tF:tcfd:trig:mu:sig:pulses:qEarly:slope");
  mNtuple1pe = new TNtuple("ntp1pe", "",
      "ievt:qPre:q:qPost:max:tmax:tth:tF:tcfd:trig:mu:sig:pulses:qEarly:slope");
}

DistributionFinder::~DistributionFinder() {
  delete mRiseTimeWaveform;
  delete mPeakSlopeWaveform;
  delete mPeakSlopeSingleWaveform;
  delete mTime;
  delete mNtupleAll;
  delete mNtuple1pe;
  if (mWaveform) {
    delete mWaveform;
  }
  if (mDerivativeWaveform) {
    delete mDerivativeWaveform;
  }
}

//----------------------------
//with trigger
void DistributionFinder::processEvent(Waveform* signalWaveform, Waveform*
    triggerWaveform) {
  // Values to calculate
  double prePulseIntegral=-1000, pulseIntegral=-1000, maximumValue=-1000,
         maximumValueTime=-1000, thresholdTime=-1000, cfdTime=-1000,
         triggerTime=-1000, baselineMean=-1000, baselineSigma=-1000,
         maxSlope=-1000;
  int pulseCount=-1000;

  setupEvent(signalWaveform, triggerWaveform);
  triggerTime = processTrigger(signalWaveform, triggerWaveform);
  double pulseStartTrigger = mPulseStart - triggerTime;

  subtractBaseline(mWaveform, pulseStartTrigger, &baselineMean, &baselineSigma);

  maximumValue = mWaveform->GetMaximum();
  maximumValueTime = mWaveform->FindBin(mWaveform->GetMaximumBin());

  int pulseBin = mWaveform->FindBin(pulseStartTrigger);
  prePulseIntegral = calculateIntegral(mWaveform, 1, pulseBin);
  pulseIntegral = calculateIntegral(mWaveform, pulseBin, mWaveform->GetNbinsX());

  thresholdTime = findThresholdTime(mWaveform, pulseStartTrigger,
      kPulseThreshold);
  cfdTime = findCfdTime(mWaveform, pulseStartTrigger);

  countPulses(mWaveform, mPulseSlopeThreshold, &pulseCount, &maxSlope);

  mNtupleAll->Fill(mEventCount, prePulseIntegral, pulseIntegral, 99999,
      maximumValue, maximumValueTime, thresholdTime, 999999, cfdTime,
      triggerTime, baselineMean, baselineSigma, pulseCount, 9999999, maxSlope);
  if (pulseCount == 1 && maximumValue > mMax1peMin && maximumValue < mMax1peMax)
  {
    mNtuple1pe->Fill(mEventCount, prePulseIntegral, pulseIntegral, 99999,
        maximumValue, maximumValueTime, thresholdTime, 999999, cfdTime,
        triggerTime, baselineMean, baselineSigma, pulseCount, 9999999,
        maxSlope);
  }

  if (mEventCount<10) {
    mWaveform->Write();
    mDerivativeWaveform->Write();
    triggerWaveform->Write();
  }
  
  mEventCount++;
}

//without trigger
void DistributionFinder::processEvent(Waveform* signalWaveform, Waveform*
    triggerWaveform) {
  // Values to calculate
  double prePulseIntegral=-1000, pulseIntegral=-1000, maximumValue=-1000,
         maximumValueTime=-1000, thresholdTime=-1000, cfdTime=-1000,
         triggerTime=-1000, baselineMean=-1000, baselineSigma=-1000,
         maxSlope=-1000;
  int pulseCount=-1000;

  setupEvent(signalWaveform, triggerWaveform);
  //triggerTime = processTrigger(signalWaveform, triggerWaveform);
  double pulseStartTrigger = mPulseStart; //- triggerTime;

  subtractBaseline(mWaveform, pulseStartTrigger, &baselineMean, &baselineSigma);

  maximumValue = mWaveform->GetMaximum();
  maximumValueTime = mWaveform->FindBin(mWaveform->GetMaximumBin());

  int pulseBin = mWaveform->FindBin(pulseStartTrigger);
  prePulseIntegral = calculateIntegral(mWaveform, 1, pulseBin);
  pulseIntegral = calculateIntegral(mWaveform, pulseBin, mWaveform->GetNbinsX());

  thresholdTime = findThresholdTime(mWaveform, pulseStartTrigger,
      kPulseThreshold);
  cfdTime = findCfdTime(mWaveform, pulseStartTrigger);

  countPulses(mWaveform, mPulseSlopeThreshold, &pulseCount, &maxSlope);

  mNtupleAll->Fill(mEventCount, prePulseIntegral, pulseIntegral, 99999,
      maximumValue, maximumValueTime, thresholdTime, 999999, cfdTime,
      triggerTime, baselineMean, baselineSigma, pulseCount, 9999999, maxSlope);
  if (pulseCount == 1 && maximumValue > mMax1peMin && maximumValue < mMax1peMax)
  {
    mNtuple1pe->Fill(mEventCount, prePulseIntegral, pulseIntegral, 99999,
        maximumValue, maximumValueTime, thresholdTime, 999999, cfdTime,
        triggerTime, baselineMean, baselineSigma, pulseCount, 9999999,
        maxSlope);
  }

  if (mEventCount<10) {
    mWaveform->Write();
    mDerivativeWaveform->Write();
    //triggerWaveform->Write();
  }
  
  mEventCount++;
}

//----------------------------------

void DistributionFinder::write() {
  mNtupleAll->Write();
  mNtuple1pe->Write();
  mRiseTimeWaveform->Write();
  mPeakSlopeWaveform->Write();
  mPeakSlopeSingleWaveform->Write();
  mTime->Scale(1./mEventCount);
  mTime->Write();
  generateDistributionPlot();
}

//-----------------------------------
//with trigger
void DistributionFinder::setupEvent(Waveform* signalWaveform, Waveform*
    triggerWaveform) {
  if (mWaveform == NULL) {
    mWaveform = new Waveform(signalWaveform->GetNbinsX(),
        signalWaveform->GetXaxis()->GetXmin(),
        signalWaveform->GetXaxis()->GetXmax());
    mDerivativeWaveform = new Waveform(signalWaveform->GetNbinsX(),
        signalWaveform->GetXaxis()->GetXmin(),
        signalWaveform->GetXaxis()->GetXmax());
  }

  mWaveform->Reset("ICE");
  mDerivativeWaveform->Reset("ICE");

  char waveformName[100];
  char derivativeWaveformName[100];
  sprintf(waveformName, "HWF%i", mEventCount);
  sprintf(derivativeWaveformName, "HWFDerivative%i", mEventCount);
  mWaveform->SetName(waveformName);
  mDerivativeWaveform->SetName(derivativeWaveformName);

}

//without trigger
void DistributionFinder::setupEvent(Waveform* signalWaveform) {
  if (mWaveform == NULL) {
    mWaveform = new Waveform(signalWaveform->GetNbinsX(),
        signalWaveform->GetXaxis()->GetXmin(),
        signalWaveform->GetXaxis()->GetXmax());
    mDerivativeWaveform = new Waveform(signalWaveform->GetNbinsX(),
        signalWaveform->GetXaxis()->GetXmin(),
        signalWaveform->GetXaxis()->GetXmax());
  }

  mWaveform->Reset("ICE");
  mDerivativeWaveform->Reset("ICE");

  char waveformName[100];
  char derivativeWaveformName[100];
  sprintf(waveformName, "HWF%i", mEventCount);
  sprintf(derivativeWaveformName, "HWFDerivative%i", mEventCount);
  mWaveform->SetName(waveformName);
  mDerivativeWaveform->SetName(derivativeWaveformName);

}

//--------------------------------------------

double DistributionFinder::processTrigger(Waveform* signalWaveform, Waveform*
    triggerWaveform) {
  // Find the trigger offset. The trigger can be either a positive or negative
  // pulse... we check for both.
  double triggerTime = -1;
  double min = triggerWaveform->GetMinimum();
  double max = triggerWaveform->GetMaximum();
  double triggerLevelSigned = fabs(max) > fabs(min) ?  mTriggerLevel :
    -mTriggerLevel;
  triggerTime = findThresholdTime(triggerWaveform, 0, triggerLevelSigned);

  // Setup mWaveform with the signal offset by the trigger
  mWaveform->GetXaxis()->SetLimits(signalWaveform->GetXaxis()->GetXmin() -
      triggerTime, signalWaveform->GetXaxis()->GetXmax() - triggerTime);
  int binCount = mWaveform->GetNbinsX();
  double* waveformData = mWaveform->getArray();
  double* signalData = signalWaveform->getArray();
  for (int iBin=0; iBin<binCount; iBin++) {
    waveformData[iBin] = signalData[iBin];
    //waveformData[iBin] = -signalData[iBin];
  }

  return triggerTime;
}

double DistributionFinder::exponentialGaussianConvolution(double* x, double* p)
{
  // p[0]: gaussian mu
  // p[1]: amplitude
  // p[2]: gaussian sig
  // p[3]: exponential decay constant
  // convolution of an exponential and a gaussian, formula found in a
  // chromatography paper on exponentially modified gaussians.
  // http://www.springerlink.com/content/yx7554182g164612/
  double time = x[0]-p[0];
  return p[1]*exp(1/2*p[2]*p[2]/p[3]/p[3]-(x[0]-p[0])/p[3])
    * (TMath::Erf(1/sqrt(2)*(p[0]/p[2]+p[2]/p[3]))
        + TMath::Erf(1/sqrt(2)*((x[0]-p[0])/p[2]-p[2]/p[3])));
}

double DistributionFinder::interpolateThresholdCrossing(double x1, double y1,
    double x2, double y2, double threshold) {
  return x2 - (y2-threshold) * (x2-x1) / (y2-y1);
}

double DistributionFinder::findThresholdTime(Waveform* waveform, double start,
    double threshold) {
  int binCount = waveform->GetNbinsX();
  for (int iBin=waveform->GetBinCenter(start); iBin<binCount; iBin++) {
    if ((threshold > 0 && waveform->GetBinContent(iBin) > threshold) ||
        (threshold < 0 && waveform->GetBinContent(iBin) < threshold)) {
      return interpolateThresholdCrossing(waveform->GetBinCenter(iBin-1),
          waveform->GetBinContent(iBin-1), waveform->GetBinCenter(iBin),
          waveform->GetBinContent(iBin), threshold);
    }
  }
  return -1000;
}

double DistributionFinder::findCfdTime(Waveform* waveform, double start) {
  double maximum = waveform->GetMaximum();
  double threshold = maximum * kCfdRatio;
  double cfdTime = findThresholdTime(waveform, start, threshold);

  // Verify that the cfd time was actually for the pulse that has the maximum
  // value. To do that, we check if it is (essentially) monotonic to the
  // maximum.  If not, just discard this event.
  double latestMaximum = 0;
  int binCount = waveform->GetNbinsX();
  for (int iBin = waveform->FindBin(cfdTime); iBin < binCount; iBin++) {
    double binValue = waveform->GetBinContent(iBin);
    if (binValue > 0.99*maximum) {
      return cfdTime;
    } else if (binValue < latestMaximum*0.8) {
      return -1000;
    }
    if (binValue > latestMaximum) {
      latestMaximum = binValue;
    }
  }
}

double DistributionFinder::calculateIntegral(Waveform* waveform, int binMin, int
    binMax) {
  // Assumes that all bins are the same size
  double* array = waveform->getArray();
  double sum = 0;
  for (int i=binMin-1; i<binMax-1; i++) {
    sum += array[i];
  }

  return sum / waveform->GetBinWidth(1);
}

double DistributionFinder::subtractBaseline(Waveform* waveform, double end,
    double* mean, double* sigma) {
  double* array = waveform->getArray();
  int endBin = waveform->FindBin(end)-1;

  // Calculate mean
  double sum = 0;
  for (int i=0; i<endBin; i++) {
    sum += array[i];
  }
  *mean = sum / (endBin - 0);

  // Calculate sigma
  double sumSigma = 0;
  for (int i=0; i<endBin; i++) {
    sumSigma = (array[i] - *mean) * (array[i] - *mean);
  }
  *sigma = sqrt(sumSigma / (endBin - 0));

  int maxBin = waveform->GetNbinsX();
  for (int i=0; i<maxBin; i++) {
    array[i] -= *mean;
  }
}

void DistributionFinder::countPulses(Waveform* waveform, double
    pulseSlopeThreshold, int* pulseCount, double* maxSlope) {
  const int binCount = waveform->GetNbinsX();
  double* derivativeData = mDerivativeWaveform->getArray();
  double* waveformData = waveform->getArray();
  *maxSlope = 0;
  *pulseCount = 0;

  // Generate the derivative waveform. This uses some smoothing to avoid jitter
  // issues.
  for (int iBin=0; iBin<binCount; iBin++) {
    if (iBin >= kDerivativeRange && iBin < binCount-kDerivativeRange) {
      derivativeData[iBin] = ((waveformData[iBin+kDerivativeRange] +
            waveformData[iBin+kDerivativeRange-1]) -
          (waveformData[iBin-kDerivativeRange] +
           waveformData[iBin-kDerivativeRange+1])) / waveform->GetBinWidth(1) /
        kDerivativeRange / 2;
    } else {
      derivativeData[iBin] = 0;
    }
    if (derivativeData[iBin] > *maxSlope) {
      *maxSlope = derivativeData[iBin];
    }
  }

  // Find pulses
  int aboveThreshold = 0;
  float pulseMaxSlope = -100;
  float pulseStart = 0;
  float pulseEnd = 0;
  for (int iBin=0; iBin<binCount; iBin++) {
    if (derivativeData[iBin] > pulseMaxSlope) {
      pulseMaxSlope = derivativeData[iBin];
    }
    if (derivativeData[iBin] > pulseSlopeThreshold) {
      if (!aboveThreshold) {
        // Start of pulse
        pulseMaxSlope = derivativeData[iBin];
        pulseStart = interpolateThresholdCrossing(
            mDerivativeWaveform->GetBinCenter(iBin), derivativeData[iBin-1],
            mDerivativeWaveform->GetBinCenter(iBin+1), derivativeData[iBin],
            pulseSlopeThreshold);
      }
      aboveThreshold++;
    } else {
      if (aboveThreshold > 0) {
        // End of pulse
        pulseEnd = interpolateThresholdCrossing(
            mDerivativeWaveform->GetBinCenter(iBin), derivativeData[iBin-1],
            mDerivativeWaveform->GetBinCenter(iBin+1), derivativeData[iBin],
            pulseSlopeThreshold);
        if (pulseEnd - pulseStart < 1) {
          // Less than 1 ns... probably just noise, not an actual pulse
          aboveThreshold = 0;
        } else {
          (*pulseCount)++;
          aboveThreshold = 0;
	  mTime->Fill(pulseStart);
          mRiseTimeWaveform->Fill(pulseEnd-pulseStart);
          mPeakSlopeWaveform->Fill(pulseMaxSlope);
          if (*pulseCount == 1) {
            mPeakSlopeSingleWaveform->Fill(pulseMaxSlope);
          }
        }
      }
    }
  }
}

void DistributionFinder::generateDistributionPlot() {
  // First pass -> find out exactly where the peak is
  TH1F* HTcfdLarge = new TH1F("HTcfdLarge", "HTcfdLarge", 3000,
      -500, 1000);
  mNtuple1pe->Project("HTcfdLarge", "tcfd");

  // Second pass -> get a nice histogram right around the peak
  double peakLocation = HTcfdLarge->GetBinCenter(HTcfdLarge->GetMaximumBin());
  std::cout << "peak location: " << peakLocation << std::endl;
  TH1F* HTcfd = new TH1F("HTcfd", "HTcfd", 400, round(peakLocation-2),
      round(peakLocation+2));
  mNtuple1pe->Project("HTcfd", "tcfd");
  peakLocation = HTcfd->GetBinCenter(HTcfd->GetMaximumBin());

  // Fit a convolution of an exponential and a gaussian to the tcfd peak
  TF1* FRespTcfd = new TF1("FRespTcfd", exponentialGaussianConvolution,
      round(peakLocation-2), round(peakLocation+2), 4);
  FRespTcfd->SetParameters(peakLocation, mNtuple1pe->GetEntries(), 0.15, 0.10);
  HTcfd->Sumw2();
  HTcfd->Fit(FRespTcfd, "QR");
  HTcfd->Draw();
  HTcfd->Write();

  // Find fwhm -> this could be done a lot better...
  const double resolution = 0.00001;
  double maximum = FRespTcfd->GetMaximum();
  std::cout << "max: " << maximum << std::endl;
  double fwhmStart, fwhmEnd;
  for (double iX=HTcfd->GetBinCenter(1);
      iX<=HTcfd->GetBinCenter(HTcfd->GetNbinsX()); iX+=resolution) {
    if (FRespTcfd->Eval(iX) > maximum/2) {
      fwhmStart = iX;
      break;
    }
  }
  for (double iX=HTcfd->GetBinCenter(HTcfd->GetNbinsX());
      iX>=HTcfd->GetBinCenter(1); iX-=resolution) {
    if (FRespTcfd->Eval(iX) > maximum/2) {
      fwhmEnd = iX;
      break;
    }
  }

  // Save the fwhm results so that they can be retrieved
  mFwhm = 1000 * (fwhmEnd - fwhmStart);
  mFwhmError = 1000 * FRespTcfd->GetParError(2) * (fwhmEnd - fwhmStart) /
    FRespTcfd->GetParameter(2);
  
  // Print some info
  std::cout << "Tcfd fit parameters: " << std::endl;
  std::cout << "gaussian position:          " << FRespTcfd->GetParameter(0) <<
    "\t+/- " << FRespTcfd->GetParError(0) << std::endl;
  std::cout << "gaussian amplitude          " << FRespTcfd->GetParameter(1) <<
    "\t+/- " << FRespTcfd->GetParError(1) << std::endl;
  std::cout << "gaussian width              " << FRespTcfd->GetParameter(2) <<
    "\t+/- " << FRespTcfd->GetParError(2) << std::endl;
  std::cout << "exponential decay constant: " << FRespTcfd->GetParameter(3) <<
    "\t+/- " << FRespTcfd->GetParError(3) << std::endl;
  std::cout << std::endl;
  std::cout << "fwhm:                       " << mFwhm << " \t+/- " <<
    mFwhmError << std::endl;
  std::cout << std::endl << mNtuple1pe->GetEntries() << " events in "
    "distribution after cuts" << std::endl;

  delete HTcfdLarge;
  delete HTcfd;
  delete FRespTcfd;
}

void DistributionFinder::getFwhmResults(double* fwhm, double* fwhmError) {
  *fwhm = mFwhm;
  *fwhmError = mFwhmError;
}
