#include "Pulse.h"
#include "TMath.h"
#include <iostream>
//Bool_t Pulse::IsSortable() const {return kTRUE;}

//Int_t Pulse::Compare(const TObject* obj) const{
//Pulse* tPulse = (Pulse*) obj;
//if(tPulse->tFit>tFit) return -1;
//if(tPulse->tFit==tFit) return 0;
//return 1;
//}

PulseArray::PulseArray(int aInitSize){
  mVect = new Pulse*[aInitSize];
  mIndex = new int[aInitSize];
  mTime = new double[aInitSize];
  mN=0;
  mNMax = aInitSize;
}
PulseArray::~PulseArray(){
  clear();
  delete[] mVect;
  delete[] mIndex;
  delete[] mTime;
}
void PulseArray::add(Pulse* aPulse, int aSort){
  if(mN==mNMax) resize();
  mVect[mN] = aPulse;
  mIndex[mN] = mN;
  mTime[mN] = aPulse->tFit;
  mN++;
  if(aSort) sort();
}

void PulseArray::sort(){
  for(int ti=0; ti<mN; ti++) mTime[ti] = mVect[ti]->tFit;
  TMath::Sort(mN,mTime,mIndex,0);
  //for(int ti=0; ti<mN; ti++){
  //cout << "sort " << mIndex[ti] << " " << mTime[mIndex[ti]] << " " << mTime[ti] << endl;
  //}
}
void PulseArray::clear(){
  for(int ti=0; ti<mN; ti++) delete mVect[ti];
  mN=0;
}
void PulseArray::resize(){
  std::cout << "PulseArray::resize from " << mNMax << " to " << 2*mNMax << std::endl;
  Pulse** tVect = new Pulse*[mN];
  int* tIndex = new int[mN];
  double* tTime = new double[mN];
  for(int ti=0; ti<mN; ti++) {
    tVect[ti] = mVect[ti];
    tIndex[ti] = mIndex[ti];
    tTime[ti] = mTime[ti];
  }
  delete[] mVect;
  delete[] mIndex;
  delete[] mTime;
  mNMax*=2;
  mVect = new Pulse*[mNMax];
  mIndex = new int[mNMax];
  mTime = new double[mNMax];
  for(int ti=0; ti<mN; ti++) {
    mVect[ti] = tVect[ti];
    mIndex[ti] = tIndex[ti];
    mTime[ti] = tTime[ti];
  }
  delete[] tVect;
  delete[] tIndex;
  delete[] tTime;
}
