#ifndef Pulse_h
#define Pulse_h

//#include "TObject.h"
//#include "TList.h"

class Pulse{
 public:
  Pulse(){};
  Pulse(double aBaseline,
	int atPF, double ahPF, double atFit, double ahFit, double aQint, int aFit)
    :baseline(aBaseline),tPF(atPF),hPF(ahPF),qint(aQint),tFit(atFit),hFit(ahFit),fit(aFit),chi2(1e12),CL(-1.),NDF(1)
    {}
  ~Pulse(){};

  bool operator <(const Pulse& aPulse){
    if(tFit<aPulse.tFit) return true;
    return false;
  }
  //  virtual Bool_t IsSortable() const;
  //Int_t Compare(const TObject* obj) const;
  double baseline;
  int tPF;
  double hPF;
  double qint;
  double tFit;
  double hFit;
  double chi2;					  
  double CL;
  double CLPrev;
  int NDF;
  int fit;

};

class PulseArray{
 public:
  PulseArray(int aInitSize=10);
  ~PulseArray();
  void add(Pulse* aPulse, int aSort=0); //-1 means no sorting
  void sort();
  Pulse& operator[](int aIndex){return *(mVect[mIndex[aIndex]]);}
  void clear();
  int size() {return mN;}
 private:
  Pulse** mVect;
  int* mIndex;
  double* mTime;
  int mN;
  int mNMax;
  void resize();

};



#endif
