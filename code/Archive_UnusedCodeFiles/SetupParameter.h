#ifndef SetupParameter_h
#define SetupParameter_h

class SetupParameter{
 public:
  static SetupParameter* instance(){return mPar;}
  static SetupParameter* instance(const char* aFileName, int aRun){
    if(!mPar) mPar = new SetupParameter(aFileName,aRun);
    return mPar;
  }
  
  // --- Run parameters
  int Run;
  int Date;
  char Device[100];
  int Temp;
  int Wlen;
  int Voltage;
  int FNameFormat;
  // 1: /home/kboone/Desktop/mppctiming/data/20110928-654V-60C/820nm000_Ch3.wfm
  char WaveformChannel[100];
  char TriggerChannel[100];
  int FileCount;
  int AvRun;
  // --- For baseline
  double AvScale;
  double BaselineMin;
  double BaselineMax;
  double SigMin;
  double SigMax;
  // --- For Distribution finder
  double Max1peMin;
  double Max1peMax;
  double PulseSlopeThreshold;
  double TriggerLevel;
  // --- For Average calculation
  int IntRangeMin;
  int IntRangeMax;
  double IntMin;
  double IntMax;
  double PTimeMin;
  double PTimeMax;
  // --- For Pulse finder
  int LowFitRange;
  int HighFitRange;
  double SNMin;
  double Chi2OverNDoFMax;
  int MaskMin;
  int MaskMax;

 private:
  // --- internal variables
  char filename[500];

 public:
  SetupParameter(){}
  SetupParameter(const char* aFileName, int aRun);
  const char* getFilename(bool triggerFile=false);
  static SetupParameter* mPar;
};

#endif
