/*******************************************************
* Tektronix WFM File reader
*
* History:
* v1	2011/08/17	Initial support for WFM003 mFiles (Kyle Boone)
* v2	2011/11/25	Integration with DataFile class (Kyle Boone)
*
* TODO:
* add support for WFM001 and WFM002 mFiles
* handle endianness
*********************************************************/

#include "WfmFile.h"
#include "Waveform.h"

#include <cstring>
#include <inttypes.h>
#include <iostream>

// --- WFM file format structs

struct FileInfo {
  uint16_t  magic;
  char      version[8];
  uint8_t   numDigitsInByteCount;
  uint32_t  numBytesToEof;
  uint8_t   numBytesPerPoint;
  uint32_t  byteOffsetToCurveBuffer;
  uint32_t  horizontalZoomScale;
  float     horizontalZoomPosition;
  double    verticalZoomScale;
  float     verticalZoomPosition;
  char      waveformLabel[32];
  uint32_t  numFastFrames;
  uint16_t  waveformHeaderSize;
} __attribute__((__packed__));

struct WaveformHeader {
  uint32_t  setType;
  uint32_t  wfmCnt;
  uint8_t   junk1[24];
  uint32_t  wfmUpdateSpecCount;
  uint32_t  impDimRefCount;
  uint32_t  expDimRefCount;  
  uint32_t  dataType;
  uint8_t   junk2[16];
  uint32_t  curveRefCount;
  uint32_t  numReqFastFrames;
  uint32_t  numAcqFastFrames;
  uint16_t  summaryFastFrame;   //TODO: only for WFM version 2 or higher!
  uint32_t  pixmapDisplayFormat;
  uint64_t  pixmapMaxValue;
} __attribute__((__packed__));


struct ExplicitDimension {
  double    dimScale;
  double    dimOffset;
  uint32_t  size;
  char      units[20];
  double    dimExtentMin;
  double    dimExtentMax;
  double    dimResolution;
  double    dimRefPoint;
  DataFormat  format : 32;
  uint32_t  storageType;
  uint32_t  nValue;
  uint32_t  overRange;
  uint32_t  underRange;
  uint32_t  highRange;
  uint32_t  lowRange;
  double    userScale;
  char      userUnits[20];
  double    userOffset;
  double    pointDensity;   //TODO: u32 if version < 3, only double in v3+
  double    href;
  double    trigDelay;
} __attribute__((__packed__));

struct ImplicitDimension {
  double    dimScale;
  double    dimOffset;
  uint32_t  size;
  char      units[20];
  double    dimExtentMin;
  double    dimExtentMax;
  double    dimResolution;
  double    dimRefPoint;
  uint32_t  spacing;
  double    userScale;
  char      userUnits[20];
  double    userOffset;
  double    pointDensity;   //TODO: u32 if version < 3, only double in v3+
  double    href;
  double    trigDelay;
} __attribute__((__packed__));

struct TimeBase {
  uint32_t  realPointSpacing;
  uint32_t  sweep;
  uint32_t  typeOfBase;
} __attribute__((__packed__));

struct WFMInfo {
  FileInfo            file;
  WaveformHeader      header;
  ExplicitDimension   ed1;
  ExplicitDimension   ed2;
  ImplicitDimension   id1;
  ImplicitDimension   id2;
  TimeBase            tb1;
  TimeBase            tb2;
} __attribute__((__packed__));

struct FrameTimingInfo {
  uint32_t  realPointOffset;
  double    ttOffset;
  double    fracSec;
  int32_t   gmtSec;
} __attribute__((__packed__));

struct FrameCurveInfo {
  uint32_t  stateFlags;
  uint32_t  checksumType;
  uint16_t  checksum;
  uint32_t  prechargeStartOffset;
  uint32_t  dataStartOffset;
  uint32_t  postchargeStartOffset;
  uint32_t  postchargeStopOffset;
  uint32_t  endOfCurveBufferOffset;
} __attribute__((__packed__));

struct Frame {
  FrameTimingInfo   timing;
  FrameCurveInfo    curve;
  uint32_t          baseOffset;
};



// --- class functions

WfmFile::WfmFile(const char* filename, int run, const int fileCount, const char*
    waveformNameFormat, double userScaleX, double userScaleY)
  : DataFile(run), mFileCount(fileCount), mFileWaveformCounts(0), mFiles(NULL),
  mFileInfos(NULL), mFileFrames(NULL), mWaveform(NULL), mBinCount(0),
  mWaveformReadBuffer(NULL), mUserScaleX(userScaleX), mUserScaleY(userScaleY) {

  setWaveformNameFormat(waveformNameFormat);

  // allocate and zero all of the metadata arrays
  mFileWaveformCounts = new int[mFileCount];
  memset(mFileWaveformCounts, 0, mFileCount*sizeof(int));
  mFiles = new std::ifstream*[mFileCount];
  memset(mFiles, 0, mFileCount*sizeof(std::ifstream*));
  mFileInfos = new WFMInfo*[mFileCount];
  memset(mFileInfos, 0, mFileCount*sizeof(WFMInfo*));
  mFileFrames = new Frame*[mFileCount];
  memset(mFileFrames, 0, mFileCount*sizeof(Frame*));

  // open up the mFiles and read the headers
  for (int i=0; i<mFileCount; i++) {
    if (!this->openFile(filename, i)) {
      mGood = false;
      return;
    }
    mWaveformCount += mFileWaveformCounts[i];
  }

  // setup the waveform object based on the first waveform
  if (mFileFrames[0]) {
    double scaleX = mFileInfos[0]->id1.dimScale * mUserScaleX;
    int binCount = (mFileFrames[0][0].curve.postchargeStartOffset -
        mFileFrames[0][0].curve.dataStartOffset) /
      mFileInfos[0]->file.numBytesPerPoint;
    char nameBuffer[200];
    sprintf(nameBuffer, mWaveformNameFormat, 0);
    mWaveform = new Waveform(binCount, 0, binCount * scaleX, nameBuffer);
    int dataSize = getDataFormatSize();
    if (dataSize >= 1) {
      mWaveformReadBuffer = new char[binCount * dataSize];
    } else {
      mGood = false;
      std::cerr << "Unknown data format: " << getDataFormat() << std::endl;
      return;
    }
  }
}

bool WfmFile::openFile(const char* filename, int index) {
  if (mFiles[index] != 0 || mFileInfos[index] != 0 || mFileFrames[index] !=
      0) {
    std::cerr << "File at index: " << index << " already open!" << std::endl;
    return false;
  }
  std::ifstream* file = new std::ifstream;
  WFMInfo* fileInfo = new WFMInfo;
  mFiles[index] = file;
  mFileInfos[index] = fileInfo;

  // read the static headers
  char indexedFilename[1000];
  sprintf(indexedFilename, filename, index);
  file->open(indexedFilename, std::ifstream::in);
  if (!file->is_open()) {
    std::cerr << "Failed to open file: " << indexedFilename << std::endl;
    return false;
  } else {
    //std::cout << "Reading file: " << indexedFilename << std::endl;
  }
  file->read((char*)fileInfo, sizeof(WFMInfo));

  // read the frame headers
  // note: the numFastFrames value stored in the WFM file is actually the
  // number-1 of frames so we need to add 1 here to account for it.
  int numFrames = -1;
  if (fileInfo->file.numFastFrames > 0) {
    printf("Read file #%d, found %d fast frames.\n", index,
        fileInfo->file.numFastFrames+1);
    numFrames = fileInfo->file.numFastFrames + 1;
  } else {
    printf("Read file #%d, found 1 normal frame\n", index);
    numFrames = 1;
  }
  mFileWaveformCounts[index] = numFrames;

  // the way that the frames are stored in the file makes absolutely no sense:
  // start of frames
  // -> first frame timing info
  // -> first frame curve info
  // -> array of timing info for rest of frames
  // -> array of curve info for rest of frames
  Frame* frames = new Frame[numFrames];
  mFileFrames[index] = frames;
  file->read((char*)&frames[0].timing, sizeof(FrameTimingInfo));
  file->read((char*)&frames[0].curve, sizeof(FrameCurveInfo));
  for (int i=1; i<numFrames; i++) {
    file->read((char*)&frames[i].timing, sizeof(FrameTimingInfo));
  }
  for (int i=1; i<numFrames; i++) {
    file->read((char*)&frames[i].curve, sizeof(FrameCurveInfo));
  }
  
  // calculate the base offsets for the data buffers for each frame
  frames[0].baseOffset = fileInfo->file.byteOffsetToCurveBuffer;
  for (int i=1; i<numFrames; i++) {
    frames[i].baseOffset = frames[i-1].baseOffset +
      frames[i-1].curve.endOfCurveBufferOffset;
  }

  return true;
}

WfmFile::~WfmFile() {
  if (mFileWaveformCounts) {
    delete[] mFileWaveformCounts;
  }
  if (mFiles) {
    for (int i=0; i<mFileCount; i++) {
      if (mFiles[i]) {
        delete mFiles[i];
      }
    }
    delete[] mFiles;
  }
  if (mFileInfos) {
    for (int i=0; i<mFileCount; i++) {
      if (mFileInfos[i]) {
        delete mFileInfos[i];
      }
    }
    delete[] mFileInfos;
  }
  if (mFileFrames) {
    for (int i=0; i<mFileCount; i++) {
      if (mFileFrames[i]) {
        delete mFileFrames[i];
      }
    }
    delete[] mFileFrames;
  }
  if (mWaveform) {
    delete mWaveform;
  }
  if (mWaveformReadBuffer) {
    delete mWaveformReadBuffer;
  }
}


DataFormat WfmFile::getDataFormat() {
  if (mFileInfos && mFileInfos[0]) {
    return mFileInfos[0]->ed1.format;
  } else {
    return kFormatInvalid;
  }
}


int WfmFile::getDataFormatSize() {
  switch(getDataFormat()) {
    case kFormatInt16:
      return sizeof(int16_t);
    case kFormatInt32:
      return sizeof(int32_t);
    case kFormatUInt32:
      return sizeof(uint32_t);
    case kFormatUInt64:
      return sizeof(uint64_t);
    case kFormatFloat32:
      return sizeof(float);
    case kFormatFloat64:
      return sizeof(double);
    case kFormatUInt8:
      return sizeof(uint8_t);
    case kFormatInt8:
      return sizeof(int8_t);
    default:
      return -1;
  }
}

void WfmFile::setWaveformNameFormat(const char* format) {
  strncpy(mWaveformNameFormat, format, 100);
}

Waveform* WfmFile::getWaveform(int index) {
  // figure out which file this waveform is in
  int fileIndex = 0;
  int fileWaveformIndex = index;
  if (fileWaveformIndex > mWaveformCount) {
    std::cerr << "WfmFile::getWaveform error. index: " << index <<
      " greater than maximum index: " << mWaveformCount-1 << std::endl;
    return NULL;
  }
  while (fileWaveformIndex > mFileWaveformCounts[fileIndex]) {
    fileWaveformIndex -= mFileWaveformCounts[fileIndex];
    fileIndex++;
  }

  char waveformTitle[200];
  sprintf(waveformTitle, mWaveformNameFormat, index);
  mWaveform->SetName(waveformTitle);
  mWaveform->SetTitle(waveformTitle);

  Frame* frame = &mFileFrames[fileIndex][fileWaveformIndex];
  std::ifstream* file = mFiles[fileIndex];

  int dataSize = getDataFormatSize();
  int binCount = mWaveform->GetNbinsX();
  DataFormat format = getDataFormat();

  file->seekg(frame->baseOffset + frame->curve.dataStartOffset);
  file->read(mWaveformReadBuffer, binCount * dataSize);

  double offset = mFileInfos[fileIndex]->ed1.dimOffset;
  double scale = mFileInfos[fileIndex]->ed1.dimScale;

  double* waveformArray = mWaveform->getArray();
  
  for (int i=0; i<binCount; i++) {
    switch(format) {
      case kFormatInt16:
        waveformArray[i] = mUserScaleY * (offset + scale *
            (double)((int16_t*)mWaveformReadBuffer)[i]);
        break;
      case kFormatInt32:
        waveformArray[i] = mUserScaleY * (offset + scale *
            (double)((int32_t*)mWaveformReadBuffer)[i]);
        break;
      case kFormatUInt32:
        waveformArray[i] = mUserScaleY * (offset + scale *
            (double)((uint32_t*)mWaveformReadBuffer)[i]);
        break;
      case kFormatUInt64:
        waveformArray[i] = mUserScaleY * (offset + scale *
            (double)((uint64_t*)mWaveformReadBuffer)[i]);
        break;
      case kFormatFloat32:
        waveformArray[i] = mUserScaleY * (offset + scale *
            (double)((float*)mWaveformReadBuffer)[i]);
        break;
      case kFormatFloat64:
        waveformArray[i] = mUserScaleY * (offset + scale *
            (double)((double*)mWaveformReadBuffer)[i]);
        break;
      case kFormatUInt8:
        waveformArray[i] = mUserScaleY * (offset + scale *
            (double)((uint8_t*)mWaveformReadBuffer)[i]);
        break;
      case kFormatInt8:
        waveformArray[i] = mUserScaleY * (offset + scale *
            (double)((int8_t*)mWaveformReadBuffer)[i]);
        break;
    }
  }

  return mWaveform;
};

/*
// TODO: REMOVEME: test code
#include "TFile.h"

void testFunc() {
  WfmFile testWfmFile("/home/kboone/Desktop/mppctiming/data/20110928-654V-60C/820nm000_Ch3.wfm", 12);
  std::cout << "Waveform count: " << testWfmFile.getWaveformCount() <<
    std::endl;
  TFile fOut("Test.root", "RECREATE");
  for (int i=0; i<testWfmFile.getWaveformCount(); i+=1000) {
    testWfmFile.getWaveform(i)->Write();
  }
  fOut.Close();
}

int main(void) {
  std::cout << "making wfm reader" << std::endl;
  testFunc();
  std::cout << "deleting wfm reader" << std::endl;
}
*/
