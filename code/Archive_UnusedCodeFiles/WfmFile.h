/*******************************************************
* Tektronix WFM File reader
*
* History:
* v1	2011/08/17	Initial support for WFM003 mFiles (Kyle Boone)
* v2	2011/11/25	Integration with DataFile class (Kyle Boone)
*
* TODO:
* add support for WFM001 and WFM002 mFiles
* handle endianness
*********************************************************/

#ifndef WFM_FILE_H 
#define WFM_FILE_H

#include <fstream>

#include "DataFile.h"

struct WFMInfo;
struct Frame;
class Waveform;

enum DataFormat {
  kFormatInvalid    = -1,
  kFormatInt16      = 0,
  kFormatInt32      = 1,
  kFormatUInt32     = 2,
  kFormatFloat32    = 4,
  kFormatUInt64     = 3,
  kFormatFloat64    = 5,
  kFormatUInt8      = 6,    //TODO: only for WFM version 3 or higher!
  kFormatInt8       = 7,    //TODO: only for WFM version 3 or higher!
};

class WfmFile : public DataFile {
private:
  // file metadata arrays
  // WFM data can be split across multiple mFiles. we handle this internally.
  int mFileCount;
  int* mFileWaveformCounts;
  std::ifstream** mFiles;
  WFMInfo** mFileInfos;
  Frame** mFileFrames;

  // waveform data
  Waveform* mWaveform;
  int mBinCount;
  char* mWaveformReadBuffer;
  double mUserScaleX;
  double mUserScaleY;
  char mWaveformNameFormat[100];

public:
  WfmFile(const char* filename, int run, const int mFileCount=0, const char*
      waveformNameFormat="HWF%i", double userScaleX=1, double userScaleY=1);
  void setWaveformNameFormat(const char* format);
  Waveform* getWaveform(int index);
  ~WfmFile();

private:
  // internal helper functions
  bool openFile(const char* filename, int index);
  DataFormat getDataFormat();
  int getDataFormatSize();
}; 


#endif


