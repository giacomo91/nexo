#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <fstream>

#include "LecroyFile.h"
#include "V1730File.h"
#include "WaveformProcessor.h"
#include "Waveform.h"

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TH1.h"

/*

Analyze and produce the output file for the BeamMap


Execution example ./BeamMap.exe 2383 2406 0 1000

int start_number=2368;
int end_number=2371;
int aChannel=0;
int Event_number=5;

 */

//This code is coupled with the code BeamMap2.exe

using namespace std;

//MAIN
int main(int argc, char** argv){

    
    float X_f,Y_f;
    float step_lenght_x,step_lenght_y;
    float actual_x,actual_y;
    int Number_of_steps_x,Number_of_steps_y;
    
    
    
    double array[20];
    double array2[200];
    char cNum[10] ;
    char cNum2[10] ;
    
    //Dlete files with integral
    system("rm Integral_values.txt");
    
    //11/20/2017 Event_number Qui c'era un errore controllare se funziona ancora con la modifica che ho fatto
    //           non dovrebbe cambiare un cazzo ....
    int start_number = argc>1 ? atoi(argv[1]) : 0;
    int end_number= argc>2 ? atoi(argv[2]) : 5;
    int aChannel=argc>3 ? atoi(argv[3]) : 0;
    int Event_number = argc>4 ? atoi(argv[4]) : 5;
    
    
   //BUILD THE RUN FILE FOR THE ANALYSIS
    
    int aRun;
    int aEvent;
    int progress=0;
    int i;
    char buffer[200];
    
   
    if(end_number<start_number){
        
        cout<<"Wrong input file...end_number<start_number please check it\n"<<endl;
    
        return 0;
    }
    

    //DELETE THE RunInfoBeamMap.txt file if you find it
    system("rm RunInfoBeamMap.txt");
    
    //WRITE FILE FOR ANALYSIS
    ofstream myfile;
    myfile.open ("RunInfoBeamMap.txt");
    myfile <<"Run FileName WFAmpBining ampMin ampMax noise riseTimeSig fallTimeTau Time2Frac fallTime2Tau Pol MinChi2ForRefit"<<endl;
    
    //FOR OVER MIDAS FILES TO ANALYZE THEM
    for(i=start_number;i<=end_number;i++){
        
        
        myfile <<progress<<" /daq/daqstore/exo/nexo/run0"<<i<<"-0000.mid.gz -1 -1 -1 30. 2.52 70.0138 0. 0 -1 3"<<endl;
        
        progress++;
        
    }
    myfile.close();
    
    //CALL SYSTEM TO EXECUTE A PROGRAM IN ORDER TO ANALYZE ONE FILE
    
  for(aRun=0;aRun<progress;aRun++){
    
    cout<<" "<<endl;
    cout<<"Analyzing next file...."<<endl;
    cout<<" "<<endl;
    
    //RUN THE ANALYSIS CONDE
    sprintf(buffer,"./bin/BeamMap2.exe %d %d %d",aRun,Event_number,aChannel);
    
    cout<<buffer<<endl;

    system(buffer);
   
      
         
  }
    
  cout<<""<<endl;
  cout<<"Finished to analyze files ..."<<endl;
  cout<<" "<<std::endl;
  cout<<"*****************************************************"<<std::endl;
  cout<<"2D histogram ..."<<endl;
    
    i=0;
   //IMPORT DATA OF POSITIONS
    ifstream readingfile ("/home/exo/Desktop/tcpsockets-master/Positions.txt", std::ifstream::in);
    if (readingfile.is_open())
    {
        
        while (readingfile.good())
        {
            readingfile.getline(cNum, 256, ',');
            array[i]= atof(cNum) ;
	    // cout<<array[i]<<endl;
            i++ ;
        }
        readingfile.close();
    }
    
    else{
        
      cout << "Unable to open file Positions.txt ..."<<endl;
        
        return 0;
        
    }
    
    
    X_f=array[0];
    Y_f=array[1];
    actual_x=array[2];
    actual_y=array[3];
    step_lenght_x=array[4];
    step_lenght_y=array[5];
    
    //NUMBER_OF_STEP
    Number_of_steps_x=(int)((X_f-actual_x)/step_lenght_x);
    Number_of_steps_y=(int)((Y_f-actual_y)/step_lenght_y);
    
    //cout<<actual_x<<endl;
    //cout<<X_f<<endl;
    //cout<<step_lenght_x<<endl;
    //cout<<Number_of_steps_x<<endl;
    
    int step_x=0;
    int step_y=0;
    
    int number_total_points=Number_of_steps_x*Number_of_steps_y;
    

    if(number_total_points>200){

      cout<<"Allocate more points in array2!"<<endl;
      cout<<"exit ...."<<endl;

      return 0;

    }


    cout<<" "<<std::endl;
     cout<<"Importing Integral value ..."<<std::endl;
   
     //OPEN FILE AND IMPORT INTEGRAL VALUES
    i=0;
    ifstream readingfile2 ("/home/exo/Desktop/Data_ana_alex/Fabrice_code/Integral_values.txt", std::ifstream::in);
    if (readingfile2.is_open())
    {
        
        while (readingfile2.good())
        {
            readingfile2>>array2[i];
            
            if(array2[i]!=0){
            
            //cout<<array2[i]<<endl;
            i++ ;
            
            }else{
                
            cout<<"Skipped zero value!"<<std::endl;
            cout<<""<<std::endl;
            }
        }
        readingfile2.close();
    }
    
    else{
        
        cout << "Unable to open file Integrals.txt ..."<<endl;
        
        return 0;
        
    }
    
    
    cout<<" "<<std::endl;
    cout<<"Building connectivity table"<<std::endl;
    cout<<" "<<std::endl;
    
    TH2D *hXY = new TH2D("hXY","X vs Y, Intensity on Z",100,0,100,100,0,100);
    //ROOT 2D HISTOGRAM HERE
    
    system("rm BeamMap.txt");
    ofstream beammap;
    beammap.open("BeamMap.txt");
    
    i=0;
    //cout<<Number_of_steps_x<<endl;
    for(step_x=0;step_x<=(Number_of_steps_x);step_x++){
        
    cout<<"X: "<<actual_x<<" Y: "<<actual_y<<" Int: "<<array2[i]<<endl;
    beammap<<actual_x<<","<<actual_y<<","<<array2[i]<<endl;

    hXY->Fill(actual_x,actual_y,array2[i]);
    i++;
        
    actual_x=actual_x+step_lenght_x;

    if(step_x==(Number_of_steps_x)){
            
    actual_x=array[2];
    step_x=-1;
            
    //Increment also Y value
    actual_y=actual_y+step_lenght_y;
    step_y++;
            
    }
           
    if(actual_y>Y_f){
    
        break;
    }
        
    }


    beammap.close();
 
    TCanvas *c1 = new TCanvas("c1","c1");
    hXY->Draw("colz");

    
    
    cout<<" "<<std::endl;
    cout<<"Finished beam map 2D!"<<endl;
    cout<<"Execute BM macro to obtain a plot ....!"<<endl;
    
}




