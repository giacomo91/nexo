#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <fstream>

#include "LecroyFile.h"
#include "V1730File.h"
#include "WaveformProcessor.h"
#include "Waveform.h"

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TH1.h"

using namespace std;

double values[2];

//Values to compute integral for photon interval
int PH_sup;
int PH_inf;

//Values to compute integral for Dark Noise
int DN_sup;
int DN_inf;


//DECLARATION
void Integral_computation(int,int,int);

//MAIN
int main(int argc, char** argv){
    
    int aRun = argc>1 ? atoi(argv[1]) : 0;
    int Event_number= argc>2 ? atoi(argv[2]) : 5;
    int aChannel = argc>3 ? atoi(argv[3]) : 0;
    
    //call beam map
    Integral_computation(aRun,Event_number,aChannel);
    

}

//FUNCTION
void Integral_computation(int aRun, int Event_number,int aChannel){
    
  //std::cout<<aRun<<" "<<Event_number<<" "<<aChannel<<std::endl;

    char aRunInfo[100];
    
    int aEvent;

    char FRunInfo[200];

    double Integral=0;
    
    double Integral_DN=0;
    
    //OPEN FILE IN THE DIRECTORY YOU ARE
    const char* aDir="./bin";
    
    //OPEN FILE IN THE DIRECTORY IN WHICH YOU ARE
    sprintf(FRunInfo,"%s/RunInfoBeamMap.txt",aDir);
    cout <<"File "<<FRunInfo <<" loaded!"<<endl;
    
    cout <<"Run Number: "<<aRun<<endl;
    
    strcpy(aRunInfo,"RunInfoBeamMap.txt");
    
    WaveformProcessor wfProc(aRunInfo,aRun,5,aChannel);
    
    for(aEvent=0;aEvent<=Event_number;aEvent++){
        
      // cout <<"Event: "<<aEvent<<endl;
        //wfProc->setFitOption("V");
        wfProc.readWaveform(aEvent);
        
        Waveform *curWF=wfProc.getCurrentWaveform();
        
	//These values should be BIN values so if compared with Anadisplay must be divided by 2
        Integral+= curWF->Integral(2900,3300);
        
        Integral_DN+=curWF->Integral(50,450);
        
     
        
    }
    
   double difference;
    
    values[0]=Integral/Event_number;
    values[1]=Integral_DN/Event_number;
    std::cout<<"PHOTON_COUNTS="<<values[0]<<std::endl;
    std::cout<<"INTEGRAL_DN="<<values[1]<<std::endl;
    
    difference=(values[1]-values[0]);
    std::cout<<"Difference(DN-PH)="<<difference<<endl;
    
    //OPEN A FILE AND WRITE ON IT
    
    std::ofstream ofs;
    ofs.open ("Integral_values.txt", std::ofstream::out | std::ofstream::app);
    
    ofs <<difference<<endl;
    
    ofs.close();
    
    
    
}

