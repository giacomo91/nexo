#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include "DarkNtpAnalysis.h"

int main(int argc, char** argv)
{
	//possible to combine two runs with different timing resolutions
	int aRun = argc>1 ? atoi(argv[1]) : 0;
	int mode = argc>2 ? atoi(argv[2]) : 0;
	double par1 = argc>3 ? atoi(argv[3]) : 0;
	int par2 = argc>4 ? atoi(argv[4]) : 0;

	/* Modes:
		0 - Create Time Distribution with a window of par1, 100 bins, no fit
		1 - Create Time Distribution with a window of par1, 1000 bins, no fit
		2 - Create Normalised Time Distribution with a window of par1, 100 bins, fit
		3 - Create Normalised Time Distribution with a window of par1, 1000 bins, fit
		4 - Fits pulses, not time dist
		
	*/

	if(argc >= 2)
	{
		DarkNtpAnalysis analysis;
		analysis.calculateAllParameters(aRun, mode, par1, par2);
		analysis.writeAll(aRun, mode);
	}
	else
	{
		std::cout << "Usage: Analysis.exe [run#1]. See nEXO/RunInfo.txt for run numbers." << std::endl;
		exit(1);
	}
}

