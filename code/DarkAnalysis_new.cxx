#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include "DarkNtpAnalysis.h"

int main(int argc, char** argv)
{
	//possible to combine two runs with different timing resolutions
	int aRun1 = argc>1 ? atoi(argv[1]) : 0;
	double window = argc>2 ? atoi(argv[2]) : 0;
	int aRun2 = argc>3 ? atoi(argv[3]) : 0;

	if(argc == 3)
	{
		DarkNtpAnalysis* analysis = new DarkNtpAnalysis(aRun1, window);
		analysis.calculateAllParameters();
		analysis.writeAll();
	}
	else if (argc == 4)
	{
		DarkNtpAnalysis* analysis = new DarkNtpAnalysis(aRun1, aRun2, window);
		analysis.calculateAllParameters();
		analysis.writeAll();
	}
	else
	{
		std::cout << "Usage: Analysis.exe [run#1] [windowsize] [run#2]. See nEXO/RunInfo.txt for run numbers." << std::endl;
		exit(1);
	}
}

