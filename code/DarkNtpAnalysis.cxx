/*******************************************************************************
* DarkNtpAnalysis.cxx
*
* Description:
* Analyzes an NTuple for dark noise, afterpulse probability, etc. Also provides
* formatting for histogram generation.
*
*******************************************************************************/

#include <sys/stat.h>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TROOT.h"
#include "TH1.h"
#include "TGraph.h"
#include "TMath.h"
#include "TCanvas.h"

#include "DarkNtpAnalysis.h"
#include "WaveformProcessor.h"
#include "LecroyFile.h"
#include "LecroyHdfFile.h"
#include "Waveform.h"

using namespace std;

DarkNtpAnalysis::DarkNtpAnalysis() //add functionality for arun
{

}

//par[0] = tau, par[1] = p_ap, par[2] = tau_ap
double fitFunction(double *x, double *par)
{
	double arg1 = 0, arg2 = 0, fitval = 0;
	if(par[0] != 0) arg1 = 1/par[0]; // 1/tau
	if(par[2] != 0) arg2 = arg1 + 1/par[2]; // (1/tau + 1/tau_k)
	
	//fitval = par[1]*exp(-1*x[0]*arg2)*arg2 + arg1*exp(-1*x[0]*arg1)*(1-par[1]);

	fitval = par[1]*arg2*exp(-1*x[0]*arg2) + arg1*exp(-1*x[0]*arg1) + par[1]/par[2]*exp(-1*x[0]/par[2]);

	return fitval;
}

//For collection data from a single data file
void DarkNtpAnalysis::processData(int aRun, bool savepulses, double maxTime, int histId)
{
	char* name = getFileName(aRun, "./RunInfo.txt", fittype);
	TFile* f1 = new TFile(name);
	TNtuple* ntp = (TNtuple*) f1->Get("ntp");
	NtpCont* cont = (NtpCont*) ntp->GetArgs();

	int nPulse = ntp->GetEntries();
	int iPulse = 0;
	int numpeaks;
	int numpairs = 0;
	int numevents = 0;
	double peaktimes[10];
	double peakamps[10];
	double time;
	int firstpeak = 0;
	int secondpeak = 0;
	double timediff;
	bool subpeak = false;
	bool xtalk = false;
	bool prepeak = false;

	for(int i = 0; i < 10; i++) xtalkcounts[i] = 0;
	int xtalksample = 0;

	pulseAmplitude = 0;
	pulseRiseTime = 0;
	pulseFallTime = 0;
	pulseChi2 = 0;

	UNCpulseAmplitude = 0;
	UNCpulseRiseTime = 0;
	UNCpulseFallTime = 0;
	UNCpulseChi2 = 0;

	double ampvalues[300000];
	double risevalues[300000];
	double fallvalues[300000];
	double chivalues[300000];

	int numfittedpeaks = 0;
	totalevents = 1000000;

	int numaftertrigger = 0;
	int peakcounts [10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	cout << nPulse << endl;

	while(iPulse<nPulse)
	{

		//Stores data for all peaks in a given event
		ntp->GetEntry(iPulse);

		if(aRun % 10 != 0){
			numpeaks = cont->nfp;
			peaktimes[0] = cont->ft;
			peakamps[0] = cont->fa;
		}
		else{
			numpeaks = cont->np;
			peaktimes[0] = cont->pt;
			peakamps[0] = cont->pa;
		}
		peakcount += numpeaks; //////////
		for(int i=1; i<numpeaks; i++)
		{
			iPulse++;
			if(aRun % 10 != 0){
				ntp->GetEntry(iPulse);
				peaktimes[i] = cont->ft;
				peakamps[i] = cont->fa;
			}
			else{
				ntp->GetEntry(iPulse);
				peaktimes[i] = cont->pt;
				peakamps[i] = cont->pa;
			}
		}

		//Checks for pre-trigger peaks
		for(int i=0; i<numpeaks; i++)
		{
			if((peaktimes[i] < -5e-8) && (peaktimes[i] > -5e-7)) prepeak = true;
			else if (peaktimes[i] > -5e-8) numaftertrigger++;
		}	

		//Finds the first peak of the event after the trigger
		for(int i=1; i<numpeaks; i++)
		{
			if(peaktimes[firstpeak] > -5e-8)
			{
				if((peaktimes[i] >-5e-8) && (peaktimes[i] < peaktimes[firstpeak])) firstpeak = i;
			}
			else firstpeak = i;
		}
		numevents++;

		//Counts cross-talk
		if(numevents % (totalevents/10) == 0) xtalksample++;
		if(peakamps[firstpeak]*polarity > xtalkthresh)
		{
			//cout << "Peak amp: " << peakamps[firstpeak]*polarity << " xtalkthresh: " << xtalkthresh << " CROSSTALK FOUND" << endl;
			numxtalk++;
			xtalkcounts[xtalksample]++;
			xtalk = true;
		}


		if((xtalk == false) &&(prepeak == false))
		{
			//Checks for a subsequent peak
			for(int i=0; i<numpeaks; i++)
			{
				if(peaktimes[i] > peaktimes[firstpeak])
				{
					subpeak = true;
				}
			}
	
			if(subpeak == true)
			{
				//Finds the subsequent peak
				for(int i=1; i<numpeaks; i++)
				{
					if(peaktimes[secondpeak] > peaktimes[firstpeak])
					{
						if((peaktimes[i] > peaktimes[firstpeak]) && (peaktimes[i] < peaktimes[secondpeak])) secondpeak = i;
					}
					else secondpeak = i;
				}

				//Calculates the time difference between the two and puts into a histogram
				timediff = peaktimes[secondpeak] - peaktimes[firstpeak];
				if ((timediff < maxTime) && (histId == 0)) TimeDist->Fill(timediff*1e9);
				else if ((timediff < maxTime) && (histId == 1)) TimeDist1->Fill(timediff*1e9);
				else if ((timediff < maxTime) && (histId == 2)) TimeDist2->Fill(timediff*1e9);

				if ((histId == 1) && (timediff > apulsingmax1) && (timediff < maxTime)) numpairs++;
				else if ((histId == 2) && (timediff > 5e-9) && (timediff < apulsingmax1)) numpairs++;
			}

			else if ((numpeaks == 1) && (willFit == true))
			{

				//Calculated parameters related to the fit shape
				WaveformProcessor* wfProc = new WaveformProcessor("RunInfo.txt",aRun);
				wfProc->setQuietMode(true);
				wfProc->readWaveform(cont->evt);
				wfProc->processBaseline();
				wfProc->findPulse();

				if(wfProc->getNPulse()==1)
				{
					wfProc->setFitOption("QR0");
					wfProc->fitSingle(fittype);
					if((fittype==7)){
						wfProc->reFitMulti();
					}
					if(savepulses == true)
					{
						gROOT->SetBatch();
						TCanvas *c1 = new TCanvas("PulsesFit", "PulsesFit", 900, 700);
						Waveform* wf = wfProc->getCurrentWaveform();
						wf->SetLineColor(1);
						wf->Draw();
						TGraph* GPulse = wfProc->getPulseGraph();
						GPulse->SetMarkerStyle(23);
						GPulse->SetMarkerColor(2);
						GPulse->Draw("p");
					
						wfProc->getFitFunction()->Draw("same");
						if((fittype==8)) wfProc->getFitFunction2()->Draw("same");

						char filename [100];
						sprintf(filename, "/home/deap/nEXO/Pulse Fits 2/%d/Single Pulse/%d-%d.pdf", aRun, cont->evt, fittype);
						c1->SaveAs(filename);

						delete wf;
						delete c1;
						delete GPulse;
					}
					pulseAmplitude = pulseAmplitude * numfittedpeaks + wfProc->getFitAmplitude(0);
					pulseAmplitude /= (numfittedpeaks+1);
					ampvalues [numfittedpeaks] = pulseAmplitude;
					pulseRiseTime = pulseRiseTime * numfittedpeaks + wfProc->getFitRiseTime();
					pulseRiseTime /= (numfittedpeaks+1);
					risevalues [numfittedpeaks] = pulseRiseTime;
					pulseFallTime = pulseFallTime * numfittedpeaks + wfProc->getFitFallTime();
					pulseFallTime /= (numfittedpeaks+1);
					fallvalues [numfittedpeaks] = pulseFallTime;
					pulseChi2 = pulseChi2 * numfittedpeaks + wfProc->getChi2();
					pulseChi2 /= (numfittedpeaks+1);
					chivalues [numfittedpeaks] = pulseChi2;
					numfittedpeaks++;
				}
				delete wfProc;
			}

		}

		if(histId == 1) fracDN = 1.0 * numpairs / numevents;
		else if (histId == 2) fracAPulsing = 1.0 * numpairs / numevents;

		peakcounts[numaftertrigger]++;	

		numaftertrigger = 0;
		firstpeak = 0;
		secondpeak = 0;
		subpeak = false;
		xtalk = false;
		prepeak = false;
		int event_i = cont->evt;
		if(event_i % 100 == 0) cout << "Event " << cont->evt << " processed." << endl;
		iPulse++;
	}

	//Finds uncertainties in parameters related to the fit shape
	if(willFit == true)
	{
		for(int i = 0; i < numfittedpeaks; i++)
		{
			UNCpulseAmplitude += (ampvalues[i]-pulseAmplitude)*(ampvalues[i]-pulseAmplitude);
			UNCpulseRiseTime += (risevalues[i]-pulseRiseTime)*(risevalues[i]-pulseRiseTime);
			UNCpulseFallTime += (fallvalues[i]-pulseFallTime)*(fallvalues[i]-pulseFallTime);
			UNCpulseChi2 += (chivalues[i]-pulseChi2)*(chivalues[i]-pulseChi2);
		}
		UNCpulseAmplitude /= numfittedpeaks;
		UNCpulseAmplitude = sqrt(UNCpulseAmplitude);
		UNCpulseAmplitude /= sqrt(1.0 * numfittedpeaks);

		UNCpulseRiseTime /= numfittedpeaks;
		UNCpulseRiseTime = sqrt(UNCpulseRiseTime);
		UNCpulseRiseTime /= sqrt(1.0 * numfittedpeaks);

		UNCpulseFallTime /= numfittedpeaks;
		UNCpulseFallTime = sqrt(UNCpulseFallTime);
		UNCpulseFallTime /= sqrt(1.0 * numfittedpeaks);

		UNCpulseChi2 /= numfittedpeaks;
		UNCpulseChi2 = sqrt(UNCpulseChi2);
		UNCpulseChi2 /= sqrt(1.0 * numfittedpeaks); 
	}
	else
	{
		pulseAmplitude = 0;
		UNCpulseAmplitude = 0;
		pulseRiseTime = 0;
		UNCpulseRiseTime = 0;
		pulseFallTime = 0;
		UNCpulseFallTime = 0;
		pulseChi2 = 0;
		UNCpulseChi2 = 0;
	}
	std::cout << "Fitted " << numfittedpeaks << " pulses." << std::endl;
	std::cout << "Found " << numpairs << " pairs." << std::endl;
	for(int i = 1; i < 10; i++) cout << i << " peaks = " << peakcounts[i] << endl;
}

//calculate all parameters
void DarkNtpAnalysis::calculateAllParameters(int aRun, int mode, double par1, int par2)
{
	numxtalk = 0;
	peakcount = 0;
	double maxTime;
	apulsingmax1 = 1000e-9;
	apulsingmax2 = 1e-6;

	TCanvas *c1 = new TCanvas("Timing Distribution", "Timing Distribution", 900, 700);
	c1->SetLogx();
	c1->SetLogy();

	if((mode >= 0) && (mode <= 4))
	{
		maxTime = par1;
		fittype = 7;
		if (mode == 4) willFit = true;
		else willFit = false;

		if((mode == 0) || (mode == 2)) nBin = 100;
		else nBin = 1000;

		bool savepulses = false;
		polarity = -1;
		xtalkthresh = 0.02;

		bps = nBin / maxTime * 1e9;
		sprintf(DName,"Timing Distribution %i",aRun);
		TimeDist = new TH1D(DName,DName,nBin,0,maxTime);

		processData(aRun, savepulses, maxTime*1e-9, 0);
		ofstream oFile;
		char dataname[100];
		sprintf(dataname, "./DarkAnalysis/Histogram Data - %i/%i-HistoData.txt", mode, aRun);
		oFile.open(dataname);
		for(int i = 0; i < nBin; i++)
		{
			oFile << TimeDist->GetBinContent(i) << endl;
		}
	}

	else if(mode == 8)
	{
		maxTime = par1;
		cout << maxTime << endl;
		fittype = 7;
		willFit = false;

		bool savepulses = false;
		polarity = -1;
		xtalkthresh = 0.02;

		bps = nBin / maxTime;
		sprintf(DName,"Timing Distribution %i",aRun);

		TimeDist = new TH1D(DName,DName,maxTime,1,maxTime);

		TimeDist1 = new TH1D("DNoise","DNoise",100,1,maxTime); //nspb = maxTime / 100
		processData(aRun, savepulses, maxTime*1e-9, 1);

		TimeDist2 = new TH1D("APulsing","APulsing",100,1,apulsingmax2*1e9);
		processData(par2, savepulses, apulsingmax2, 2);


		double integral1 = TimeDist1->Integral("width");
		double integral2 = TimeDist2->Integral("width");
/*
		for (int i = 1; i < 100; i++)
		{
			TimeDist2->SetBinContent(i, TimeDist2->GetBinContent(i) / integral2 * fracAPulsing);
			TimeDist2->SetBinError(i, TimeDist2->GetBinError(i) / integral2 * fracAPulsing);
		}
		for (int i = 1; i < 100; i++)
		{
			TimeDist1->SetBinContent(i, TimeDist1->GetBinContent(i) / integral1 * fracDN);
			TimeDist1->SetBinError(i, TimeDist1->GetBinError(i) / integral1 * fracDN);
		}
*/
/*
		for (int i = 1; i < TimeDist2->FindBin(apulsingmax1); i++)
		{
			TimeDist->SetBinContent(TimeDist->FindBin(TimeDist2->GetBinCenter(i)), TimeDist2->GetBinContent(i));
			TimeDist->SetBinError(TimeDist->FindBin(TimeDist2->GetBinCenter(i)), TimeDist2->GetBinError(i));
		}
		int counter = 2;
		for (int i = 3 * maxTime / 200; i <= maxTime; i += maxTime / 100)
		{
			TimeDist->SetBinContent(i, TimeDist1->GetBinContent(counter));
			TimeDist->SetBinError(i, TimeDist1->GetBinError(counter));
			if (TimeDist->GetBinContent(counter) > 0.1) cout << "Error: " << counter << " " << TimeDist->GetBinContent(counter) << endl;
			counter++;
		}*/

		for (int i = 1; i < TimeDist2->GetXaxis()->GetNbins(); i++)
		{
			TimeDist->SetBinContent(TimeDist->FindBin(TimeDist2->GetBinCenter(i)), TimeDist2->GetBinContent(i));
			TimeDist->SetBinError(TimeDist->FindBin(TimeDist2->GetBinCenter(i)), TimeDist2->GetBinError(i));
		}
		double endheight = TimeDist2->GetBinContent(TimeDist2->GetXaxis()->GetNbins() - 1);
		double startheight = TimeDist1->GetBinContent(2);
		double heightratio = endheight / startheight;
		for (int i = 1; i < TimeDist1->GetXaxis()->GetNbins(); i++)
		{
			TimeDist->SetBinContent(TimeDist->FindBin(TimeDist1->GetBinCenter(i)), TimeDist1->GetBinContent(i) * heightratio);
			TimeDist->SetBinError(TimeDist->FindBin(TimeDist1->GetBinCenter(i)), TimeDist1->GetBinError(i) * heightratio);
		}
/*
		ofstream oFile;
		char dataname[100];
		sprintf(dataname, "./DarkAnalysis/Histogram Data - %i/%i-HistoData.txt", mode, aRun);
		oFile.open(dataname);
		for(int i = 0; i < nBin; i++)
		{
			oFile << TimeDist->GetBinContent(i) << endl;
		}
*/
	}
	//TimeDist->SetMaximum(1e-6);

	TimeDist1->Draw("EP");
	TimeDist2->Draw("EP same");
	TimeDist->GetXaxis()->SetTitle("Time Distribution(ns)");
	TimeDist->GetYaxis()->SetTitle("Counts");
	TimeDist->GetXaxis()->CenterTitle();
	TimeDist->GetYaxis()->CenterTitle();

	if ((mode == 2) || (mode == 3) || (mode == 8))
	{

		double integral = TimeDist->Integral("width");
		cout << "Integral: " << integral << endl;
		for (int i = 0; i <= TimeDist->GetXaxis()->GetNbins(); i++){
			TimeDist->SetBinContent(i, TimeDist->GetBinContent(i) / integral);
			TimeDist->SetBinError(i, TimeDist->GetBinError(i) / integral);
		}

		TF1* f1 = new TF1("fit", fitFunction, 0, maxTime, 3);

		f1->SetParameter(0, 1000);
		f1->SetParLimits(0, 0, 20000);
		f1->FixParameter(0, 100);

		f1->SetParameter(1, 0.5);
		f1->SetParLimits(1, 0, 1);
		f1->FixParameter(1, .1);

		f1->SetParameter(2, 1000);
		f1->SetParLimits(2, 0, 20000);
		f1->FixParameter(2, .1);

		TimeDist->Fit("fit", "", "");
		f1->Draw("same");

		double error;
		double percerror;

		double Tau = f1->GetParameter(0);
		darkNoiseRate = 1/Tau;
		cout << "DN Rate: " << darkNoiseRate << endl;
		error = f1->GetParError(0);
		percerror = error / Tau;
		UNCdarkNoiseRate = percerror * darkNoiseRate;


		double p_ap = f1->GetParameter(1);
		avgNumAfterpulse = 1/(1-p_ap);
		cout << "Apulse: " << avgNumAfterpulse << endl;
		error = f1->GetParError(1);
		percerror = error / (1-p_ap);
		UNCavgNumAfterpulse = percerror * avgNumAfterpulse;

		crossTalkProbability = 1.0 * numxtalk / totalevents;
		//EVALUATE
		UNCcrossTalkProbability = 0;
		for(int i = 0; i < 10; i++) UNCcrossTalkProbability += (10.0*xtalkcounts[i]/totalevents - crossTalkProbability) * (10.0*xtalkcounts[i]/totalevents - crossTalkProbability);
		UNCcrossTalkProbability /= 10;
		UNCcrossTalkProbability = sqrt(UNCcrossTalkProbability);
		UNCcrossTalkProbability /= sqrt(10.0);
		cout << "Peak count: " << peakcount << endl;
	}

	char savename[100];
	if (mode == 8)
	{
		sprintf(savename, "./DarkAnalysis/Combined Distros/%i-%i_v21.pdf", aRun, par2);
	}
	else sprintf(savename, "./DarkAnalysis/Single Distros/%i-%i.pdf", aRun, mode);
	c1->SaveAs(savename);

}

//Make plots. If true, save and close plots
void DarkNtpAnalysis::makePlots(bool savePlots, int aRun) 
{
	TCanvas* c1 = new TCanvas("c1", "Time Distribution", 900, 600);
	c1->SetLogy();
	c1->SetLogx();

	TimeDist->Draw();
	char savename [100];
	sprintf(savename, "Time Distribution %i.pdf", aRun);

	if(savePlots == true) c1->SaveAs(savename);
}

//write all parameters to a text file
void DarkNtpAnalysis::writeAll(int run, int mode)
{
	char savename[100];
	if(willFit == true) sprintf(savename, "./DarkAnalysis/Parameters - %i/%i-DarkAnalysis.txt", mode, run);
	else sprintf(savename, "./DarkAnalysis/Parameters - %i/%i-DarkAnalysis-NoFit.txt", mode, run);
	ofstream oFile;
	oFile.open(savename);
	oFile << "DN_Rate\tAvgNumAfterpulse\tCrosstalkProb\tPulseAmp\tPulseRiseTime\tPulseFallTime\tPulseChi2\n";
	oFile << darkNoiseRate << " +- " << UNCdarkNoiseRate << "\t";
	oFile << avgNumAfterpulse << " +- " << UNCavgNumAfterpulse << "\t";
	oFile << crossTalkProbability << " +- " << UNCcrossTalkProbability << "\t";
	oFile << pulseAmplitude << " +- " << UNCpulseAmplitude << "\t";
	oFile << pulseRiseTime << " +- " << UNCpulseRiseTime << "\t";
	oFile << pulseFallTime << " +- " << UNCpulseFallTime << "\t";
	oFile << pulseChi2 << " +- " << UNCpulseChi2;
	oFile.close();
}

//Opens run data file of type .anat(#) where # = aTraining
char* DarkNtpAnalysis::getFileName(int run, char* aLog, int aTraining=0)
{
	char FileNames [100] [2] [100];
	char* activename = new char[1024];
	ifstream iFile;
	iFile.open(aLog);
	int counter = 0;
	char buffer [10000];
	int runnum;
	char* newbuff;
	for (int i = 0; i < 10; i++){
		iFile >> buffer;
	}
	while(!iFile.eof()){
		iFile >> FileNames[counter][0];
		iFile >> FileNames[counter][1];
		sscanf(FileNames[counter][0], "%d", &runnum);
		if((runnum == run)){
			newbuff = FileNames[counter][1];
			sprintf(activename, "%s.anat%i", newbuff, aTraining);	
			return activename;
		}
		for (int i = 0; i < 8; i++){
			iFile >> buffer;
		}
	}
	std::cout << "File not found" << std::endl;
}
