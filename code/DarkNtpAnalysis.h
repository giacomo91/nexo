#ifndef DarkNtpAnalysis_h
#define DarkNtpAnalysis_h

class TF1;
class TH1F;
class TH1D;

class DarkNtpAnalysis{

 public:
	//constructors
	DarkNtpAnalysis();

	void calculateAllParameters(int aRun, int mode, double par1, int par2);
	void makePlots(bool savePlots, int aRun);
	void writeAll(int run, int mode);
	
	double getAvgNumAfterpulse(){return avgNumAfterpulse;}
	double getDarkNoiseRate(){return darkNoiseRate;}
	double getCrossTalkProbability(){return crossTalkProbability;}
	double getPulseAmplitude(){return pulseAmplitude;}
	double getPulseRiseTime(){return pulseRiseTime;}
	double getPulseFallTime(){return pulseFallTime;}
	double getPulseChi2(){return pulseChi2;}

	//Uncertainties
	double getAvgNumAfterpulseUncertainty(){return UNCavgNumAfterpulse;}
	double getDarkNoiseRateUncertainty(){return UNCdarkNoiseRate;}
	double getCrossTalkProbabilityUncertainty(){return UNCcrossTalkProbability;}
	double getPulseAmplitudeUncertainty(){return UNCpulseAmplitude;}
	double getPulseRiseTimeUncertainty(){return UNCpulseRiseTime;}
	double getPulseFallTimeUncertainty(){return UNCpulseFallTime;}
	double getPulseChi2Uncertainty(){return UNCpulseChi2;}

 private: 
	double avgNumAfterpulse;
	double darkNoiseRate;
	double crossTalkProbability;
	double pulseAmplitude;
	double pulseRiseTime;
	double pulseFallTime;
	double pulseChi2;

	double UNCavgNumAfterpulse;
	double UNCdarkNoiseRate;
	double UNCcrossTalkProbability;
	double UNCpulseAmplitude;
	double UNCpulseRiseTime;
	double UNCpulseFallTime;
	double UNCpulseChi2;

	double bps;
	bool willFit;
	int polarity;
	int nBin;
	char DName[50];
	double xtalkthresh;
	int numxtalk;
	int peakcount;
	double fittype;
	int totalevents;
	int xtalkcounts[10];

	double apulsingmax1;
	double apulsingmax2;

	double fracAPulsing;
	double fracDN;

	char* getFileName(int run, char* aLog, int aTraining);
	void processData(int aRun, bool savepulses, double maxTime, int histId);

	TH1D* TimeDist; //hist 0
	TH1D* TimeDist1; 
	TH1D* TimeDist2;

	//may need to look up syntax for struct in class header.
	struct NtpCont{
		float evt; //event number
		float blmu; //baseline mean for waveform
		float np; //number of pulses in an event
		float pa; //pulse amplitude
		float pt; //pulse time
		float nfp; //number of fitted pulses in an event
		float fa; //fit amplitude
		float ft; //fit time
		float frt; //fit rise time
		float fft; //fit fall time
		float fblmu; //fit baseline mean
		float chi2; //chi2
		float ndf; //number degrees of freedom
		float chi2r; //chi2 for refit
		float ndfr; //number degrees of freedom for refit
	};
};

#endif



