#ifndef DarkNtpAnalysis_h
#define DarkNtpAnalysis_h

class TF1;
class TH1F;
class TH1D;

class DarkNtpAnalysis{

 public:
	//constructors
	DarkNtpAnalysis(int run1, double windowsize);
	DarkNtpAnalysis(int run1, int run2, double windowsize);
	void calculateAllParameters(int aRun, int mode, double par1, int par2);
	void makePlots(bool savePlots, int aRun);
	void writeAll(int run, int mode);
	
	double getAvgNumAfterpulse(){return avgNumAfterpulse;}
	double getDarkNoiseRate(){return darkNoiseRate;}
	double getCrossTalkProbability(){return crossTalkProbability;}
	double getPulseAmplitude(){return pulseAmplitude;}
	double getPulseRiseTime(){return pulseRiseTime;}
	double getPulseFallTime(){return pulseFallTime;}
	double getPulseChi2(){return pulseChi2;}

	//Uncertainties
	double getAvgNumAfterpulseUncertainty(){return UNCavgNumAfterpulse;}
	double getDarkNoiseRateUncertainty(){return UNCdarkNoiseRate;}
	double getCrossTalkProbabilityUncertainty(){return UNCcrossTalkProbability;}
	double getPulseAmplitudeUncertainty(){return UNCpulseAmplitude;}
	double getPulseRiseTimeUncertainty(){return UNCpulseRiseTime;}
	double getPulseFallTimeUncertainty(){return UNCpulseFallTime;}
	double getPulseChi2Uncertainty(){return UNCpulseChi2;}

 private: 
	int nDatasets;
	int aRun1;
	int aRun2;
	double maxTime;
	bool useFittedPeaks;
	bool fitPulses;
	bool savePulseFits;
	double triggerMinTime;
	double prePulseThreshold;
	int xTalkCounts[10];
	int nEvent;
	int polarity;
	double xTalkThreshold;
	int fitType;

	double avgNumAfterpulse;
	double darkNoiseRate;
	double crossTalkProbability;
	double pulseAmplitude;
	double pulseRiseTime;
	double pulseFallTime;
	double pulseChi2;

	double UNCavgNumAfterpulse;
	double UNCdarkNoiseRate;
	double UNCcrossTalkProbability;
	double UNCpulseAmplitude;
	double UNCpulseRiseTime;
	double UNCpulseFallTime;
	double UNCpulseChi2;

	void init(int run1, int run2, double windowsize);
	char* getFileName(int run, char* aLog, int aTraining);
	void processData(int dataSet);

	TH1D* TimeDist;
	TH1D* TimeDist1; 
	TH1D* TimeDist2;
	TF1* HistFit;

	//may need to look up syntax for struct in class header.
	struct NtpCont{
		float evt; //event number
		float blmu; //baseline mean for waveform
		float np; //number of pulses in an event
		float pa; //pulse amplitude
		float pt; //pulse time
		float nfp; //number of fitted pulses in an event
		float fa; //fit amplitude
		float ft; //fit time
		float frt; //fit rise time
		float fft; //fit fall time
		float fblmu; //fit baseline mean
		float chi2; //chi2
		float ndf; //number degrees of freedom
		float chi2r; //chi2 for refit
		float ndfr; //number degrees of freedom for refit
	};
};

#endif



