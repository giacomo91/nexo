

/*
 
Faccio Programma che fa esattamente la stessa roba del Pulse finding
 ma in più alla fine produce una ntupla con all'interno la distribuzione dei pulses
 
 
 */



#include <sys/stat.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

#include "LecroyFile.h"
#include "V1730File.h"
#include "WaveformProcessor.h"
#include "Waveform.h"

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TH1.h"

// Parameters

int main(int argc, char** argv){
    int aRun = argc>1 ? atoi(argv[1]) : 0;
    int aFitType = argc>2 ? atoi(argv[2]) : 0;
    int aNEventMax = argc>3 ? atoi(argv[3]) : 100000000;
    int aChannel = argc>4 ? atoi(argv[4]) : 1;
    char aRunInfo[100];
    if(argc>5) sprintf(aRunInfo,"ntp/%s/RunInfo.txt",argv[5],argv[5]);
    else strcpy(aRunInfo,"RunInfo.txt");
    
    //Time distribution Analysis
    int number_pulse;
    float **pulses_values;
    float time;
    
    WaveformProcessor wfProc(aRunInfo,aRun,5,aChannel);
    
    // --- Open output file
    //>>> Strip directory name from file name
    int slashIndex=0;
    for(int index=0; index<strlen(wfProc.getFileName()); index++){
        if(strncmp(wfProc.getFileName()+index,"/",1)==0) slashIndex=index;
    }
    char outFileName[200];
    if(argc>5){
        sprintf(outFileName,"ntp/%s/%s.Ch%iFit%i",argv[5],wfProc.getFileName()+slashIndex,wfProc.getChannel(), aFitType);
    }
    else{
        sprintf(outFileName,"ntp/%s.Ch%iFit%i",wfProc.getFileName()+slashIndex,wfProc.getChannel(), aFitType);
    }
    //sprintf(outFileName,"/home/huth/Desktop/nEXO/testdata%s.fanat%i",wfProc.getFileName()+slashIndex,aFitType);
    std::cout << outFileName <<  " " << wfProc.getBaselineRMS() << slashIndex << std::endl;
    
    TFile outFile(outFileName ,"RECREATE");
    TNtuple ntp("ntp","ntp",
                "evt:tt:blmu:blRMS:np:pbl:paa:pa:pt:pq:pw:tchi2:fa:ft:frt:fft:ff2:fft2:fblmu:fchi2:ndf:frchi2:frndf");
    TNtuple ntpE("ntpE","ntpE","evt:tt:blmu:blRMS:Amin:tmin:Amax:tmax:np:qL:qD");
    TNtuple ntptime("ntptime","ntptime","evt:td");
    float ntpCont[100];
    int skippedcount = 0;
    // ---
    int nEvent = wfProc.getWaveformCount();
    if(nEvent>aNEventMax || nEvent==-1) nEvent=aNEventMax;
    int iEvent=0;
    while(iEvent<nEvent && wfProc.readNextWaveform()){
        //for(int iEvent=0; iEvent<nEvent; iEvent++){
        //wfProc.readNextWaveform();
        
      std::cout<<"Ievent"<<iEvent<<std::endl;
       iEvent++;
        int skipFit=0;
        if(wfProc.processBaseline() && wfProc.findPulse()){
            wfProc.fit(aFitType);
        }
        else{
            skippedcount++;
            skipFit=1;
        }
        Waveform* curWF = wfProc.getCurrentWaveform();
        ntpE.Fill(iEvent,wfProc.getTriggerTime(),
                  wfProc.getBaselineMu(),wfProc.getBaselineRMS(),
                  curWF->GetMinimum(),curWF->GetBinLowEdge(curWF->GetMinimumBin()),
                  curWF->GetMaximum(),curWF->GetBinLowEdge(curWF->GetMaximumBin()),
                  wfProc.getNPulse(),
                  curWF->Integral(2400,2700),curWF->Integral(1000,1300));
        //std::cout << "Event: " << iEvent <<"\t nPulses: "
        //          << wfProc.getNPulse() << std::endl;
        
        
        //Trovo numero di Pulses
        number_pulse=wfProc.getNPulse();
        
        //Alloco un array del tipo pulses[Number_of_pulses][Information_per_pulses]
        //Se il numero di pulses è diverso da zero alloro array bidimensionale
        if(number_pulse!=0){
            
            //allocate memory for rows
            pulses_values = (float**)malloc(number_pulse *sizeof(float*)); //for each row allocate memory for columns
            for(int i=0; i<number_pulse; i++)
            {
                *(pulses_values+i) = (float*)malloc(23 *sizeof(float));
            }
            
            
        }
        
        for(int iPulse=0; iPulse<wfProc.getNPulse(); iPulse++){
            
            //Salvo informazioni nell'ntupla
            ntpCont[0]=iEvent;
            ntpCont[1]=wfProc.getTriggerTime();
            ntpCont[2]=wfProc.getBaselineMu();
            ntpCont[3]=wfProc.getBaselineRMS();
            ntpCont[4]=wfProc.getNPulse();
            ntpCont[5]=wfProc.getPulseBaseline(iPulse);
            ntpCont[6]=wfProc.getPulseAbsAmplitude(iPulse);
            ntpCont[7]=wfProc.getPulseAmplitude(iPulse);
            ntpCont[8]=wfProc.getPulseTime(iPulse);
            ntpCont[9]=wfProc.getPulseCharge(iPulse);
            ntpCont[10]=wfProc.getPulseWidth(iPulse);
            ntpCont[11]=wfProc.getSPTemplateChi2(iPulse);
            ntpCont[12]=wfProc.getFitAmplitude(iPulse);
            ntpCont[13]=wfProc.getFitTime(iPulse);
            ntpCont[14]=wfProc.getFitRiseTime(iPulse);
            ntpCont[15]=wfProc.getFitFallTime(iPulse);
            ntpCont[16]=wfProc.getFitTime2Frac(iPulse);
            ntpCont[17]=wfProc.getFitFallTime2(iPulse);
            ntpCont[18]=wfProc.getFitBaseline(iPulse);
            ntpCont[19]=wfProc.getChi2(iPulse);
            ntpCont[20]=skipFit? -1 : wfProc.getNDF(iPulse);
            ntpCont[21]=wfProc.getChi2Refit(iPulse);
            ntpCont[22]=wfProc.getNDFRefit(iPulse);
            ntp.Fill(ntpCont);
            
            //Salvo informazione nell'array doppio
        
        if(number_pulse!=0){
            
            pulses_values[iPulse][0]=iEvent;
            pulses_values[iPulse][1]=wfProc.getTriggerTime();
            pulses_values[iPulse][2]=wfProc.getBaselineMu();
            pulses_values[iPulse][3]=wfProc.getBaselineRMS();
            pulses_values[iPulse][4]=wfProc.getNPulse();
            pulses_values[iPulse][5]=wfProc.getPulseBaseline(iPulse);
            pulses_values[iPulse][6]=wfProc.getPulseAbsAmplitude(iPulse);
            pulses_values[iPulse][7]=wfProc.getPulseAmplitude(iPulse);
            pulses_values[iPulse][8]=wfProc.getPulseTime(iPulse);
            pulses_values[iPulse][9]=wfProc.getPulseCharge(iPulse);
            pulses_values[iPulse][10]=wfProc.getPulseWidth(iPulse);
            pulses_values[iPulse][11]=wfProc.getSPTemplateChi2(iPulse);
            pulses_values[iPulse][12]=wfProc.getFitAmplitude(iPulse);
            pulses_values[iPulse][13]=wfProc.getFitTime(iPulse);
            pulses_values[iPulse][14]=wfProc.getFitRiseTime(iPulse);
            pulses_values[iPulse][15]=wfProc.getFitFallTime(iPulse);
            pulses_values[iPulse][16]=wfProc.getFitTime2Frac(iPulse);
            pulses_values[iPulse][17]=wfProc.getFitFallTime2(iPulse);
            pulses_values[iPulse][18]=wfProc.getFitBaseline(iPulse);
            pulses_values[iPulse][19]=wfProc.getChi2(iPulse);
            pulses_values[iPulse][20]=skipFit? -1 : wfProc.getNDF(iPulse);
            pulses_values[iPulse][21]=wfProc.getChi2Refit(iPulse);
            pulses_values[iPulse][22]=wfProc.getNDFRefit(iPulse);
        }
            
            
            
        }
        
        if(iEvent==0) wfProc.getBaselineHisto()->Write();
        
        if(floor(iEvent*200./nEvent)==(iEvent*200./nEvent)){
            std::cout << iEvent << "/" << nEvent << std::endl; //"\r";
            //std::cout.flush();
        }
        
        //Ora l'array con le informazioni sui Pulses è pieno
        //Se il numero di pulse nella Waveform è inferiore a 2 i.e. 0 o 1 non fare un
        //cazzo
    
    if(number_pulse>1){
        
        
        //Da aggiungere:
        //1) Condizione sulla soglia: Se il primo pulse è inferiore al valore di soglia che
        //hai messo significa che sta nel pretrigger di un pulse primario.
        //In realtà qui devi fare un while fino a quando non trovi un pulse overthreshold, che è quello
        //che per l'appunto ti ha fatto scattare il trigger
        //
        //
        //
        
    
    //Calcolo la differenza in tempo tra i due pulses
              time=pulses_values[1][8]-pulses_values[0][8];
	      std::cout<<"Event: "<<iEvent<<" Time difference: "<<time<<std::endl;

        ntptime.Fill(iEvent,time);
        
    }
        
        
       
    //Prima di cabiare evendo cancello arrey doppio evento precedente
        
        //if(iEvent>195000) std::cout << iEvent << std::endl;
    }
    
    std::cout << "Writing output in " << outFileName << std::endl;
    std::cout << "Skipped " << skippedcount << " triggers." << std::endl;
    ntpE.Write();
    ntp.Write();
    ntptime.Write();
}

