/*******************************************************

*
0 1000 0
*********************************************************/
#include <cstring>
#include <inttypes.h>
#include <iostream>
//#include <typeinfo>

#include "TV1730RawData.hxx"
#include "V1730File.h"
#include "Waveform.h"

#include "TFile.h"
#include "TKey.h"
#include "TROOT.h"
#include "TList.h"

#include "TMidasFile.h"
#include "TMidasOnline.h"
#include "TMidasEvent.h"
#include "TDataContainer.hxx"

V1730File::V1730File(const char* aFileName, int aChannel):DataFile(0),mChannel(aChannel),mWF(0){
  mFile = new TMidasFile();
  bool tryOpen = mFile->Open(aFileName);  
  mMidasEvent = new TMidasEvent;
  mDataContainer = new TDataContainer();
  mIndex=0;

  if (!tryOpen){
    printf("Cannot open input file \"%s\"\n",aFileName);
    exit(0);
  }
  else{
    std::cout << "Opening " << aFileName << std::endl;
  }

  mFile->Read(mMidasEvent);
  int eventId = mMidasEvent->GetEventId();
  if ((eventId & 0xFFFF) != 0x8000){// begin run event
    std::cerr << "Error: First event Id does not match expectation" 
	      << std::endl;
    exit(0);
  }
  
  mWaveformCount=-1;// number of events not known

  mWFYNBins=16384;
  mWFYMax=16384.;
  mWFYMin=0.;
}

V1730File::~V1730File() {
  mFile->Close();
  delete mWF;
  delete mMidasEvent;
  delete mDataContainer;
  delete mFile;  
}


Waveform* V1730File::getNextWaveform(){
  TV1730RawData *v1730 =0;
  while(!v1730){
    mFile->Read(mMidasEvent);
    int eventId = mMidasEvent->GetEventId(); // read next event    
    if ((eventId & 0xFFFF) == 0x8001){// end run event
      printf("printed event \n");
      return 0;
    }
    mMidasEvent->SetBankList();
    // Set the midas event pointer in the physics event.
    mDataContainer->SetMidasEventPointer(*mMidasEvent);

    int timestamp = mDataContainer->GetMidasData().GetTimeStamp();

    v1730 = mDataContainer->GetEventData<TV1730RawData>("V730");
    if(!v1730 )   mDataContainer->CleanupEvent();
  }

  if(!v1730 ){ 
    printf("Error. No V1730 data\n");
    exit(0);
  }
  
  std::vector<RawChannelMeasurement> measurements = v1730->GetMeasurements();
  int nBins=measurements[mChannel].GetNSamples();
  if(!mWF) mWF = new Waveform(nBins,0.,nBins*2.);

  for(int iBin = 0; iBin < nBins; iBin++){
    mWF->SetBinContent(iBin+1,
		       measurements[mChannel].GetSample(iBin));
  }

          
  // Cleanup the information for this event.
  mDataContainer->CleanupEvent();

  std::cout<<"Cazzo grande"<<std::endl;        

  return mWF;  
}
  

Waveform* V1730File::getWaveform(int index){
  while(index>mIndex){
    getNextWaveform();
    mIndex++;
  }
  if(index<mIndex) std::cerr << "Index too low. Cannot read backward for V1730" << std::endl;
  return getNextWaveform();
}





//Waveform* V1730File::getWaveform(int index) {

  //printf("index:%i \n",index);
  //sleep(1000);
  // ss_sleep(100000);
  //	char wfName[20];
  //	sprintf(wfName,"WFCh0Ev%i",index);
  
  //return getNextWaveform();
//}





