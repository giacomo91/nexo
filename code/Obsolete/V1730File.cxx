/*******************************************************

*
0 1000 0
*********************************************************/
#include <cstring>
#include <inttypes.h>
#include <iostream>
//#include <typeinfo>

#include "TV1730RawData.hxx"
#include "V1730File.h"
#include "Waveform.h"

#include "TFile.h"
#include "TKey.h"
#include "TROOT.h"
#include "TList.h"

#include "TMidasFile.h"
#include "TMidasOnline.h"
#include "TMidasEvent.h"
#include "TDataContainer.hxx"







// --- class functions

V1730File::V1730File(const char* aFileName, int aChannel):DataFile(0){
  mFileName=aFileName;
 mFile = new TMidasFile();
  bool tryOpen = mFile->Open(mFileName);  
  
  if (!tryOpen){
    printf("Cannot open input file \"%s\"\n",mFileName);
    return;
  }

  mWaveformCount=10;

  first_wf_done=0;
  chan = aChannel;

  lastwf=false;
  fMaxEvents = 0;
  fCurrentRunNumber = 0;
  fIsOffline = true;
  fDataContainer = new TDataContainer();
  
  // TMidasEvent* mMidasEvent;

  printf("1\n");
 
}



V1730File::~V1730File() {
  printf("2\n");
  mFile->Close();
  if(mWF) delete mWF;
  if(fDataContainer) delete fDataContainer;
  if(mFile) delete mFile;
  
}

bool V1730File::CheckEventID(int eventId){

  // If we didn't specify list of accepted IDs, then accept all.
  if(fProcessEventIDs.size()==0) return true;

  // Otherwise check event ID against list
  for(unsigned int i = 0; i < fProcessEventIDs.size(); i++){
    if(fProcessEventIDs[i] == (eventId & 0xFFFF))
      return true;
  }
  
  return false;
}


bool V1730File::ProcessMidasEvent(){

 int i=chan;



  //process here and fill the waveform. It will be called in get next waveform 

  int id = fDataContainer->GetMidasEvent().GetSerialNumber();
  // if(id%10 == 0) printf(".");

    ////////////////////////////////////////////////////////////////////////////////////////



     printf("Save V1730 waveform\n");
  int eventid = fDataContainer->GetMidasData().GetEventId();
  int timestamp = fDataContainer->GetMidasData().GetTimeStamp();

  TV1730RawData *v1730 = fDataContainer->GetEventData<TV1730RawData>("V730");
  

  int tDeltaTime=2;

  if(v1730 ){  

    std::vector<RawChannelMeasurement> measurements = v1730->GetMeasurements();
  int mNBins=measurements[i].GetNSamples();
    time=v1730->GetTriggerTimeTag();

    printf("timestamp%i\n",time);

//printf("time%i\n",time);
   
 if( first_wf_done==0)
    {
      first_wf_done=1;

   mWF = new Waveform(mNBins,time,time+mNBins*tDeltaTime);
      //create waveform object pointer

      //	Waveform* tWF = (Waveform*) ((TKey*) mKeyIter->Next())->ReadObj();
      //mCurIndex++;
    }

    for(int ib = 0; ib < mNBins; ib++)
      {
		   int adc = measurements[i].GetSample(ib);
		    mWF->SetBinContent(ib+1,adc);
	
    }
    //	}



  }
    
	//std::cout << tWF->GetName() << " " << mCurIndex << std::endl;




  return true;
}

Waveform* V1730File::getNextWaveform(){

printf("yo1\n");

  if(lastwf==true)
    printf("Last wave form, please create a new object\n"); 

 
  TMidasEvent mMidasEvent;
  mFile->Read(&mMidasEvent);
printf("yo2\n");
      
      /// Treat the begin run and end run events differently.
      int eventId = mMidasEvent.GetEventId();


      if ((eventId & 0xFFFF) == 0x8000){// begin run event
	// mMidasEvent.Print();
printf("printed event begin \n");
}  
      else if ((eventId & 0xFFFF) == 0x8001){// end run event
	//mMidasEvent.Print();
 printf("printed event \n");
}
      else { // all other events; check that this event ID should be processed.
	printf("yo3\n");
        // Set the bank list for midas event.       
        mMidasEvent.SetBankList();
 printf("yo4\n");

        // Set the midas event pointer in the physics event.
         fDataContainer->SetMidasEventPointer(mMidasEvent);

        //ProcessEvent if prefilter is satisfied...
				if(PreFilter())
					 ProcessMidasEvent();
        
        // Cleanup the information for this event.
        fDataContainer->CleanupEvent();
        
      }
       ii++;
      printf("i is %i",ii);
      /*
      if((ii%25)==1)
	printf("\n");

      if ((fMaxEvents!=0)&&(ii>=fMaxEvents)){
	printf("Reached event %i, exiting loop.\n",ii);
	lastwf=true;
      }
      */

  if(ii==1){

    //return NULL if it is eventheader

 return NULL;
 
}
     printf("%i",ii);
      return mWF;

    }
  


// EndRunRAD(0,fCurrentRunNumber,0);
// EndRun(0,fCurrentRunNumber,0);
//CloseRootFile();  

  // start the ROOT GUI event loop
  //  app->Run(kTRUE);

//Waveform* LecroyFile::getWaveform(const char* aHName){
//return (Waveform*) mFile->Get(aHName);



Waveform* V1730File::getWaveform(int index) {

  printf("index:%i \n",index);
  sleep(1000);
  // ss_sleep(100000);
  //	char wfName[20];
  //	sprintf(wfName,"WFCh0Ev%i",index);
  
  return getNextWaveform();
}





