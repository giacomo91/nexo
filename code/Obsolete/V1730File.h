/*******************************************************
* Tektronix WFM File reader
*
* History:
* v1	2011/08/17	Initial support for WFM003 mFiles (Kyle Boone)
* v2	2011/11/25	Integration with DataFile class (Kyle Boone)
*
* TODO:
* add support for LECROY001 and LECROY002 mFiles
* handle endianness
*********************************************************/

#ifndef V1730_FILE_H 
#define V1730_FILE_H

#include <vector>
#include <stdint.h>

#include "DataFile.h"

class Waveform;
class TMidasFile;
class TMidasEvent;
class TDataContainer;

class V1730File : public DataFile {
 public:
  V1730File(const char* aFileName, int aChannel);
  Waveform* getNextWaveform();
  Waveform* getWaveform(int index);



  bool ProcessMidasEvent();
  void ProcessThisEventID(int eventID){
    fProcessEventIDs.push_back(eventID);
  };
  bool CheckEventID(int eventId);


    /// Are we processing online data?
  bool IsOnline() const {return !fIsOffline;};

  /// Are we processing offline data?
  bool IsOffline() const {return fIsOffline;};
  
  /// Current Run Number
  int GetCurrentRunNumber() const {return fCurrentRunNumber;};

  /// Current Run Number
  void SetCurrentRunNumber(int run) {fCurrentRunNumber = run;};


  virtual ~V1730File();
  
private:
  TMidasFile* mFile;
  TMidasEvent* mMidasEvent;
  TDataContainer *mDataContainer;
  Waveform* mWF;
  int mChannel;

  int mCurIndex;

  uint32_t time;
  int first_wf_done;
  const char* mFileName;
  int chan;
  int ii;
  bool fIsOffline;
  bool lastwf;

  /// Current run number
  int fCurrentRunNumber;



  /// This is the set of eventIDs to process

  std::vector<int> fProcessEventIDs;
 
  
 // Variables for offline analysis
  int fMaxEvents;

  // The TApplication...
  // TApplication *fApp;

  // Should we automatically create a Main Window?
  bool fCreateMainWindow;

  // Use a batch mode.
  bool fUseBatchMode;

  
}; 


#endif


