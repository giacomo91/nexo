/*******************************************************
* Tektronix WFM File reader
*
* History:
* v1	2011/08/17	Initial support for WFM003 mFiles (Kyle Boone)
* v2	2011/11/25	Integration with DataFile class (Kyle Boone)
*
* TODO:
* add support for WFM001 and WFM002 mFiles
* handle endianness
*********************************************************/

#include "LecroyFile.h"
#include "V730File.h"
#include "TMidasFile.h"
#include "Waveform.h"
#include "TFile.h"
#include "TKey.h"
#include "TROOT.h"
#include "TList.h"

//#include <cstring>
//#include <inttypes.h>
#include <iostream>



// --- class functions

V1730File::V1730File(const char* aFileName):DataFile(0){

  mFile = new TMidasFile();
  bool tryOpen = mFile->Open(fname);     
  TMidasEvent* mMidasEvent;


 
}



V1730File::~V1730File() {
  delete mFile;
}

Waveform* V1730File::getNextWaveform(){
  mFile->Read(mMidasEvent);
  mMidasEvent->SetBankList();
  mDataContainer->SetMidasEventPointer(*mMidasEvent);


  int id = dataContainer.GetMidasEvent().GetSerialNumber();
    if(id%10 == 0) printf(".");

    ////////////////////////////////////////////////////////////////////////////////////////



    // printf("Save V1730 waveform\n");
  int eventid = dataContainer.GetMidasData().GetEventId();
  int timestamp = dataContainer.GetMidasData().GetTimeStamp();
uint32_t time;
  TV1730RawData *v1730 = dataContainer.GetEventData<TV1730RawData>("V730");
  
  int entries;
  if(v1730 ){  
    entries =  fTree->GetEntries();
    xvalues=0;  
    
    trigtimetag = v1730->GetTriggerTimeTag();

    //  printf("timestamp%i\n",trigtimetag);
    trigtime->Fill();
    std::vector<RawChannelMeasurement> measurements = v1730->GetMeasurements();


time=v1730->GetTriggerTimeTag();

//printf("time%i\n",time);
    


   // for(unsigned int i = 0; i < measurements.size(); i++) // loop over measurements
        //{
//time=v1730->GetTriggerTimeTag();
//}
        //int chan = measurements[i].GetChannel();
    
    int i=0;
	/*for(int ib = 0; ib < measurements[i].GetNSamples(); ib++){


		   uint32_t adc = measurements[i].GetSample(ib);
		adc
		   adc_value_chan0 = adc;
		   //  printf("adc value: %i\n",adc);
		   //  if(chan==0)
		     // adc->Fill();
		     xvalues+=2;
		     fTree->GetEntry(ib);
		     XVAL->Fill();
		     adcbranch->Fill();
	}
     
        */
for(int ib = 600; ib < 750; ib++){


		   int adc = measurements[i].GetSample(ib);
		adc-=7110;
		adc*=-1;
		integral+=adc;

		   adc_value_chan0 = adc;
		   //  printf("adc value: %i\n",adc);
		   //  if(chan==0)
		     // adc->Fill();
		     xvalues+=2;
		     fTree->GetEntry(ib);
		     XVAL->Fill();
		     adcbranch->Fill();
	}



  }
      //id event timetag integral
indexx++;
fp1 = fopen ("/home/exo/Desktop/Data_ana_alex/darkana.txt", "a");
fprintf(fp1,"%i %zu %i \n",indexx,time,integral);
fclose(fp1);
integral=0;
  fTree->Write();


	Waveform* tWF = (Waveform*) ((TKey*) mKeyIter->Next())->ReadObj();
	mCurIndex++;
	//std::cout << tWF->GetName() << " " << mCurIndex << std::endl;
	return tWF;
}

//Waveform* LecroyFile::getWaveform(const char* aHName){
//return (Waveform*) mFile->Get(aHName);
//}


Waveform* V1730File::getWaveform(int index) {
	char wfName[20];
	sprintf(wfName,"WFCh0Ev%i",index);
	return (Waveform*) mFile->Get(wfName);
}








      if (!f.Read(&event))
	break;
//if (fODB) delete fODB;
//      fODB = new XmlOdb(event.GetData(),event.GetDataSize());
 
