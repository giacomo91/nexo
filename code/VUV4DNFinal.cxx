//g++ -g code/VUV4DNFinal.cxx -o bin/VUV4DNFinal.exe -pthread -m64 -I/home/deap/packages/root/include -L/home/deap/packages/root/lib -lCore -lCint -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -pthread -lm -ldl -rdynamic


#include <fstream>
#include <cstring>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include "TF1.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TH1.h"
#include "TH2.h"
#include "TROOT.h"

int main(int argc, char** argv){
  //int aRun = argc>1 ? atoi(argv[1]) : 0;

  double timeMin=4280.;
  double gainScale=-0.5/pow(2,14)/50*1e-9/1.6e-19/100;


  // bins for next pulse histogram
  int nDtBinMax=1001;
  double DtBin[1000];
  int nDtBin=0;
  double upperBound=pow(10,nDtBin/10.);
  while(upperBound<1e9 && nDtBin<nDtBinMax){
    DtBin[nDtBin]= upperBound;
    //bin[nDtBin]=1000;
    nDtBin++;
    upperBound = pow(10,nDtBin/10.);
  }
  DtBin[nDtBin]=upperBound;


  ifstream fPar("ntp/VUV4DNNov2017/RunList.txt");
  char dum[50];
  fPar >> dum >> dum >> dum >> dum >> dum;
  int iRun;
  int nVoltage=0;
  double Voltage[20];
  double OverVoltage[20];
  double Gain[20];
  double GainErr[20];
  double DXT[20];
  double DXTErr[20];
  int nTemp=0;
  int Temperature[20];
  double BreakdownVoltage[20];  
  double BreakdownVoltageErr[20];   	  
  
  fPar >> iRun >> Voltage[nVoltage] >> dum >> Temperature[nTemp];

  TFile fOut("ntp/VUV4DNFinal.root","RECREATE");

  TF1 FGaus("FGaus","[0]/sqrt(2*3.14)/[2]*exp(-(x[0]-[1])*(x[0]-[1])/2/[2]/[2])",0.,20e6);

  TDirectory* locDir = gDirectory;

  while(!fPar.eof()){
    char ntpFileName[200];
    sprintf(ntpFileName,"ntp/VUV4DNNov2017/run%05i-0000.mid.gz.Ch0Fit101",iRun);
   
    char lsNtpFile[200];
    sprintf(lsNtpFile,"ls %s",ntpFileName);
    int checkFile = system(lsNtpFile);
    if(checkFile!=0){ // does not exist. Produce the file
       char produceFile[200];
       sprintf(produceFile,"./bin/PulseFinding.exe %i 101 100000 0 VUV4DNNov2017",iRun);
       system(produceFile);
    }

    std::cout << iRun << " " << ntpFileName << " " << Voltage[nVoltage] << std::endl;
    TFile fIn(ntpFileName);
    TNtuple* ntp = (TNtuple*) fIn.Get("ntp");
    int nEntries = ntp->GetEntries();
    float* ntpCont = ntp->GetArgs();
    
    fOut.cd();
    
    char HQTriggerName[50];
    sprintf(HQTriggerName,"HQTrigger%i",iRun);
    TH1D HQTrigger(HQTriggerName,HQTriggerName,400,0.,40e6);

    char HQTTriggerName[50];
    sprintf(HQTTriggerName,"HQTTrigger%i",iRun);
    TH2D HQTTrigger(HQTTriggerName,HQTTriggerName,timeMin, 100, timeMin+100, 100,0.,40e6);
    
    char HAvTriggerName[50];
    sprintf(HAvTriggerName,"HAvTrigger%i",iRun);
    TH1D HAvTrigger(HAvTriggerName,HAvTriggerName,200,0.,5.);
    

    // --- Look for trigger pulse starting after timeMin and histo chargel
    int iEntry=0;      
    ntp->GetEntry(iEntry);
    float iEvent = 1.;
    while(iEntry<nEntries){
      int triggerPulse=0;
      while(iEntry<nEntries && ntpCont[0]==iEvent){
	if(ntpCont[13]>timeMin && !triggerPulse){
	  triggerPulse=1;
	  HQTrigger.Fill(ntpCont[12]*gainScale);
	  HQTTrigger.Fill(ntpCont[13],ntpCont[12]*gainScale);
	}
	iEntry++;
	ntp->GetEntry(iEntry);
      }
      //std::cout << iEvent << " " << iEntry << " " << nEntries << std::endl;
      iEvent = ntpCont[0];
    }

    // --- Figure out 1PE charge
    FGaus.SetParameters(HQTrigger.GetMaximum()*2.5,
			 HQTrigger.GetBinCenter(HQTrigger.GetMaximumBin()),
			 1e5);
    //HQTrigger.Fit(FGaus.GetName());
    Gain[nVoltage] = FGaus.GetParameter(1);
    GainErr[nVoltage] = FGaus.GetParError(1);
    
    //_________________________________
    // --- Read file again now that we know 

    // --- After-pulse loop
    //1: Histogram for Time Distribution
    char HDTimeName[20];
    sprintf(HDTimeName,"HDTime%i",iRun);
    TH1D HDTime(HDTimeName,HDTimeName,nDtBin,DtBin);
    
    //2: Histogram with time on x and Amplitude on Y
    char HDTimeAmpName[20];
    sprintf(HDTimeAmpName,"HDTimeAmp%i",iRun);
    TH2D HDTimeAmp(HDTimeAmpName,HDTimeAmpName,nDtBin,DtBin,100,0.,3.5);

    char HRateName[20];
    sprintf(HRateName,"HRate%i",iRun);
    TH1D HRate(HRateName,HRateName,nDtBin,DtBin);

    int NEventNorm=0;

    iEntry=0;
    while(iEntry<nEntries){
      do{
	ntp->GetEntry(iEntry);
	iEntry++;
      }while(iEntry<nEntries &&
	     !(fabs(ntpCont[12]*gainScale/Gain[nVoltage]-1.)<0.25 && //single PE bump
	       fabs(ntpCont[13]-4308)<10.));
      if(iEntry<nEntries){ // found a pulse at the right time and right amplitude
	NEventNorm++;
	double tTriggerTime = ntpCont[1];
	double tTriggerPulseTime = ntpCont[13]+ntpCont[1];
	ntp->GetEntry(iEntry);
	do{
	  ntp->GetEntry(iEntry);
	  iEntry++;
	}while(iEntry<nEntries &&
	       ntpCont[12]*gainScale/Gain[nVoltage]<0.14); // not noise, NON SO big problem if your are sure you have fitted well (so if you don't have noise fit)
	if(iEntry<nEntries){// found a 2nd pulse
	  HDTime.Fill((ntpCont[13]+ntpCont[1]-tTriggerPulseTime));
	  HDTimeAmp.Fill((ntpCont[13]+ntpCont[1]-tTriggerPulseTime),
			  ntpCont[12]*gainScale/Gain[nVoltage],1.);
	  iEntry--; //Allow this pulse to be reused as first pulse (VERY IMPORTANT AT LOW TEMPERATURES)
	}
      }
    }

    std::cout << NEventNorm << std::endl;
    /*double prevBlank=1e3;
    iEntry=0;       
    float prevPulseTime=0.;
    float firstPulseTime=-1.;
    while(iEntry<nEntries){
      ntp->GetEntry(iEntry);

      if(firstPulseTime>0. &&  // good first pulse
	 ntpCont[12]*gainScale/Gain[nVoltage]>0.1){ // not noise
	HDTime.Fill(ntpCont[13]+ntpCont[1]-firstPulseTime);
	HDTimeAmp.Fill(ntpCont[13]+ntpCont[1]-firstPulseTime,ntpCont[12]*gainScale/Gain[nVoltage]);
      }
      if(//ntpCont[13]+ntpCont[1]-prevPulseTime>prevBlank && // no earlier pulse
	 fabs(ntpCont[13]-4308)<10. &&
	 fabs(ntpCont[12]*gainScale/Gain[nVoltage]-1.)<0.15){ // 1PE
	firstPulseTime=ntpCont[13]+ntpCont[1];
      }
      else{
	firstPulseTime=-1.;
      }
      if(ntpCont[12]*gainScale/Gain[nVoltage]>0.1) prevPulseTime = ntpCont[13]+ntpCont[1];

      iEntry++;
      }*/
    
    /* int NEventNorm=0;
    iEntry=0;
    while(iEntry<nEntries){
      do{
	ntp->GetEntry(iEntry);
	iEntry++;
      }
      while(iEntry<nEntries &&
	    !(fabs(ntpCont[12]+60e3)<15e3 &&
	      //fabs(ntpCont[12]*gainScale/Gain[nVoltage]-1.)<0.15 &&
	      fabs(ntpCont[13]-4308.)<10.));
      if(iEntry<nEntries){ // found a pulse at the right time and right amplitude
	NEventNorm++;
	double tTriggerPulseTime = ntpCont[13]+ntpCont[1];
	ntp->GetEntry(iEntry);
	do{
	  ntp->GetEntry(iEntry);
	  iEntry++;
	}while(iEntry<nEntries &&
	       ntpCont[12]>-60e3/7.);
	       //ntpCont[12]*gainScale/Gain[nVoltage]<0.1);
	if(iEntry<nEntries){// found a 2nd pulse
	  HDTime.Fill((ntpCont[13]+ntpCont[1]-tTriggerPulseTime));
	  HDTimeAmp.Fill((ntpCont[13]+ntpCont[1]-tTriggerPulseTime),
			 ntpCont[12]*gainScale/Gain[nVoltage],1.);
	  iEntry--; //Allow this pulse to be reused as first pulse (VERY IMPORTANT AT LOW TEMPERATURES)
        }
      }
    }
    */

    double Pi, Flambda, sigFlambda, sigFbeta;
    double Fbeta=0.;
    for(int ti=1; ti<=HDTime.GetNbinsX(); ti++){
      Pi = HDTime.GetBinContent(ti)/NEventNorm;
      Flambda = -log(1-Pi/exp(-Fbeta));     
      sigFlambda = sqrt(Pi/NEventNorm+Pi*Pi*sigFbeta*sigFbeta)/(exp(-Fbeta)-Pi);
      // >>> Calculate for next bin
      sigFbeta = sqrt(Pi/NEventNorm+exp(-2*Fbeta)*sigFbeta*sigFbeta)/(exp(-Fbeta)-Pi);
      Fbeta+=Flambda;
      HRate.SetBinContent(ti,Flambda/HDTime.GetBinWidth(ti));
      HRate.SetBinError(ti,sigFlambda/HDTime.GetBinWidth(ti));
      HDTime.SetBinContent(ti,HDTime.GetBinContent(ti)/NEventNorm/HDTime.GetBinWidth(ti));
      HDTime.SetBinError(ti,sqrt(HDTime.GetBinContent(ti))/NEventNorm/HDTime.GetBinWidth(ti));      
    }



    HDTime.Write();
    HDTimeAmp.Write();
    HRate.Write();

    // --- Cross-talk 
    iEntry=0;      
    ntp->GetEntry(iEntry);
    iEvent = 1.;
    while(iEntry<nEntries){
      int triggerPulse=0.;;
      while(iEntry<nEntries && ntpCont[0]==iEvent){
	if(!triggerPulse && ntpCont[13]>timeMin){
	  triggerPulse=ntpCont[13];
	  HAvTrigger.Fill(ntpCont[12]*gainScale/Gain[nVoltage]);
	}
	iEntry++;
	ntp->GetEntry(iEntry);
      }
      //std::cout << iEvent << " " << iEntry << " " << nEntries << std::endl;
      iEvent = ntpCont[0];
    }     
    DXT[nVoltage] = HAvTrigger.GetMean()-1.;
    DXTErr[nVoltage] = HAvTrigger.GetRMS()/sqrt(HAvTrigger.GetEntries());
    HQTrigger.Write();
    HAvTrigger.Write();
    
    // ---
    Voltage[nVoltage]-=0.5;// assume 1V binning
    nVoltage++;
  
    int nextTemperature;
    double tempVoltage;
    fPar >> iRun >> tempVoltage >> dum >> nextTemperature;
  
    std::cout << nTemp << " " << nextTemperature << " " << Temperature[nTemp] << std::endl;
  
    // --- check end of temperature
    if((fPar.eof() ||  nextTemperature!=Temperature[nTemp])){
      if(nVoltage>0){
	Voltage[nVoltage] = Voltage[nVoltage-1]+1.;
	char HGainName[20];
	sprintf(HGainName,"HGainT%s%i",Temperature[nTemp]<0? "m": "p",
		abs(Temperature[nTemp]));
	TH1D HGain(HGainName,HGainName,nVoltage,Voltage);
	std::cout << HGain.GetName() << " " << nVoltage << std::endl;
	for(int iVoltage=0; 
	    iVoltage<nVoltage; 
	    iVoltage++){
	  HGain.SetBinContent(iVoltage+1,Gain[iVoltage]);
	  HGain.SetBinError(iVoltage+1,GainErr[iVoltage]);
	}
	HGain.SetMarkerStyle(20);
	HGain.Write();
	TF1 FGainV("FGainV","[0]*(x[0]-[1])",30.,70.);
	FGainV.SetParameters(1e6,40.);
	//HGain.Fit("FGainV");
	BreakdownVoltage[nTemp] = FGainV.GetParameter(1);
	BreakdownVoltageErr[nTemp] = FGainV.GetParError(1);
	
	for(int iVoltage=0; iVoltage<=nVoltage; iVoltage++){
	  OverVoltage[iVoltage] = Voltage[iVoltage]- BreakdownVoltage[nTemp];
	}
	char HDXTName[20];
	sprintf(HDXTName,"HDXTT%s%i",Temperature[nTemp]<0? "m": "p",
		abs(Temperature[nTemp]));
	TH1D HDXT(HDXTName,HDXTName,nVoltage,OverVoltage);
	for(int iVoltage=0;  iVoltage<nVoltage; iVoltage++){
	  HDXT.SetBinContent(iVoltage+1,DXT[iVoltage]);
	  HDXT.SetBinError(iVoltage+1,DXTErr[iVoltage]);
	}

	HDXT.SetMarkerStyle(20);
	HDXT.Write();
	nVoltage=0;
      }
      nTemp++;
      Temperature[nTemp]=nextTemperature;
    }
    Voltage[nVoltage] = tempVoltage;
  }
}
