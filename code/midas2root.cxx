// Program for converting Lecroy scope data from MIDAS format to ROOT format.
//
// T. Lindner (Jan 2016) 
//
//

#include <stdio.h>
#include <iostream>
#include <time.h>
#include <vector>

#include "TRootanaEventLoop.hxx"
#include "TLecroyData.hxx"
#include "TFile.h"
#include "TTree.h"

class Analyzer: public TRootanaEventLoop {




public:

  TTree *fTree;

  // Values for the waveforms for four channels.
  std::vector<double> voltch0;
  std::vector<double> voltch1;
  std::vector<double> voltch2;
  std::vector<double> voltch3;
  //std::vector<double> timebase;
  float timebase;



std::string SetFullOutputFileName(int run, std::string midasFilename)
{
    char buff[64];  // (longest filename is 25 chars)
    Int_t in_num = 0, part = 0;
    Int_t num[2] = { 0, 0 };
    // get run/subrun numbers from file name
    for (int i=0; ; ++i) {
        char ch = midasFilename[i];
        if (!ch) break;
        if (ch == '/') {
            // skip numbers in the directory name
            num[0] = num[1] = in_num = part = 0;
        } else if (ch >= '0' && ch <= '9' && part < 2) {
            num[part] = num[part] * 10 + (ch - '0');
            in_num = 1;
        } else if (in_num) {
            in_num = 0;
            ++part;
        }
    }
    if (part == 2) {
        if (run != num[0]) {
            std::cerr << "File name run number (" << num[0]
                      << ") disagrees with MIDAS run (" << run << ")" << std::endl;
            exit(1);
        }
        sprintf(buff,"nexo_%.6d_%.4d.root", run, num[1]);
	printf("Using filename %s\n",buff);
    } 
    return std::string(buff);
};


  Analyzer() {

  };

  virtual ~Analyzer() {};

  void Initialize(){


  }
  
  
  void BeginRun(int transition,int run,int time){
    
    // Create a TTree
    fTree = new TTree("waveforms","Lecroy waveforms");
    fTree->Branch("voltch0",&voltch0);
    fTree->Branch("voltch1",&voltch1);
    fTree->Branch("voltch2",&voltch2);
    fTree->Branch("voltch3",&voltch3);
    fTree->Branch("timebase",&timebase,"timebase/F");

    //fTree->Branch("nentries",&nentires,"nentries/I");
    //fTree->Branch("voltch3",voltch3,"voltch3[nentries]/F");

  }   


  void EndRun(int transition,int run,int time){
        printf("\n");
  }

  
  
  // Main work here; create ttree events for every sequenced event in 
  // Lecroy data packets.
  bool ProcessMidasEvent(TDataContainer& dataContainer){

    int id = dataContainer.GetMidasEvent().GetSerialNumber();
    if(id%10 == 0) printf(".");

    // First loop over waveforms to make sure we have some data 
    // and figure out which channel's waveform is first.
    TLecroyData *lecroyTrace[4] = {0,0,0,0};
    TLecroyData *firstTrace = 0;
    bool isSequencedData = false;
    for(int ch = 0; ch < 4; ch++){
      
      char name[5];
      sprintf(name,"LCR%i",ch);
      
      lecroyTrace[ch] = dataContainer.GetEventData<TLecroyData>(name);

      if(!lecroyTrace[ch]) continue;

      // don't understand this error condition..
      if(lecroyTrace[ch]->GetWaveArrayLength() == 0) continue;

      if(lecroyTrace[ch] && firstTrace == 0)
	firstTrace = lecroyTrace[ch];

      if(lecroyTrace[ch]->IsSequencedEvent())
	isSequencedData = true;
	
    }

    // If no waveform data, just return;
    if(!firstTrace) return true;

    // Different treatments for sequenced and un-sequenced data.
    if(isSequencedData){
      
      // Loop over the sequenced events in each Lecroy data block;
      // Fill tree after each event.
      //std::cout << "Number of sequenced events" << firstTrace->GetSubarrayCount() << std::endl;
      //     exit(0);
      for(int evt = 0; evt < firstTrace->GetSubarrayCount(); evt++){

	//std::cout << "Dealing with event " << evt << std::endl;
	
	voltch0.clear();
	voltch1.clear();
	voltch2.clear();
	voltch3.clear();
	//timebase.clear();

	// Loop over the channels.
	for(int ch = 0; ch < 4; ch++){
	  
	  if(!lecroyTrace[ch]) continue;
	  
	  timebase = lecroyTrace[ch]->GetHorizontalInterval();
	  
	  for(int bin = 0; bin < lecroyTrace[ch]->GetSamplesInWaveform(); bin++){
	    if(ch == 0)
	      voltch0.emplace_back(lecroyTrace[ch]->GetSeqVoltageValue(evt,bin));
	    else if (ch == 1)
	      voltch1.emplace_back(lecroyTrace[ch]->GetSeqVoltageValue(evt,bin));
	    else if (ch == 2)
	      voltch2.emplace_back(lecroyTrace[ch]->GetSeqVoltageValue(evt,bin));
	    else if (ch == 3)
	      voltch3.emplace_back(lecroyTrace[ch]->GetSeqVoltageValue(evt,bin));
	    
	    //if(firstTrace == lecroyTrace[ch])
	    //timebase.emplace_back(timeInterval * (float) bin);
	  }// end loop over samples

	} // end loop over channels.

	//std::cout << "filling tree" << std::endl;
	fTree->Fill();

      } // end loop over events in Lecroy data packet
      
    } else { // else if unsequenced data

      // reset histograms.
      voltch0.clear();
      voltch1.clear();
      voltch2.clear();
      voltch3.clear();
      //timebase.clear();

      // Loop over the channels.
      for(int ch = 0; ch < 4; ch++){
	
	if(!lecroyTrace[ch]) continue;
	
	timebase = lecroyTrace[ch]->GetHorizontalInterval();
	
	for(int bin = 0; bin < lecroyTrace[ch]->GetSamplesInWaveform(); bin++){
	  if(ch == 0)
	    voltch0.emplace_back(lecroyTrace[ch]->GetVoltageValue(bin));
	  else if (ch == 1)
	    voltch1.emplace_back(lecroyTrace[ch]->GetVoltageValue(bin));
	  else if (ch == 2)
	    voltch2.emplace_back(lecroyTrace[ch]->GetVoltageValue(bin));
	  else if (ch == 3)
	    voltch3.emplace_back(lecroyTrace[ch]->GetVoltageValue(bin));
	  
	  //if(firstTrace == lecroyTrace[ch])
	  //timebase.emplace_back(timeInterval * (float) bin);
	}// end loop over samples
	
      } // end loop over channels.
      
      //std::cout << "filling tree" << std::endl;
      fTree->Fill();

    }

    return true;

  }
  

}; 


int main(int argc, char *argv[])
{

  Analyzer::CreateSingleton<Analyzer>();
  return Analyzer::Get().ExecuteLoop(argc, argv);

}

