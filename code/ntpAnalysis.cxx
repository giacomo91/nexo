/*******************************************************************************
* ntpAnalysis.cxx
*
* Description:
* Analyzes an NTuple for dark noise, afterpulse probability, etc. Also provides
* formatting for histogram generation.
*
*******************************************************************************/

#include <sys/stat.h>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>

#include "TFile.h"
#include "TNtuple.h"
#include "TF1.h"
#include "TROOT.h"
#include "TH1.h"
#include "TGraph.h"
#include "TMath.h"
#include "TCanvas.h"

#include "ntpAnalysis.h"

ntpAnalysis::ntpAnalysis(int aRun) //add functionality for arun
{

}

ntpAnalysis::ntpAnalysis(int aRun1, int aRun2) //add functionality for arun1, arun2
{

}

//For collection data from a single data file
void ntpAnalysis::processData(int aRun)
{
	char* name = getFileName(aRun, "./RunInfo.txt", 0);
	TFile* f1 = new TFile(name);
	TNtuple* ntp = (TNtuple*) f1->Get("ntp");
	NtpCont* cont = (NtpCont*) ntp->GetArgs();

	int nPulse = ntp->GetEntries();
	int iPulse = 0;
	bool predarkpulse = false;
	bool darkpulse = false;
	bool mainpulse = false;
	int numpeaks;
	double peaktimes[10];
	double peakamps[10];
	double time;
	int firstpeak = 0;

	while(iPulse<nPulse)
	{
		//Stores data for all peaks in a given event
		ntp->GetEntry(iPulse);
		numpeaks = cont->nfp;
		peaktimes[0] = cont->ft;
		peakamps[0] = cont->fa;
		for(int i=1; i<numpeaks; i++)
		{
			iPulse++;
			ntp->GetEntry(iPulse);
			peaktimes[i] = cont->ft;
		}
		numevents = cont->evt;
		if(numpeaks==1) numsinglepeaks++;

		//Counts delayed pulses and their times for single-peak events
		if(numpeaks==1 && cont->fa<-0.01){// one pulse (can be >1PE due to xtalk)
			time = peaktimes[0] - shift;
			if((time > 2e-9) && (time < 8e-8)) num2to80++;
			else if((time > 8e-8) && (time < 5e-7)) num80to500++;
			TimeDist->Fill(time*1e9);
		}

		//Checks for peaks in the -1000:-500ns range, the -500:0ns range, and the 0+ns range
		for(int i=0; i<numpeaks; i++)
		{
			if(peaktimes[i] < -5e-7+shift) predarkpulse = true;
			if((peaktimes[i] > -5e-7+shift) && (peaktimes[i] < shift)) darkpulse = true;
			if((peaktimes[i] > shift) && (peaktimes[i] < 5e-7+shift)) mainpulse = true;
		}
		if(mainpulse == false) numpeaklessevents++;

		//Counts the number of events valid for dark noise measurement, and the number of those that contain dark noise pulses
		if(predarkpulse == false)
		{
			numvaliddarkevents++;
			if(darkpulse == true) numdevents++;
		}	

		//Counts the number of events with no -500:0ns pulse, and for those finds the first peak after the trigger
		if((darkpulse == false) && (mainpulse == true))
		{
			numvalidevents++;
			for(int i=1; i<numpeaks; i++)
			{
				if(peaktimes[firstpeak] > shift)
				{
					if((peaktimes[i] >shift) && (peaktimes[i] < peaktimes[firstpeak])) firstpeak = i;
				}
				else firstpeak = i;
			}
		}	
		//Excludes cross-talk and fills a histogram with the peak times up to 500ns after the first peak
		//Also counts the number of afterpulses and the number of those after 80ns
		if((polarity*peakamps[firstpeak] < xtalkthresh) && (darkpulse==false) && (mainpulse==true))
		{ 
			for(int i=0; i<numpeaks; i++)
			{
				if((peaktimes[i] > peaktimes[firstpeak]) && (peaktimes[i] < 5e-7+shift))
				{
					aPulsingDist->Fill((peaktimes[i]-shift)*1e9);
					numapulses++;
					if(peaktimes[i] > 8e-8+shift) numpost80ns++;
				}
			}
		}
		firstpeak = 0;
		predarkpulse = false;
		darkpulse = false;
		mainpulse = false;
		iPulse++;
	}
}

//For combining the results from data files with different resolutions
//Note: the magnitude of 'cut' is the time that separates the two segments
//Note: if 'cut' is negative, the function processes subsequent data, otherwise it processes previous data
void ntpAnalysis::processData(int aRun, double cut)
{
	char* name = getFileName(aRun, "./RunInfo.txt", 0);
	TFile* f1 = new TFile(name);
	TNtuple* ntp = (TNtuple*) f1->Get("ntp");
	NtpCont* cont = (NtpCont*) ntp->GetArgs();

	int nPulse = ntp->GetEntries();
	int iPulse = 0;
	bool predarkpulse = false;
	bool darkpulse = false;
	bool mainpulse = false;
	int numpeaks;
	double peaktimes[10];
	double peakamps[10];
	double time;
	int firstpeak = 0;

	while(iPulse<nPulse)
	{
		//Stores data for all peaks in a given event
		ntp->GetEntry(iPulse);
		numpeaks = cont->nfp;
		peaktimes[0] = cont->ft;
		peakamps[0] = cont->fa;
		for(int i=1; i<numpeaks; i++)
		{
			iPulse++;
			ntp->GetEntry(iPulse);
			peaktimes[i] = cont->ft;
		}
		numevents = cont->evt;
		//Counts delayed pulses and their times for single-peak events
		if(numpeaks==1 && cont->fa*polarity < xtalkthresh){// one pulse (can be >1PE due to xtalk)
			time = peaktimes[0] - shift;
			if(cut > 0){
				if((time > 2e-9) && (time < cut)) num2to80+=eventdiscrepancy;  
				smallTimeDist->Fill(time*1e9);
			}
			else if(cut < 0){
				if((time > -1*cut) && (time < 8e-8)) num2to80++;
				else if((time > 8e-8) && (time < 5e-7)) num80to500++;
				largeTimeDist->Fill(time*1e9);
			}
		}
		if(cut<0)
		{
			if(numpeaks==1) numsinglepeaks2++;
			//Checks for peaks in the -1000:-500ns range, the -500:0ns range, and the 0+ns range
			for(int i=0; i<numpeaks; i++)
			{
				if(peaktimes[i] < -5e-7+shift) predarkpulse = true;
				if((peaktimes[i] > -5e-7+shift) && (peaktimes[i] < shift)) darkpulse = true;
				if((peaktimes[i] > shift) && (peaktimes[i] < 5e-7+shift)) mainpulse = true;
			}
			if(mainpulse == false) numpeaklessevents++;

			//Counts the number of events valid for dark noise measurement, and the number of those that contain dark noise pulses
			if(predarkpulse == false)
			{
				numvaliddarkevents++;
				if(darkpulse == true) numdevents++;
			}	
			//Counts the number of events with no -500:0ns pulse, and for those finds the first peak after the trigger
			if((darkpulse == false) && (mainpulse == true))
			{
				numvalidevents++;
				for(int i=1; i<numpeaks; i++)
				{
					if(peaktimes[firstpeak] > shift)
					{
						if((peaktimes[i] >shift) && (peaktimes[i] < peaktimes[firstpeak])) firstpeak = i;
					}
					else firstpeak = i;
				}
			}	
			//Excludes cross-talk and fills a histogram with the peak times up to 500ns after the first peak
			//Also counts the number of afterpulses and the number of those after 80ns
			if((polarity*peakamps[firstpeak] < xtalkthresh) && (darkpulse==false) && (mainpulse==true))
			{ 
				for(int i=0; i<numpeaks; i++)
				{
					if((peaktimes[i] > peaktimes[firstpeak]) && (peaktimes[i] < 5e-7+shift))
					{
						aPulsingDist->Fill((peaktimes[i]-shift)*1e9);
						numapulses++;
						if(peaktimes[i] > 8e-8+shift) numpost80ns++;
					}
				}
			}
			firstpeak = 0;
			predarkpulse = false;
			darkpulse = false;
			mainpulse = false;
		}
		//else if((cut > 0) && (numpeaks==1)) numsinglepeaks1++;
		iPulse++;
	}
}

//calculate all parameters
void ntpAnalysis::calculateAllParameters(int aRun)
{
	//Must be input
	shift = 54e-9; //in seconds
	polarity = -1;
	xtalkthresh = 0.05; //Positive, in V
	eventdiscrepancy = 10; 	//has value other than 1 if one dataset used has more events than the other
							//is the ratio of tail dataset events to prompt peak dataset events

	nBin1=2000;
	nBin2=20000;
	minTime = -1000;
	maxTime = 1000;
	bps = nBin1 / 2e-6;
	char AName[50];
	sprintf(AName,"Afterpulsing %i",aRun);
	char DName[50];
	sprintf(DName,"Delayed Pulses %i",aRun);

	aPulsingDist = new TH1D(AName,AName,nBin1,minTime,maxTime);
	TimeDist = new TH1D(DName,DName,nBin2,minTime,maxTime);

	numevents = 0;
	numvalidevents = 0; //Events with no pulses between 0 and 500ns before trigger
	numvaliddarkevents = 0; //Events with no pulses between 500 and 1000ns before trigger
	numdevents = 0; //Valid dark events with 1 or more pulses between 0 and 500ns before trigger
	numapulses = 0;
	numpost80ns = 0;
	num2to80 = 0; //Number of main avalanches between 2:80ns
	num80to500 = 0; //Number of main avalanches between 80:500ns
	numpeaklessevents = 0;
	numsinglepeaks = 0;

	processData(aRun);

	double devents = 1.0 * numvaliddarkevents;
	double ddarkevents = 1.0 * numdevents;

	//Calculates the probability that a given 500ns period will have no peaks
	//Given that dark noise follows poisson stats, the avg. number of dark pulses in 500ns can be calculated
	darkNoiseRate = -1 * log((devents - ddarkevents) / devents);
	darkNoiseRate /= 500e-9;

	//Subtracts from afterpulsing data to account for dark noise peaks
	double aPulsing_DN = darkNoiseRate * numvalidevents;
	numapulses -= aPulsing_DN * 500e-9;
	numpost80ns -= aPulsing_DN * 420e-9;
	for(int iBin = 0; iBin < aPulsingDist->GetNbinsX(); iBin++){
		if(aPulsingDist->GetBinContent(iBin) > aPulsing_DN/bps) aPulsingDist->SetBinContent(iBin, aPulsingDist->GetBinContent(iBin)-aPulsing_DN/bps);
		else aPulsingDist->SetBinContent(iBin, 0);
	}

	//Calculates the avg. # of afterpulses and the fraction of these after 80ns
	fractionAfter80 = 1.0*numpost80ns / numapulses;
	avgNumAfterpulse = 1.0*numapulses / numvalidevents;

	//Calculates the probability of a single-peak event being dark noise
	double probnopeak = 1.0 * numpeaklessevents / numevents;
	double probnodnpeak = (devents - ddarkevents) / devents;
	double probnophotopeak = probnopeak / probnodnpeak;
	double prob1dnpeak = darkNoiseRate * 500e-9 * exp(-1 *  darkNoiseRate * 500e-9); //by poisson statistics
	double numfalsepulses = numsinglepeaks * prob1dnpeak * probnophotopeak;
	falsepulserate = numfalsepulses / 500e-9; 

	num2to80 -= falsepulserate * 78e-9;
	num80to500 -= falsepulserate * 42e-8;

	//Calculates the probability of delayed pulses in the specified ranges
	avalancheProb_2to80 = 1.0 * num2to80 / numevents;
	avalancheProb_80to500 = 1.0 * num80to500 / numevents;
	avalancheProb_2to500 = avalancheProb_2to80 + avalancheProb_80to500;
}


//calculate all parameters
void ntpAnalysis::calculateAllParameters(int aRun, int aRun2, double cut)
{
	//Must be input
	shift = 54e-9; //in seconds
	polarity = -1;
	xtalkthresh = 0.05; //Positive, in V
	eventdiscrepancy = 10; 	//has value other than 1 if one dataset used has more events than the other
							//is the ratio of tail dataset events to prompt peak dataset events

	nBin1=2000;
	nBin2=20000;
	minTime = -1000;
	maxTime = 1000;
	bps = nBin1 / 2e-6;
	char AName[50];
	sprintf(AName,"Afterpulsing %i",aRun);
	char sDName[50];
	sprintf(sDName,"s-hist Delayed Pulses %i",aRun);
	char lDName[50];
	sprintf(lDName,"l-hist Delayed Pulses %i",aRun);

	aPulsingDist = new TH1D(AName,AName,nBin1,minTime,maxTime);
	smallTimeDist = new TH1D(sDName,sDName,nBin2,minTime,maxTime);
	largeTimeDist = new TH1D(lDName,lDName,nBin2,minTime,maxTime);

	numevents = 0;
	numvalidevents = 0; //Events with no pulses between 0 and 500ns before trigger
	numvaliddarkevents = 0; //Events with no pulses between 500 and 1000ns before trigger
	numdevents = 0; //Valid dark events with 1 or more pulses between 0 and 500ns before trigger
	numapulses = 0;
	numpost80ns = 0;
	num2to80 = 0; //Number of main avalanches between 2:80ns
	num80to500 = 0; //Number of main avalanches between 80:500ns
	numpeaklessevents = 0;
	numsinglepeaks1 = 0; //These two are used instead of numsinglepeaks when two datasets are used
	numsinglepeaks2 = 0;

	processData(aRun, -1*cut);
	processData(aRun2, cut);

	double devents = 1.0 * numvaliddarkevents;
	double ddarkevents = 1.0 * numdevents;

	//Calculates the probability that a given 500ns period will have no peaks
	//Given that dark noise follows poisson stats, the avg. number of dark pulses in 500ns can be calculated
	darkNoiseRate = -1 * log((devents - ddarkevents) / devents);
	darkNoiseRate /= 500e-9;

	//Subtracts from afterpulsing data to account for dark noise peaks
	double aPulsing_DN = darkNoiseRate * numvalidevents;
	numapulses -= aPulsing_DN * 500e-9;
	numpost80ns -= aPulsing_DN * 420e-9;
	for(int iBin = 0; iBin < aPulsingDist->GetNbinsX(); iBin++){
		if(aPulsingDist->GetBinContent(iBin) > aPulsing_DN/bps) aPulsingDist->SetBinContent(iBin, aPulsingDist->GetBinContent(iBin)-aPulsing_DN/bps);
		else aPulsingDist->SetBinContent(iBin, 0);
	}

	//Calculates the avg. # of afterpulses and the fraction of these after 80ns
	fractionAfter80 = 1.0*numpost80ns / numapulses;
	avgNumAfterpulse = 1.0*numapulses / numvalidevents;

	//Calculates the probability of a single-peak event being dark noise
	double probnopeak = 1.0 * numpeaklessevents / numevents;
	double probnodnpeak = (devents - ddarkevents) / devents;
	double probnophotopeak = probnopeak / probnodnpeak;
	double prob1dnpeak = darkNoiseRate * 500e-9 * exp(-1 *  darkNoiseRate * 500e-9); //by poisson statistics
	double numfalsepulses1 = numsinglepeaks1 * prob1dnpeak * probnophotopeak;
	double numfalsepulses2 = numsinglepeaks2 * prob1dnpeak * probnophotopeak;
	falsepulserate1 = numfalsepulses1 / 500e-9; 
	falsepulserate2 = numfalsepulses2 / 500e-9;
	num2to80 -= falsepulserate2 * 78e-9;
	num80to500 -= falsepulserate2 * 42e-8;

	//Calculates the probability of delayed pulses in the specified ranges
	avalancheProb_2to80 = 1.0 * num2to80 / numevents;
	avalancheProb_80to500 = 1.0 * num80to500 / numevents;
	avalancheProb_2to500 = avalancheProb_2to80 + avalancheProb_80to500;
}

//Combine histograms of two different timing resolutions
void ntpAnalysis::combineHistograms(double cut)
{
	//Subtracts dark noise pulses from each plot
	for(int iBin = 0; iBin < smallTimeDist->GetNbinsX(); iBin++)
	{
		smallTimeDist->SetBinContent(iBin, smallTimeDist->GetBinContent(iBin) - falsepulserate1 * smallTimeDist->GetBinWidth(iBin));
		largeTimeDist->SetBinContent(iBin, largeTimeDist->GetBinContent(iBin) - falsepulserate2 * largeTimeDist->GetBinWidth(iBin));
	}

	//Normalises both component histograms to have an integral of 1
	double ssum = smallTimeDist->Integral("width");
	for(int iBin = 0; iBin < smallTimeDist->GetNbinsX(); iBin++)
	{
		smallTimeDist->SetBinContent(iBin, smallTimeDist->GetBinContent(iBin) / ssum);
		smallTimeDist->SetBinError(iBin, smallTimeDist->GetBinError(iBin) / ssum);
	}

	double lsum = largeTimeDist->Integral("width");
	for(int iBin = 0; iBin < largeTimeDist->GetNbinsX(); iBin++)
	{
		largeTimeDist->SetBinContent(iBin, largeTimeDist->GetBinContent(iBin) / lsum);
		largeTimeDist->SetBinError(iBin, largeTimeDist->GetBinError(iBin) / lsum);
	}

	//Fills the combined histogram up to the cut with one dataset and after the cut with the other
	//Note: Assumes both histograms have the same range and bin number
	char cDName[50];
	sprintf(cDName,"Delayed Pulses");
	combinedTimeDist = new TH1D(cDName,cDName,nBin2,minTime,maxTime);
	for(int iBin = 0; iBin < combinedTimeDist->GetNbinsX(); iBin++)
	{
		if(combinedTimeDist->GetBinCenter(iBin) < cut)
		{
			combinedTimeDist->SetBinContent(iBin, smallTimeDist->GetBinContent(iBin));
			combinedTimeDist->SetBinError(iBin, smallTimeDist->GetBinError(iBin));
		}
		else
		{
			combinedTimeDist->SetBinContent(iBin, largeTimeDist->GetBinContent(iBin));
			combinedTimeDist->SetBinError(iBin, largeTimeDist->GetBinError(iBin));
		}
	
	}

	//Renormalises the resultant histogram
	double csum = combinedTimeDist->Integral("width");
	for(int iBin = 0; iBin < combinedTimeDist->GetNbinsX(); iBin++)
	{
		combinedTimeDist->SetBinContent(iBin, combinedTimeDist->GetBinContent(iBin) / csum);
		combinedTimeDist->SetBinError(iBin, combinedTimeDist->GetBinError(iBin) / csum);
	}
}

//Make plots. If true, save and close plots
void makePlots(bool savePlots, double cut) 
{
	if(cut != 0) combineHistograms(cut);\

	TCanvas* c1 = new TCanvas("c1", "Time Distribution", 900, 600);
	c1->SetLogy();
	c1->SetLogx();

	TCanvas* c1 = new TCanvas("c1", "Time Distribution", 900, 600);
	c1->SetLogy();
	c1->SetLogx();
	
}

//write all parameters to an ascii file
void ntpAnalysis::writeAll(char* outfile)
{
	ofstream oFile;
	oFile.open(outfile);
	oFile << "DN_Rate\tAvalancheProb_2to80\tAvalancheProb_80to500\tAvgNumAfterpulse\tFracAPulseAfter80\n";
	oFile << darkNoiseRate << "\t" << avalancheProb_2to80 << "\t" << avalancheProb_80to500 << "\t" << avgNumAfterpulse << "\t" << fractionAfter80;
}

//Opens run data file of type .anat(#) where # = aTraining
char* ntpAnalysis::getFileName(int run, char* aLog, int aTraining=0)
{
	char FileNames [100] [2] [100];
	char* activename = new char[1024];
	ifstream iFile;
	iFile.open(aLog);
	int counter = 0;
	char buffer [10000];
	int runnum;
	char* newbuff;
	for (int i = 0; i < 10; i++){
		iFile >> buffer;
	}
	while(!iFile.eof()){
		iFile >> FileNames[counter][0];
		iFile >> FileNames[counter][1];
		sscanf(FileNames[counter][0], "%d", &runnum);
		if((runnum == run)){
			newbuff = FileNames[counter][1];
			sprintf(activename, "/home/deap/nEXO/%s.anat%i", newbuff, aTraining);	
			return activename;
		}
		for (int i = 0; i < 8; i++){
			iFile >> buffer;
		}
	}
	std::cout << "File not found" << std::endl;
}
