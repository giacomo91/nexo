IVfit.C	--> from Luca
	    modified by Jens 2018-02-14
	    works to plot V(I) = K*log(I/I0+1)+R*I
	    with terrible Chi2/ndf

newIVfit.C --> based on Luca's macro above
	       Jens 2018-02-14
	       try fit function I(V) = a*exp(c*V) + b*exp(c*V/2)
                      or maybe  I(V) = a*exp(c1*V) + b*exp(c2*V)
