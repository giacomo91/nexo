#include "TF1.h"
#include "Math/WrappedTF1.h"
#include "Math/BrentRootFinder.h"

Double_t Vmin = 49;
Double_t Vmax = 63.5;

Double_t Imin = 1e-11;
Double_t Imax = 1e-3;

Double_t fconst(double *x , Double_t *par){return par[0];}

Double_t FitFunction2(Double_t *x, Double_t *par){

  Double_t I   = x[0];
  Double_t K   = par[0];
  Double_t Is  = par[1];
  Double_t Vbd = par[2];
  Double_t Vra = par[3];
  Double_t R   = par[4];
  Double_t N   = par[5];
  Double_t M   = par[6];
  Double_t Vs  = par[7];
  Double_t V   = par[8];

  Double_t a = K*TMath::Power(V+R*I,N);//K*(V+R*I)*(1-TMath::Exp(-Vs*(V+R*I)));
  Double_t b = 1 - TMath::Power((V+R*I)/Vra,M);

  Double_t res = 1e12;
  if (b!=0) res = I - Is - a/b;
  
  return res;
}

Double_t FindCurrent(Double_t x, Double_t *par){

  Double_t K   = par[0];
  Double_t Is  = par[1];
  Double_t Vbd = par[2];
  Double_t Vra = par[3];
  Double_t R   = par[4];
  Double_t N   = par[5];
  Double_t M   = par[7];
  Double_t Vs  = par[8];
  
  //This will be a function of I at fixed V
  TF1 f2("f2",FitFunction2,Imin,Imax,9);

  Double_t V = x - Vbd;
  
  f2.FixParameter(0,K);
  f2.FixParameter(1,Is);
  f2.FixParameter(2,Vbd);
  f2.FixParameter(3,Vra);
  f2.FixParameter(4,R);
  f2.FixParameter(5,N);
  f2.FixParameter(6,M);
  f2.FixParameter(7,Vs);
  f2.FixParameter(8,V);

  ROOT::Math::WrappedTF1 wf2(f2);
 
  // Create the Integrator
  ROOT::Math::BrentRootFinder brf2;
 
  // Set parameters of the method
  brf2.SetFunction( wf2, Imin,Imax );
  
  brf2.Solve();
 
  //cout << brf2.Root() << endl;

  //just in case..
  f2.ReleaseParameter(0);
  f2.ReleaseParameter(1);
  f2.ReleaseParameter(2);
  f2.ReleaseParameter(3);
  f2.ReleaseParameter(4);
  f2.ReleaseParameter(5);
  f2.ReleaseParameter(6);
  f2.ReleaseParameter(7);
  f2.ReleaseParameter(8);

  
  return brf2.Root();

}

Double_t FitFunction(Double_t *x, Double_t *par){

  
  Double_t K   = par[0];
  Double_t Is  = par[1];
  Double_t Vbd = par[2];
  Double_t Vra = par[3];
  Double_t R   = par[4];
  Double_t N   = par[5];
  Double_t M   = par[6];
  Double_t Vs  = par[7];

  Double_t V   = fabs(x[0] - Vbd);
  Double_t I = FindCurrent(x[0],par);

  //cout << I << endl;
  
  Double_t a = K*TMath::Power(V+R*I,N);     //K*(V+R*I)*(1-TMath::Exp(-Vs*(V+R*I)));//K*pow(V+R*I,N);
  
  Double_t b = 1 - TMath::Power( (V+R*I)/Vra , M );

  Double_t res = 1e12;
  if (b!=0) res = Is + a/b;
  
  return res;
  
}
 
int rf()
{

  gStyle->SetOptStat(0);
  
  /*
  f->FixParameter(0,1e3);
  f->FixParameter(1,1e-3);
  f->FixParameter(2,26);
  f->FixParameter(3,13);
  f->FixParameter(4,10);
  f->FixParameter(5,2);
  */
  
  ifstream in1("m10.dat");

  Double_t V,I,eI;
  TGraphErrors *g1 = new TGraphErrors();
  int i=0;
  while(1){

    in1 >> V >> I >> eI;

    if (!in1.good()) break;

    if (I>0 && V<70 && eI!=0) {
      i++;
      g1->SetPoint(i,V,I);
      g1->SetPointError(i,0.001,eI);
    }
    cout << V << " " << I << " " << eI << endl;
  }

  TCanvas *c1 = new TCanvas("c1","",10,10,1000,500);
  c1->Divide(1,1);
  c1->cd(1);

  gPad->SetLogy();

  TH1F *dh = new TH1F("dh","",1000,40,65);
  dh->GetYaxis()->SetRangeUser(1e-11,1e-3);
  dh->Draw();
  g1->SetMarkerStyle(7);
  g1->Draw("LP");

  TF1 *lin = new TF1("lin",fconst,43,44,1);
  g1->Fit("lin","R");
  

  TF1 *f = new TF1("f",FitFunction, Vmin , Vmax , 8);

  f->SetParName(0,"K");
  f->SetParName(1,"Is");
  f->SetParName(2,"Vbd");
  f->SetParName(3,"Vra");
  f->SetParName(4,"R");
  f->SetParName(5,"N");
  f->SetParName(6,"M");
  f->SetParName(7,"Vs");
  f->SetParName(4,"R");
  f->SetParName(5,"N");
  f->SetParName(6,"M");
  f->SetParName(7,"Vs");
  
  
  f->SetParLimits(1,0,10000);
  f->SetParLimits(3,0,10000);
  
  //f->SetParameter(0,1e3);//K
  f->FixParameter(1,40e-12);//lin->GetParameter(0));//Is
  f->SetParameter(2,49.2);//Vbd
  f->SetParameter(3,13.1);//Vra

  f->SetParLimits(5,1,3);
  f->SetParLimits(6,1,3);
  
  //f->FixParameter(4,0);//R
  f->SetParameter(5,2.0);//N
  f->SetParameter(6,2.0);//M
  //f->SetParLimits(7,0,1000);//Vs

  f->FixParameter(7,0);//Va
  
  g1->Fit("f","R");
  double chi2 = f->GetChisquare();
  double Ndf  = f->GetNDF();
  cout << "Chi2/NDF = " << chi2 << "/" << Ndf << " = " << chi2/Ndf << endl;
 
  
  f->Draw("SAME");

  /*
  ROOT::Math::WrappedTF1 wf1(f);
 
  // Create the Integrator
  ROOT::Math::BrentRootFinder brf;
 
  // Set parameters of the method
  brf.SetFunction( wf1, 25,35 );
  brf.Solve();
 
  cout << brf.Root() << endl;
  */
  return 0;
}
