
/**********************************
 *          Jens Kroeger          *
 *  kroeger@physi.uni-heidelberg  *
 *          March, 2018           *
 **********************************
 *
 * This macro reads
 * ke, ke2, kh, kh2
 * from a file which is written when fitting IV_VUV4_reverse
 * and calculates
 *
 * AlphaEdeltaS, P1, P2, P3 etc. and AlphaHdeltaS etc.
 * and writes it to file
 */

#include <string>

// ** ** ** ** ** ** //
// GLOBAL VARIABLES: //
// ** ** ** ** ** ** //

const string currents_s = "/home/jens/Documents/data/all_plots/temp/currents.txt";
const string DNrates_s = "/home/jens/Documents/data/all_plots/temp/DN_rates.txt";
const string APrates_s = "/home/jens/Documents/data/all_plots/temp/AP_rates.txt";
const string ratios_s = "/home/jens/Documents/data/all_plots/calc_ratios.txt";

FILE *ratios;


int calc_ratios()
{
    cout << "\n\nMake sure both input files contain\n"
            "the same number of lines and compatible\n"
            "temperatures!!!\n\n";

    ratios = fopen(ratios_s.c_str(),"w");
    Int_t n=100;
    double T, Re, Rh, Ie0, Ih0, Ae, Ah;
    double Re_err, Rh_err, Ie0_err, Ih0_err, Ae_err, Ah_err;
    double RatioE, RatioH;

    double electron_charge = 1.6022e-19;

    ifstream in_currents;
    ifstream in_rates_DN;
    ifstream in_rates_AP;
    in_currents.open(currents_s.c_str());
    in_rates_DN.open(DNrates_s.c_str());
    in_rates_AP.open(APrates_s.c_str());

    TGraphErrors *g_Ie, *g_Ih, *g_Re, *g_Rh, *g_Ae, *g_Ah,
    TGraph *g;

    g_Ie = new TGraphErrors();
    g_Ih = new TGraphErrors();
    g_Re = new TGraphErrors();
    g_Rh = new TGraphErrors();
    g_Ae = new TGraphErrors();
    g_Ah = new TGraphErrors();
    g = new TGraph();
    // this is to skip the first line
    string dummyLine;
    getline(in_currents, dummyLine);
    getline(in_rates_DN, dummyLine);
    getline(in_rates_AP, dummyLine);

    fprintf(ratios,"Temp[C]\tIe0\t\tIh0\t\tRe\t\tRh\t\t(Ie0/e)/(Rh0*36)\t(Ih0/e)/(Rh*36)\n");

    int i=0;
    while(1){
        cout << "test here: " << i << endl;
        in_currents >>T >>Ie0 >> Ie0_err  >>Ih0 >> Ih0_err;
        in_rates_DN    >>T >>Re  >> Re_err   >>Rh  >> Rh_err;
        in_rates_AP    >>T >>Ae  >> Ae_err   >>Ah  >> Ah_err;
        if(!in_currents.good()) break;
        cout << T << "\t Ie0=" << Ie0 << "\tIh0=" << Ih0 << endl;

        g_Ie->SetPoint(i,T,Ie0);
        g_Ie->SetPointError(i,T,Ie0_err);
        g_Ih->SetPoint(i,T,Ih0);
        g_Ih->SetPointError(i,0,Ih0_err);
        g_Re->SetPoint(i,T,Re);
        g_Re->SetPointError(i,0,Re_err);
        g_Rh->SetPoint(i,T,Rh);
        g_Rh->SetPointError(i,0,Rh_err);
        g_Ae->SetPoint(i,T,Ae);
        g_Ae->SetPointError(i,0,Ae_err);
        g_Ah->SetPoint(i,T,Ah);
        g_Ah->SetPointError(i,0,Ah_err);

        g->SetPoint(i,T,Ie0);

        RatioE = (Ie0/electron_charge)/(Re*36);
        RatioH = (Ih0/electron_charge)/(Rh*36);

        fprintf(ratios,"%d\t%.2e\t%.2e\t%.2e\t%.2e\t"
                       "%.2e\t\t%.2e\n",
                T,
                Ie0, Ih0,
                Re, Rh,
                RatioE, RatioH);
        i++;
    }

    fclose(ratios);

    TCanvas *c1 = new TCanvas("c1","c1",1000,600);
    TCanvas *c2 = new TCanvas("c2","c2",1000,600);
    TCanvas *c3 = new TCanvas("c3","c3",1000,600);
//    TCanvas *c4 = new TCanvas("c4","c4",1000,600);

    g_Ie->GetXaxis()->SetTitle("Temperature [#circ C]");
    g_Ih->GetXaxis()->SetTitle("Temperature [#circ C]");
    g_Re->GetXaxis()->SetTitle("Temperature [#circ C]");
    g_Rh->GetXaxis()->SetTitle("Temperature [#circ C]");
    g_Ae->GetXaxis()->SetTitle("Temperature [#circ C]");
    g_Ah->GetXaxis()->SetTitle("Temperature [#circ C]");

    g_Ie->GetYaxis()->SetTitle("Current [A]");
    g_Ih->GetYaxis()->SetTitle("Current [A]");
    g_Re->GetYaxis()->SetTitle("Dark Noise Rate [Hz/mm^2]");
    g_Rh->GetYaxis()->SetTitle("Dark Noise Rate [Hz/mm^2]");
    g_Ae->GetYaxis()->SetTitle("Afterpulses in 1 us");
    g_Ah->GetYaxis()->SetTitle("Afterpulses in 1 us");

    g_Ie->SetMarkerStyle(20);
    g_Ie->SetMarkerSize(0.5);
    g_Ih->SetMarkerStyle(20);
    g_Ih->SetMarkerSize(0.5);
    g_Re->SetMarkerStyle(20);
    g_Re->SetMarkerSize(0.5);
    g_Rh->SetMarkerStyle(20);
    g_Rh->SetMarkerSize(0.5);
    g_Ae->SetMarkerStyle(20);
    g_Ae->SetMarkerSize(0.5);
    g_Ah->SetMarkerStyle(20);
    g_Ah->SetMarkerSize(0.5);

    g_Ih->SetMarkerColor(kRed);
    g_Ih->SetLineColor(kRed);
    g_Rh->SetMarkerColor(kRed);
    g_Rh->SetLineColor(kRed);
    g_Ah->SetMarkerColor(kRed);
    g_Ah->SetLineColor(kRed);

    c1->cd();
    c1->SetLogy();
    g_Ie->GetYaxis()->SetRangeUser(1e-13,1e-7);
    TLegend * leg1 = new TLegend(0.175,0.7,0.375,0.9);
    leg1->AddEntry(g_Ie,"I_{e0}","p");
    leg1->AddEntry(g_Ih,"I_{h0}","p");
    g_Ie->Draw("ap");
    g_Ih->Draw("samep");
    leg1->Draw();

    c2->cd();
    c2->SetLogy();
    g_Re->GetYaxis()->SetRangeUser(1e-1,1e4);
    TLegend * leg2 = new TLegend(0.175,0.7,0.375,0.9);
    leg2->AddEntry(g_Re,"R_{e}","p");
    leg2->AddEntry(g_Rh,"R_{h}","p");
    g_Re->Draw("ap");
    g_Rh->Draw("samep");
    leg2->Draw();

    c3->cd();
//    c3->SetLogy();
    g_Ae->GetYaxis()->SetRangeUser(0,0.5);
    TLegend * leg3 = new TLegend(0.175,0.7,0.375,0.9);
    leg3->AddEntry(g_Ae,"A_{e}","p");
    leg3->AddEntry(g_Ah,"A_{h}","p");
    g_Ae->Draw("ap");
    g_Ah->Draw("samep");
    leg3->Draw();

    c1->SaveAs("/home/jens/Documents/data/all_plots/aaa_comparison/currents.pdf");
    c2->SaveAs("/home/jens/Documents/data/all_plots/aaa_comparison/DN_rates.pdf");
    c3->SaveAs("/home/jens/Documents/data/all_plots/aaa_comparison/AP_rates.pdf");


//    system("gedit ../../../data/IV_curves/2018-01-10/reverse/plots/calc_ratios.txt &");
    return 0;
}

