/*For changing the range accordingly to your needs, change line 55*/


#include <iostream>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <functional>
#include <cctype>
#include <locale>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <cstddef> 

#include <TFile.h> 
#include <TCanvas.h>
#include <TPad.h>
#include <TLegend.h>
#include <TH1F.h>
#include <TGraphErrors.h> 
#include <Riostream.h> 

using namespace std;

string GetDataFile(const int& RunNumber) {
  ifstream RunInfo;
  RunInfo.open("/home/exo/IV/RunInfoIV.txt");
  int RunID =0;
  string line, RunFile;
  while(getline(RunInfo, line, '\n')) {
	if(line[0]=='#') continue;
	istringstream linestream(line);
	linestream >> RunID >> RunFile;
	if(RunID == RunNumber) return RunFile;
  }
} 

string GetFileName(const string& FilePath) {
  size_t pos = FilePath.find_last_of("/");
  return FilePath.substr(pos+1);
}

void IV_comp(const int& RunStart, const int& RunEnd){

  const int totalfiles = RunEnd-RunStart+1;
  const int nFloatPerLine = 8;

  TCanvas *ct = new TCanvas("ct","IV-Scan");
  ct->SetGrid();
  ct->SetLogy();
  gStyle->SetOptStat(0);

  //draw a frame to define the range
  TH2F *hh = new TH2F("hh", "IV-Scan", 10, -45, -27, 10, 8e-11, 700e-6); //(..,xmin, xmax,...,ymin, ymax)
  hh->SetXTitle("Voltage in V");
  hh->SetYTitle("Current in A");
  hh->Draw();


  TGraphErrors* GIV = new TGraphErrors[totalfiles];
  TLegend* leg = new TLegend(0.7,0.7,0.95,0.95);
  
  ifstream fIn;
  
  int nPoint=0;
  double voltage[1000];
  double avCurrent[1000];
  double rmsCurrent[1000];
  double tmpCurrent;
  double nCurrent=9;
  int iFloat=1;
  char Dum[50];
  string filename;


 
  for(int i = 0; i < totalfiles; i++) {
	//open files
	filename = GetDataFile(RunStart+i);
	fIn.open(filename.c_str());
	printf("%s \n", filename.c_str());
	fIn >> Dum >> voltage[0];
	while(!fIn.eof()){
    	  avCurrent[nPoint]=0.;
    	  rmsCurrent[nPoint]=0.;
    	  for(int iCurrent=0; iCurrent<nCurrent; iCurrent++){
      		if(iFloat==nFloatPerLine){
		  fIn >> Dum;
		  iFloat=0;
      		}
      		fIn >> tmpCurrent;
      		avCurrent[nPoint] += tmpCurrent;
      		rmsCurrent[nPoint] += tmpCurrent*tmpCurrent;
      		iFloat++;
    	  }	
    	  avCurrent[nPoint]/=nCurrent;
    	  rmsCurrent[nPoint]=sqrt(rmsCurrent[nPoint]/nCurrent-
	  avCurrent[nPoint]*avCurrent[nPoint])/sqrt(nCurrent);
    	  nPoint++;
    	  if(iFloat==nFloatPerLine){
      	  	fIn >> Dum;
      	  	iFloat=0;
    	  }
    	  fIn >> voltage[nPoint];
    
    	  iFloat++;
  	}
 
  	*GIV = TGraphErrors(nPoint,voltage,avCurrent,0,rmsCurrent);

	GIV->SetMarkerSize(0.5);
	GIV->SetMarkerStyle(20);
	GIV->SetMarkerColor(i+1);
	leg->AddEntry(GIV, GetFileName(filename).c_str(), "lep");

  	GIV->Draw("p");
 	GIV++;

	fIn.close();
	nPoint = 0, iFloat = 1;
  	
  }

  leg->Draw();
  return;    
}
