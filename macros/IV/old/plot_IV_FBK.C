
/*

 This macro plots IV reverse.
 The file with the data must be formatted in this way:

 if aSame=1 leave the Canvas open in order to plot more plots in the same canvas from different files


 Created by
    G.Gallina
    giacomo@triumf.ca

 Modified by
    Jens Kroeger (March 2018)
    kroeger@physi.uni-heidelberg.de

 */
#include <stdio.h>
#define NFILE 1;
string filePath = "../../../data/IV_curves/2018-01-10/FBK_foreign/";
//string filePath = "";

// if you add a file here,
// don't forget to increment NFILE!!
string fileName[NFILE] = {
                          "W11_-40"
                         };

vector<string> temp;

//_____________________________________________________________________

double fit_func(double *x, double *par)
{   // no AP or CT
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t I0     = par[7];

    Double_t V = x[0]+Vbd;
    Double_t Vov = V-Vbd;
    Double_t I = I0e*Vov*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*Vov*(exp(kh*exp(-kh2/Vov))-1) + I0;
    return I;
}

double fit_func_AP(double *x, double *par)
{   // including AP but no CT
    /*
     * from Quoqing:
     * G_ap = 1+P_ap/(1-P_ap) where P_ap = (V-Vbd)/(Vra-Vbd)
     *
     * --> Wolfram Alpha:
     * http://www.wolframalpha.com/input/?i=1%2Bp%2F(1-p)+where+p%3D(x-a)%2F(b-a)
     *
     * G_ap = (Vra-Vbd)/(Vra-V)
     */
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t I0     = par[7];
    Double_t Vra    = par[8];

    Double_t V = x[0]+Vbd;
    Double_t Vov = V-Vbd;
    Double_t I = (I0e*Vov*(exp(ke*exp(-ke2/Vov))-1)+ I0h*Vov*(exp(kh*exp(-kh2/Vov))-1))
                *(Vra-Vbd)/(Vra-V) + I0;
    return I;
}

double fit_func_AP_CT(double *x, double *par)
{   // including AP but no CT
    /*
     * from Quoqing:
     * G_ap = 1+P_ap/(1-P_ap) where P_ap = (V-Vbd)/(Vra-Vbd)
     *
     * --> Wolfram Alpha:
     * http://www.wolframalpha.com/input/?i=1%2Bp%2F(1-p)+where+p%3D(x-a)%2F(b-a)
     *
     * G_ap = (Vra-Vbd)/(Vra-V)
     */
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t I0     = par[7];
    Double_t Vra    = par[8];
    Double_t kxt    = par[9];

    Double_t V = x[0]+Vbd; // in file: Vov instead of V
    Double_t Vov = V-Vbd;
    // this is (1+AP)(1+CT):
    Double_t I = (I0e*(exp(ke*exp(-ke2/Vov))-1)
//               + I0h*(exp(kh*exp(-kh2/Vov))-1))
                 + I0h*(kh*exp(-kh2/Vov)+(kh*exp(-kh2/Vov))**2
                   +(kh*exp(-kh2/Vov))**3 +(kh*exp(-kh2/Vov))**4))
//                   +(kh*exp(-kh2/Vov))**5))
//               * ((Vra-Vbd)/(Vra-V)*(2-exp(-kxt*Vov**2)));
                 * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return I;
}

void plot_IV_FBK(int try_fit=0, bool draw=0){

    if(try_fit==0){
        cout << "***********************************************\n"
                "* HOW TO USE:                                 *\n"
                "***********************************************\n"
                "* .x plot_IV_new.C(1) --> IV fit w/o AP or CT *\n"
                "* .x plot_IV_new.C(1) --> IV fit w/ AP w/o CT *\n"
                "* .x plot_IV_new.C(1) --> IV fit w/ AP & CT   *\n"
                "***********************************************\n";
    }
    //Declare variables
    Int_t nV=100;
    Double_t tV, tI, tI_err;

    temp.push_back("-40 C");
    temp.push_back("-60 C");
    temp.push_back("-80 C");
    temp.push_back("-95 C");
    temp.push_back("-110 C");

    ifstream in1[NFILE];
    TGraphErrors *g1[NFILE];
    TTree *t[NFILE];

    FILE *myfile;
    myfile = fopen((filePath+"plots/fitparameters.txt").c_str(),"w");
//    myfile = fopen("fitparameters.txt","w");

    //Loop to import all files
    for(int aWhich=0; aWhich<NFILE; aWhich++)
    {
        string file = filePath + fileName[aWhich] + ".csv";
        cout << "Opening file: " << file << endl;
        t[aWhich] = new TTree("tree", "tree");
        t[aWhich]->ReadFile(file.c_str(),"V:I:dI");

//        int n = t->Draw("V:I:dI","","");
//        g1[aWhich] = new TGraphErrors(n, t->GetV1(), t->GetV2(), 0, t->GetV3());
        int n = t[aWhich]->Draw("V:0.05*V:I:dI","","");
        g1[aWhich] = new TGraphErrors(n, t[aWhich]->GetV1(), t[aWhich]->GetV3(),
                                         t[aWhich]->GetV2(), t[aWhich]->GetV4());
        delete t[aWhich];

        g1[aWhich]->GetYaxis()->CenterTitle();
        g1[aWhich]->GetYaxis()->SetTitleOffset(1.5);
        g1[aWhich]->SetMarkerStyle(22);

    }
    for(int aWhich=0; aWhich<NFILE; aWhich++)
    {
        if(aWhich==0) // first plot
        {
            TCanvas *CDplot = new TCanvas("c1","c1"/*,10,10*/,1000,600);

            CDplot->SetBorderMode(0);
            CDplot->SetFillColor(0);
            CDplot->SetLeftMargin(0.12);
            CDplot->SetRightMargin(0.04);
            CDplot->SetBottomMargin(0.1);
//            CDplot->SetTopMargin(0.03);
            CDplot->Draw();
            CDplot->SetLogy();

            g1[aWhich]->GetXaxis()->SetTitle("Voltage [V]");
            g1[aWhich]->GetXaxis()->CenterTitle();
            g1[aWhich]->GetXaxis()->SetTitleOffset(1.2);
            g1[aWhich]->GetXaxis()->SetLimits(0,12);
            g1[aWhich]->GetYaxis()->SetRangeUser(1e-13,1e-4);

            g1[aWhich]->SetTitle("Unknown FBK device");
            g1[aWhich]->GetYaxis()->SetTitle("Current [A]");;
            g1[aWhich]->Draw("ap");
            TLegend * leg = new TLegend(0.175,0.7,0.375,0.9);

        }
        else // n-th plot (n>1)
        {
            continue;
            g1[aWhich]->SetMarkerStyle(22+aWhich);

            // avoid yellow
            if(aWhich<4) {
                g1[aWhich]->SetMarkerColor(1+aWhich);
                g1[aWhich]->SetLineColor(1+aWhich);
            }
            else {
                g1[aWhich]->SetMarkerColor(2+aWhich);
                g1[aWhich]->SetLineColor(2+aWhich);
            }
            g1[aWhich]->Draw("samep");
        }

        // add fit
        if(try_fit)
        {
            cout << endl;
            cout << " *** *** *** *** ** *** " << endl;
            cout << " *** TRY FIT *** " << endl;
            cout << " *** *** *** *** ** *** " << endl;
            if(aWhich==0){
                fprintf(myfile,"FIT FUNCTION: \n");
                if(try_fit==1)
                fprintf(myfile,"I = I0e*exp(ke*exp(-ke2/Vov))+ I0h*exp(kh*exp(-kh2/Vov))");
                if(try_fit==2)
                    fprintf(myfile,"I = (I0e*Vov*(exp(ke*exp(-ke2/Vov))-1)+ I0h*Vov*(exp(kh*exp(-kh2/Vov))-1))*(Vra-Vbd)/(Vra-V) + I0");
                if(try_fit==3)
                    fprintf(myfile,"I = (I0e*Vov*exp(ke*exp(-ke2/Vov)) + I0h*Vov*exp(kh*exp(-kh2/Vov)))*((Vra-Vbd)/(Vra-V)*(2-exp(-kxt*Vov**2))) + I0");

                fprintf(myfile,"\n\nTemp\tI0e\t\tke\t\tke2\t\tI0h\t\tkh\t\tkh2\t\t");
                fprintf(myfile,"Vbd\t\tI0\t\tVra\t\tkxt\t\tChi^2/NDF\n");
                fprintf(myfile,"\tI0e_err\t\tke_err\t\tke2_err");
                fprintf(myfile,"\t\tI0h_err\t\tkh_err\t\tkh2_err");
                fprintf(myfile,"\t\tVbd_err\t\tI0_err\t\tVra_err\t\tkxt_err\n");
                fprintf(myfile,"-------------------------------------------------");
                fprintf(myfile,"-------------------------------------------------");
                fprintf(myfile,"-------------------------------------------------");
                fprintf(myfile,"-------------------------------\n");
            }

            g1[aWhich]->SetTitle("");
            ROOT::Math::MinimizerOptions::SetDefaultMaxFunctionCalls(1e5);

            double fit_int[2];
            int NumCONST = 8;
            if(try_fit==1){
                NumCONST = 8;
                if(aWhich==0) {// -40C
                     fit_int[0] = 49;
                     fit_int[1] = 55;
                } else if(aWhich==1) { // -60C
                    fit_int[0] = 49;
                    fit_int[1] = 55;
                } else if(aWhich==2) { // -80C
                    fit_int[0] = 49;
                    fit_int[1] = 57;
                } else if(aWhich==3) { // -95C
                    fit_int[0] = 49;
                    fit_int[1] = 57;
                } else if(aWhich==4) { // -110C
                    fit_int[0] = 49;
                    fit_int[1] = 57;
                }
                TF1 *f1 = new TF1("f1",fit_func,49,fit_int[0],fit_int[1]);
            }
            if(try_fit==2) {
                NumCONST = 9;
                if(aWhich==0) {         // -40C
                    fit_int[0] = 0.2;//49.5-48.54;
                    fit_int[1] = 10;
                } else if(aWhich==1) {  // -60C
                    fit_int[0] = 49.5;
                    fit_int[1] = 58;
                } else if(aWhich==2) {  // -80C
                    fit_int[0] = 49.5;
                    fit_int[1] = 57;
                } else if(aWhich==3) { // -95C
                    fit_int[0] = 49;
                    fit_int[1] = 57;
                } else if(aWhich==4) { // -110C
                    fit_int[0] = 49;
                    fit_int[1] = 57;
                }
                TF1 *f1 = new TF1("f1",fit_func_AP,fit_int[0],fit_int[1],9);
            }
            if(try_fit==3) {
                NumCONST = 10;
                if(aWhich==0) {         // -40C
                     fit_int[0] = 0.1;//49.5-48.54;
                     fit_int[1] = 9;
//                } else if(aWhich==1) {  // -60C
//                    fit_int[0] = 48.5;
//                    fit_int[1] = 57.5;
//                } else if(aWhich==2) {  // -80C
//                    fit_int[0] = 47;
//                    fit_int[1] = 56.0;
//                } else if(aWhich==3) { // -95C
//                    fit_int[0] = 47;
//                    fit_int[1] = 55.5;
//                } else if(aWhich==4) { // -110C
//                    fit_int[0] = 46;
//                    fit_int[1] = 54;
                }
                TF1 *f1 = new TF1("f1",fit_func_AP_CT,fit_int[0],fit_int[1],10);
            }

            f1->SetParName(0,"Ie0");
            f1->SetParName(1,"ke");
            f1->SetParName(2,"ke2");
            f1->SetParName(3,"Ih0");
            f1->SetParName(4,"kh");
            f1->SetParName(5,"kh2");
            f1->SetParName(6,"Vbd");
            f1->SetParName(7,"I0"); // pre-breakdown
            f1->SetParName(8,"Vra");
            f1->SetParName(9,"kxt");

            //Set Constraints:
            f1->SetParLimits(0,0,1e-3);
            f1->SetParLimits(7,0,1); // I0 - pre-breakdown
            f1->SetParLimits(9,0,100000); // kxt

            if(aWhich==0) { // -40C
                f1->SetParameter(0,7e-12);   // Ie0
                f1->SetParameter(3,1.3e-11); // Ih0
                f1->FixParameter(1,0.5);  // ke --> like PDE
                f1->FixParameter(2,1);   // ke2 --> like PDE
                f1->SetParameter(4,4.39149); // kh --> like PDE
                f1->SetParameter(5,3.94998); // kh2 --> like PDE
                f1->SetParameter(6,48.);   // Vbd --> like PDE etc.
                f1->FixParameter(7,0);       // I0 -> pre-breakdown
                f1->SetParameter(8,58.8);    // Vra
                f1->SetParameter(9,0.012);    // kxt --> cross-talk
//                f1->SetParameter(0,7e-12);   // Ie0
//                f1->SetParameter(3,1.3e-11); // Ih0
//                f1->FixParameter(1,0.3423);  // ke --> like PDE
//                f1->FixParameter(2,0.415);   // ke2 --> like PDE
//                f1->FixParameter(4,4.39149); // kh --> like PDE
//                f1->FixParameter(5,3.94998); // kh2 --> like PDE
//                f1->FixParameter(6,48.45);   // Vbd --> like PDE etc.
//                f1->FixParameter(7,0);       // I0 -> pre-breakdown
//                f1->SetParameter(8,58.8);    // Vra
//                f1->SetParameter(9,1e-3);    // kxt --> cross-talk
            }
//            else if(aWhich==1) { // -60C
//                f1->SetParameter(0,1e-10);   // Ie0
//                f1->SetParameter(3,0.8e-10); // Ih0
//                f1->FixParameter(1,0.3423);  // ke --> like PDE
//                f1->FixParameter(2,0.415);   // ke2 --> like PDE
//                f1->FixParameter(4,3.2595);  // kh --> like PDE
//                f1->FixParameter(5,4.10026); // kh2 --> like PDE
//                f1->SetParameter(6,47.3302); // Vbd --> like PDE etc.
//                f1->FixParameter(7,0);       // I0 -> pre-breakdown
//                f1->SetParameter(8,57.8);    // Vra
//                f1->SetParameter(9,1e-3);    // kxt --> cross-talk
//            }
//            else if(aWhich==2) { // -80C
//                f1->SetParameter(0,5e-12);   // Ie0
//                f1->SetParameter(3,0.5e-11); // Ih0
//                f1->FixParameter(1,0.3423);  // ke --> like PDE
//                f1->FixParameter(2,0.415);   // ke2 --> like PDE
//                f1->FixParameter(4,4.14868); // kh --> like PDE
//                f1->FixParameter(5,5.25931); // kh2 --> like PDE
//                f1->SetParameter(6,46.1948); // Vbd --> like PDE etc.
//                f1->FixParameter(7,0);       // I0 -> pre-breakdown
//                f1->SetParameter(8,58.2);      // Vra
//                f1->SetParameter(9,1e-3);    // kxt --> cross-talk
//            }
//            else if(aWhich==3) { // -95C
//                f1->SetParameter(0,5e-12);   // Ie0
//                f1->SetParameter(3,0.5e-11); // Ih0
//                f1->FixParameter(1,0.3423);  // ke --> like PDE
//                f1->FixParameter(2,0.415);   // ke2 --> like PDE
//                f1->FixParameter(4,5.5316); // kh --> like PDE
//                f1->FixParameter(5,7.97512); // kh2 --> like PDE
//                f1->SetParameter(6,45.5453); // Vbd --> like PDE etc.
//                f1->FixParameter(7,0);       // I0 -> pre-breakdown
//                f1->SetParameter(8,58.2);      // Vra
//                f1->SetParameter(9,1e-3);    // kxt --> cross-talk
//            }
//            else if(aWhich==4) { // -110C
//                f1->SetParameter(0,5e-12);   // Ie0
//                f1->SetParameter(3,0.5e-11); // Ih0
//                f1->FixParameter(1,0.3423);  // ke --> like PDE
//                f1->FixParameter(2,0.415);   // ke2 --> like PDE
//                f1->FixParameter(4,6.35977); // kh --> like PDE
//                f1->FixParameter(5,8.03743); // kh2 --> like PDE
//                f1->SetParameter(6,44.8823); // Vbd --> like PDE etc.
//                f1->FixParameter(7,0);       // I0 -> pre-breakdown
//                f1->SetParameter(8,58.2);      // Vra
//                f1->SetParameter(9,1e-3);    // kxt --> cross-talk
//            }
            else cout << "JENS ERROR" << endl;

            if(draw)
                f1->Draw("same");
            else {
            g1[aWhich]->Fit("f1","R");
            f1 = g1[aWhich]->GetFunction("f1");
            }
//            if(aWhich<4-1) f1->SetLineColor(1+1+aWhich); // avoid yellow
//            else         f1->SetLineColor(2+1+aWhich);
            f1->SetLineColor(kGreen);

            cout << "\n  Chi^2/NDF = " << f1->GetChisquare() << "/"
                 << f1->GetNDF() << endl << endl;

            cout << "aWhich = " << aWhich << endl;
            fprintf(myfile,temp[aWhich].c_str());
            for(int j=0;j<NumCONST;j++)
                fprintf(myfile,"\t%.2e",f1->GetParameter(j));
            fprintf(myfile,"\n");
            for(int k=0;k<NumCONST;k++){
                if(f1->GetParError(k)==0)
                    fprintf(myfile,"\tfixed\t");
                else
                    fprintf(myfile,"\t%.2e",f1->GetParError(k));
            }

            fprintf(myfile,"\t%.2f/%i\n\n",f1->GetChisquare(),f1->GetNDF());
        } // end if(try_fit)

        // for all fits:
        gStyle->SetOptFit(0);

        // add entry to legend:
        leg->AddEntry(g1[aWhich],(temp[aWhich]).c_str(),"p");
        leg->Draw();

    } // end for(aWhich...)
    fclose(myfile);

    CDplot->SaveAs((filePath+"plots/IV.pdf").c_str());

} // end void plot_IV_new(bool try_fit=0);
