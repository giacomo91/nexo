
/*

 This macro plots IV reverse.
 The file with the data must be formatted in this way:

 if aSame=1 leave the Canvas open in order to plot more plots in the same canvas from different files


 Created by
    G.Gallina
    giacomo@triumf.ca

 Modified by
    Jens Kroeger (March 2018)
    kroeger@physi.uni-heidelberg.de

 */
#include <stdio.h>
#define NFILE 1;
string filePath = "../../../data/IV_curves/2018-01-10/reverse/";

string fileName[NFILE] = {
                          "IV_curve_-40C",
                         };

vector<string> temp;
vector<int> temp_int;

//_____________________________________________________________________


double fit_func_AP_CT_1(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    // this is (1+AP+CT):
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*(exp(kh*exp(-kh2/Vov))-1))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)))/* + 1e-11*/;
    return I;
}

double fit_func_AP_CT_2(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    // this is (1+AP)(1+CT):
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*(exp(kh*exp(-kh2/Vov))-1))
               * ((Vra-Vbd)/(Vra-V)*(2-exp(-kxt*Vov**2)))/* + 1e-11*/;
    return I;
}

double fit_func_AP_CT_3(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);
    Double_t P2 = P1-AlphaEDeltaS*exp(-AlphaEDeltaS);
    Double_t P3 = P2-AlphaEDeltaS**2 * exp(-AlphaEDeltaS)/2;
    Double_t P4 = P3-AlphaEDeltaS**3 * exp(-AlphaEDeltaS)/6;

    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*((P1+P2+P3)+(P1+P2+P3)**2))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return 2e3*I;
    }

double fit_func_AP_CT_4(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);
    Double_t P2 = P1-AlphaEDeltaS*exp(-AlphaEDeltaS);
    Double_t P3 = P2-AlphaEDeltaS**2 * exp(-AlphaEDeltaS)/2;
    Double_t P4 = P3-AlphaEDeltaS**3 * exp(-AlphaEDeltaS)/6;

    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*((P1+P2+P3)+(P1+P2+P3)**2+(P1+P2+P3)**3))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return 2e3*I;
    }

double fit_func_AP_CT_5(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);
    Double_t P2 = P1-AlphaEDeltaS*exp(-AlphaEDeltaS);
    Double_t P3 = P2-AlphaEDeltaS**2 * exp(-AlphaEDeltaS)/2;
    Double_t P4 = P3-AlphaEDeltaS**3 * exp(-AlphaEDeltaS)/6;

    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*((P1+P2+P3+P4)+(P1+P2+P3+P4)**2))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return 2e3*I;
    }

double fit_func_AP_CT_6(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);
    Double_t P2 = P1-AlphaEDeltaS*exp(-AlphaEDeltaS);
    Double_t P3 = P2-AlphaEDeltaS**2 * exp(-AlphaEDeltaS)/2;
    Double_t P4 = P3-AlphaEDeltaS**3 * exp(-AlphaEDeltaS)/6;

    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*((P1+P2+P3+P4)+(P1+P2+P3+P4)**2+(P1+P2+P3+P4)**3))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return I;
    }

double fit_func_AP_CT_7(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);
    Double_t P2 = P1-AlphaEDeltaS*exp(-AlphaEDeltaS);
    Double_t P3 = P2-AlphaEDeltaS**2 * exp(-AlphaEDeltaS)/2;
    Double_t P4 = P3-AlphaEDeltaS**3 * exp(-AlphaEDeltaS)/6;
    Double_t P5 = P4-AlphaEDeltaS**4 * exp(-AlphaEDeltaS)/24;

    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*((P1+P2+P3+P4+P5)+(P1+P2+P3+P4+P5)**2))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return I;
}

double fit_func_AP_CT_8(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);
    Double_t P2 = P1-AlphaEDeltaS*exp(-AlphaEDeltaS);
    Double_t P3 = P2-AlphaEDeltaS**2 * exp(-AlphaEDeltaS)/2;
    Double_t P4 = P3-AlphaEDeltaS**3 * exp(-AlphaEDeltaS)/6;
    Double_t P5 = P4-AlphaEDeltaS**4 * exp(-AlphaEDeltaS)/24;

    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*((P1+P2+P3+P4+P5)+(P1+P2+P3+P4+P5)**2+(P1+P2+P3+P4+P5)**3))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return 2e3*I;
}

double fit_func_AP_CT_9(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);
    Double_t P2 = P1-AlphaEDeltaS*exp(-AlphaEDeltaS);
    Double_t P3 = P2-AlphaEDeltaS**2 * exp(-AlphaEDeltaS)/2;

    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*((P1+P2+P3)+(P1+P2+P3)**2+(P1+P2+P3)**3+(P1+P2+P3)**4))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return I;
}

double fit_func_AP_CT_10(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);
    Double_t P2 = P1-AlphaEDeltaS*exp(-AlphaEDeltaS);

    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*((P1+P2)+(P1+P2)**2))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return I;
}

double fit_func_AP_CT_11(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);
    Double_t P2 = P1-AlphaEDeltaS*exp(-AlphaEDeltaS);

    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*((P1+P2)+(P1+P2)**2+(P1+P2)**3))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return I;
}

double fit_func_AP_CT_12(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);

    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*(P1+P1**2))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return 2e3*I;
}

double fit_func_AP_CT_13(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);

    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*(P1+P1**2+P1**3))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return I;
}

double fit_func_AP_CT_14(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);

    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*(P1))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return I;
}

double fit_func_AP_CT_15(double *x, double *par)
{
    Double_t I0e    = par[0];
    Double_t ke     = par[1];
    Double_t ke2    = par[2];
    Double_t I0h    = par[3];
    Double_t kh     = par[4];
    Double_t kh2    = par[5];
    Double_t Vbd    = par[6];
    Double_t Vra    = par[7];
    Double_t kxt    = par[8];
    Double_t be     = par[9];
    Double_t bh     = par[10];

    Double_t V = x[0];
    Double_t Vov = V-Vbd;

    Double_t AlphaEDeltaS = ke*exp(-ke2/Vov);
    Double_t P1 = 1-exp(-AlphaEDeltaS);
    Double_t P2 = P1-AlphaEDeltaS*exp(-AlphaEDeltaS);
    Double_t P3 = P2-AlphaEDeltaS**2 * exp(-AlphaEDeltaS)/2;
    Double_t P4 = P3-AlphaEDeltaS**3 * exp(-AlphaEDeltaS)/6;
    Double_t P5 = P4-AlphaEDeltaS**4 * exp(-AlphaEDeltaS)/24;


    // this is the NEW approach:
    Double_t I = (I0e*exp(be*V)*(exp(ke*exp(-ke2/Vov))-1)
               + I0h*exp(bh*V)*(P1+P2+P3+P4+P5))
               * ((Vra-Vbd)/(Vra-V)+1-exp(-kxt*(Vov**2)));
    return I;
}

void plot_IV_VUV4_compare(int try_fit=0, bool draw=0){

    //Declare variables
    Int_t nV=100;
    Double_t tV, tI, tI_err;

    temp.push_back("-40 C");
    temp_int.push_back(-40);

    ifstream in1[NFILE];
    TGraphErrors *g1[NFILE];
    TTree *t[NFILE];

    FILE *myfile;
    FILE *myfile2;
    myfile = fopen((filePath+"plots/fitparameters.txt").c_str(),"w");
    myfile2 = fopen((filePath+"/V_vs_T.txt").c_str(),"w");
//    myfile = fopen("fitparameters.txt","w");

    //Loop to import all files
    for(int aWhich=0; aWhich<NFILE; aWhich++)
    {
        string file = filePath + fileName[aWhich] + ".csv";
        cout << "Opening file: " << file << endl;
        t[aWhich] = new TTree("tree", "tree");
        t[aWhich]->ReadFile(file.c_str(),"V:I:dI");
//        int n = t->Draw("V:I:dI","","");
//        g1[aWhich] = new TGraphErrors(n, t->GetV1(), t->GetV2(), 0, t->GetV3());
        int n = t[aWhich]->Draw("V:0.002*V:I:dI","","");
        g1[aWhich] = new TGraphErrors(n, t[aWhich]->GetV1(), t[aWhich]->GetV3(),
                                         t[aWhich]->GetV2(), t[aWhich]->GetV4());
        delete t[aWhich];

        g1[aWhich]->GetYaxis()->CenterTitle();
        g1[aWhich]->GetYaxis()->SetTitleOffset(1.5);
        g1[aWhich]->SetMarkerStyle(22);

    }
    for(int aWhich=0; aWhich<NFILE; aWhich++)
    {
        if(aWhich==0) // first plot
        {
            TCanvas *CDplot = new TCanvas("c1","c1"/*,10,10*/,1000,600);

            CDplot->SetBorderMode(0);
            CDplot->SetFillColor(0);
            CDplot->SetLeftMargin(0.12);
            CDplot->SetRightMargin(0.04);
            CDplot->SetBottomMargin(0.1);
//            CDplot->SetTopMargin(0.03);
            CDplot->Draw();
            CDplot->SetLogy();

            g1[aWhich]->GetXaxis()->SetTitle("Voltage [V]");
            g1[aWhich]->GetXaxis()->CenterTitle();
            g1[aWhich]->GetXaxis()->SetTitleOffset(1.2);
            g1[aWhich]->GetXaxis()->SetLimits(48,60);
            g1[aWhich]->GetYaxis()->SetRangeUser(1e-11,1e-5);

            g1[aWhich]->SetTitle("");
            g1[aWhich]->GetYaxis()->SetTitle("Current [A]");;
            g1[aWhich]->Draw("ap");
//            CDplot->SetLogy();
            TLegend * leg = new TLegend(0.15,0.55,0.5,0.9);

        }

            cout << endl;
            cout << " *** *** *** *** ** *** *** *** *** ***" << endl;
            cout << " *** COMPARING FIT FUNCTIONS: " << temp[aWhich]
                 << " *** " << endl;
            cout << " *** *** *** *** ** *** *** *** *** ***" << endl;

            g1[aWhich]->SetTitle("");
            ROOT::Math::MinimizerOptions::SetDefaultMaxFunctionCalls(1e5);

            double fit_int[2];
            int NumCONST = 0;

            if(try_fit==3) {
                NumCONST = 11;//9;
                if(aWhich==0) {         // -40C
                     fit_int[0] = 48.8;
                     fit_int[1] = 59.5;
                }

                TF1 *f1 = new TF1("",fit_func_AP_CT_1,fit_int[0],fit_int[1],NumCONST);
                TF1 *f2 = new TF1("f2",fit_func_AP_CT_2,fit_int[0],fit_int[1],NumCONST);
                TF1 *f3 = new TF1("f3",fit_func_AP_CT_3,fit_int[0],fit_int[1],NumCONST);
                TF1 *f4 = new TF1("f4",fit_func_AP_CT_4,fit_int[0],fit_int[1],NumCONST);
                TF1 *f5 = new TF1("f5",fit_func_AP_CT_5,fit_int[0],fit_int[1],NumCONST);
                TF1 *f6 = new TF1("f6",fit_func_AP_CT_6,fit_int[0],fit_int[1],NumCONST);
                TF1 *f7 = new TF1("f7",fit_func_AP_CT_7,fit_int[0],fit_int[1],NumCONST);
                TF1 *f8 = new TF1("f8",fit_func_AP_CT_8,fit_int[0],fit_int[1],NumCONST);
                TF1 *f9 = new TF1("f9",fit_func_AP_CT_9,fit_int[0],fit_int[1],NumCONST);
                TF1 *f10 = new TF1("f10",fit_func_AP_CT_10,fit_int[0],fit_int[1],NumCONST);
                TF1 *f11 = new TF1("f11",fit_func_AP_CT_11,fit_int[0],fit_int[1],NumCONST);
                TF1 *f12 = new TF1("f12",fit_func_AP_CT_12,fit_int[0],fit_int[1],NumCONST);
                TF1 *f13 = new TF1("f13",fit_func_AP_CT_13,fit_int[0],fit_int[1],NumCONST);
                TF1 *f14 = new TF1("f14",fit_func_AP_CT_14,fit_int[0],fit_int[1],NumCONST);
                TF1 *f15 = new TF1("f15",fit_func_AP_CT_15,fit_int[0],fit_int[1],NumCONST);
            }

            if(aWhich==0) { // -40C
                f1->SetParameter(0,1.07473e-12);   // Ie0
                f1->SetParameter(3,1.74049e-14); // Ih0
                f1->FixParameter(1,0.3423);  // ke --> like PDE
                f1->FixParameter(2,0.415);   // ke2 --> like PDE
                f1->SetParameter(4,7.19880e-01); // kh --> like PDE
                f1->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f1->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f1->SetParameter(7,60);    // Vra
                f1->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f1->SetParameter(9,0);
                f1->SetParameter(10,0);

                f2->SetParameter(0,1.07473e-12);   // Ie0
                f2->SetParameter(3,1.74049e-14); // Ih0
                f2->FixParameter(1,0.3423);  // ke --> like PDE
                f2->FixParameter(2,0.415);   // ke2 --> like PDE
                f2->SetParameter(4,7.19880e-01); // kh --> like PDE
                f2->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f2->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f2->SetParameter(7,60);    // Vra
                f2->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f2->SetParameter(9,0);
                f2->SetParameter(10,0);

                f3->SetParameter(0,1.07473e-12);   // Ie0
                f3->SetParameter(3,1.74049e-14); // Ih0
                f3->FixParameter(1,0.3423);  // ke --> like PDE
                f3->FixParameter(2,0.415);   // ke2 --> like PDE
                f3->SetParameter(4,7.19880e-01); // kh --> like PDE
                f3->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f3->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f3->SetParameter(7,60);    // Vra
                f3->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f3->SetParameter(9,0);
                f3->SetParameter(10,0);

                f4->SetParameter(0,1.07473e-12);   // Ie0
                f4->SetParameter(3,1.74049e-14); // Ih0
                f4->FixParameter(1,0.3423);  // ke --> like PDE
                f4->FixParameter(2,0.415);   // ke2 --> like PDE
                f4->SetParameter(4,7.19880e-01); // kh --> like PDE
                f4->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f4->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f4->SetParameter(7,60);    // Vra
                f4->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f4->SetParameter(9,0);
                f4->SetParameter(10,0);

                f5->SetParameter(0,1.07473e-12);   // Ie0
                f5->SetParameter(3,1.74049e-14); // Ih0
                f5->FixParameter(1,0.3423);  // ke --> like PDE
                f5->FixParameter(2,0.415);   // ke2 --> like PDE
                f5->SetParameter(4,7.19880e-01); // kh --> like PDE
                f5->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f5->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f5->SetParameter(7,60);    // Vra
                f5->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f5->SetParameter(9,0);
                f5->SetParameter(10,0);

                f6->SetParameter(0,1.07473e-12);   // Ie0
                f6->SetParameter(3,1.74049e-14); // Ih0
                f6->FixParameter(1,0.3423);  // ke --> like PDE
                f6->FixParameter(2,0.415);   // ke2 --> like PDE
                f6->SetParameter(4,7.19880e-01); // kh --> like PDE
                f6->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f6->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f6->SetParameter(7,60);    // Vra
                f6->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f6->SetParameter(9,0);
                f6->SetParameter(10,0);

                f7->SetParameter(0,1.07473e-12);   // Ie0
                f7->SetParameter(3,1.74049e-14); // Ih0
                f7->FixParameter(1,0.3423);  // ke --> like PDE
                f7->FixParameter(2,0.415);   // ke2 --> like PDE
                f7->SetParameter(4,7.19880e-01); // kh --> like PDE
                f7->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f7->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f7->SetParameter(7,60);    // Vra
                f7->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f7->SetParameter(9,0);
                f7->SetParameter(10,0);

                f8->SetParameter(0,1.07473e-12);   // Ie0
                f8->SetParameter(3,1.74049e-14); // Ih0
                f8->FixParameter(1,0.3423);  // ke --> like PDE
                f8->FixParameter(2,0.415);   // ke2 --> like PDE
                f8->SetParameter(4,7.19880e-01); // kh --> like PDE
                f8->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f8->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f8->SetParameter(7,60);    // Vra
                f8->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f8->SetParameter(9,0);
                f8->SetParameter(10,0);

                f9->SetParameter(0,1.07473e-12);   // Ie0
                f9->SetParameter(3,1.74049e-14); // Ih0
                f9->FixParameter(1,0.3423);  // ke --> like PDE
                f9->FixParameter(2,0.415);   // ke2 --> like PDE
                f9->SetParameter(4,7.19880e-01); // kh --> like PDE
                f9->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f9->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f9->SetParameter(7,60);    // Vra
                f9->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f9->SetParameter(9,0);
                f9->SetParameter(10,0);

                f10->SetParameter(0,1.07473e-12);   // Ie0
                f10->SetParameter(3,1.74049e-14); // Ih0
                f10->FixParameter(1,0.3423);  // ke --> like PDE
                f10->FixParameter(2,0.415);   // ke2 --> like PDE
                f10->SetParameter(4,7.19880e-01); // kh --> like PDE
                f10->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f10->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f10->SetParameter(7,60);    // Vra
                f10->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f10->SetParameter(9,0);
                f10->SetParameter(10,0);

                f11->SetParameter(0,1.07473e-12);   // Ie0
                f11->SetParameter(3,1.74049e-14); // Ih0
                f11->FixParameter(1,0.3423);  // ke --> like PDE
                f11->FixParameter(2,0.415);   // ke2 --> like PDE
                f11->SetParameter(4,7.19880e-01); // kh --> like PDE
                f11->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f11->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f11->SetParameter(7,60);    // Vra
                f11->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f11->SetParameter(9,0);
                f11->SetParameter(10,0);

                f12->SetParameter(0,1.07473e-12);   // Ie0
                f12->SetParameter(3,1.74049e-14); // Ih0
                f12->FixParameter(1,0.3423);  // ke --> like PDE
                f12->FixParameter(2,0.415);   // ke2 --> like PDE
                f12->SetParameter(4,7.19880e-01); // kh --> like PDE
                f12->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f12->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f12->SetParameter(7,60);    // Vra
                f12->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f12->SetParameter(9,0);
                f12->SetParameter(10,0);

                f13->SetParameter(0,1.07473e-12);   // Ie0
                f13->SetParameter(3,1.74049e-14); // Ih0
                f13->FixParameter(1,0.3423);  // ke --> like PDE
                f13->FixParameter(2,0.415);   // ke2 --> like PDE
                f13->SetParameter(4,7.19880e-01); // kh --> like PDE
                f13->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f13->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f13->SetParameter(7,60);    // Vra
                f13->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f13->SetParameter(9,0);
                f13->SetParameter(10,0);

                f14->SetParameter(0,1.07473e-12);   // Ie0
                f14->SetParameter(3,1.74049e-14); // Ih0
                f14->FixParameter(1,0.3423);  // ke --> like PDE
                f14->FixParameter(2,0.415);   // ke2 --> like PDE
                f14->SetParameter(4,7.19880e-01); // kh --> like PDE
                f14->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f14->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f14->SetParameter(7,60);    // Vra
                f14->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f14->SetParameter(9,0);
                f14->SetParameter(10,0);

                f15->SetParameter(0,1.07473e-12);   // Ie0
                f15->SetParameter(3,1.74049e-14); // Ih0
                f15->FixParameter(1,0.3423);  // ke --> like PDE
                f15->FixParameter(2,0.415);   // ke2 --> like PDE
                f15->SetParameter(4,7.19880e-01); // kh --> like PDE
                f15->SetParameter(5,2.92539e+00); // kh2 --> like PDE
                f15->SetParameter(6,48.3445);   // Vbd --> like PDE etc.
                f15->SetParameter(7,60);    // Vra
                f15->FixParameter(8,1.07621e-02);    // kxt --> cross-talk
                f15->SetParameter(9,0);
                f15->SetParameter(10,0);

                f1->SetLineColor(kRed);
                f2->SetLineColor(kBlue);
                f3->SetLineColor(kGreen);
                f4->SetLineColor(kOrange);
                f5->SetLineColor(kCyan);
                f6->SetLineColor(kMagenta);
                f7->SetLineColor(8);
                f8->SetLineColor(46);
                f9->SetLineColor(kBlack);
                f10->SetLineColor(kPink);
                f11->SetLineColor(kPink-1);
                f12->SetLineColor(kYellow);
                f13->SetLineColor(kGray);
                f14->SetLineColor(kAzure+10);
                f15->SetLineColor(kYellow+2);

            }
//           f1->Draw("same");
//           f2->Draw("same");
           f3->Draw("same");
           f4->Draw("same");
           f5->Draw("same");
//           f6->Draw("same");
//           f7->Draw("same");
           f8->Draw("same");
//           f9->Draw("same");
//           f10->Draw("same");
//           f11->Draw("same");
           f12->Draw("same");
//           f13->Draw("same");
//           f14->Draw("same");
//           f15->Draw("same");

           leg->AddEntry(f1,"f1: (1+AP+CT)","l");
           leg->AddEntry(f2,"f2: (1+AP)(1+CT)","l");
           leg->AddEntry(f3,"f3: (P1+P2+P3)+(P1+P2+P3)^2","l");
           leg->AddEntry(f4,"f4: (P1+P2+P3)+...+(P1+P2+P3)^3","l");
           leg->AddEntry(f5,"f5: (P1+P2+P3+P4)+(P1+P2+P3+P4)^2","l");
           leg->AddEntry(f6,"f6: (P1+P2+P3+P4)+...+(P1+P2+P3+P4)^3","l");
           leg->AddEntry(f7,"f7: (P1+P2+P3+P4+P5)+(P1+P2+P3+P4+P5)^2","l");
           leg->AddEntry(f8,"f8: (P1+P2+P3+P4+P5)+...+(P1+P2+P3+P4+P5)^3","l");
           leg->AddEntry(f9,"f9: (P1+P2+P3)+...+(P1+P2+P3)^4","l");
           leg->AddEntry(f10,"f10: (P1+P2)+(P1+P2)^2","l");
           leg->AddEntry(f11,"f11: (P1+P2)+...+(P1+P2)^3","l");
           leg->AddEntry(f12,"f12: (P1+P1^2)","l");
           leg->AddEntry(f13,"f13: (P1+P1^2+P1^3)","l");
           leg->AddEntry(f14,"f14: (P1)","l");
           leg->AddEntry(f15,"f15: (P1+P2+P3+P4+P5)","l");
           leg->Draw();

//           TCanvas *c2 = new TCanvas("c2","c2"/*,10,10*/,1000,600);
//           c2->cd();
//           c2->SetLogy();
////           f6->Draw();
//           f7->Draw("");
////           f8->Draw("same");

    } // end for(aWhich...)
    fclose(myfile);
    fclose(myfile2);

} // end void plot_IV_new(bool try_fit=0);
