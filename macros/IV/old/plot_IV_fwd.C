// -- -- -- -- -- -- -- -- -- -- -- -- //
// -- -- -- Plotting IV scans -- -- -- //
// -- -- -- -- FORWARD BIAS -- -- -- - //
// -- -- -- -- -- -- -- -- -- -- -- -- //

/**********************************
 *          Jens Kroeger          *
 *  kroeger@physi.uni-heidelberg  *
 *          January, 2018         *
 **********************************
 *
 * HOW TO USE:
 * in a terminal type
 *      root -l
 *      .L plot_IV_fwd.C
 *      run()
*/
#include <string>

// ** ** ** ** ** ** //
// GLOBAL VARIABLES: //
// ** ** ** ** ** ** //
const string my_path = "../../../data/IV_curves/2018-01-10/forward/";

const int NPAR = 3;
const int NPAR_IV = 3;

// fit inverted function: voltage vs. current
Double_t fit_int[2]     = {0.1e-10,1e-1};
//Double_t fit_par[NPAR]  = {0.025,2e-16,35};
Double_t fit_par[NPAR]  = {40,1e-16,102}; // values from Luca--> don't make any sense!

// fit regular function: current vs. voltage
Double_t fit_int_IV[2] = {0.3,0.7};
Double_t fit_par_IV[NPAR_IV] = {1e-16,1e-16,40};

// ** ** ** ** //
// FUNCTIONS:  //
// ** ** ** ** //

Double_t VI_func(double *x, Double_t *par)
{
    Double_t K  = par[0];
    Double_t Is = par[1];
    Double_t R  = par[2];
//    Double_t V0 = par[3];

    Double_t I = x[0];

    // from "I-V Characteristics of Silicon Photomultipliers.pptx"
    // -- Luca Doria, Guoqing, Giacomo
    //          Luca: I0 instead of Is
    Double_t V = 0;
    /*if(Is>0)*/ V = K*log(I/Is +1) + R*I ;
//    else     V = 0;

    return V;
}

Double_t IV_func(double *x, Double_t *par)
{
    Double_t a = par[0];
    Double_t b = par[1];
    Double_t c = par[2];

    Double_t V = x[0];

    return a*exp(c*V) + b*exp(c*V/2);
}

/*
void test()
{
    TCanvas *c1 = new TCanvas("c1","",800,600);
    TF1 *f1 = new TF1("myfunc1",VI_func,fit_int[0],fit_int[1],NPAR);
    for(int i=0; i<NPAR; i++) f1->SetParameter(i,fit_par[i]);

    f1->SetTitle("Forward Bias - Luca's values for -60C");
    f1->GetXaxis()->SetTitle("Current [A]");
    f1->GetYaxis()->SetTitle("Voltage [V]");
    f1->Draw();
    gStyle->SetOptStat(1);
    c1->SetLogx();
    TLegend * leg = new TLegend(0.2,0.75,0.6,0.9);
    leg->AddEntry(f1,"V = K*log(I/Is +1) + R*I","l");
    leg->Draw();

    return;
}
*/
/*
void bla() // testing stuff
{
    TCanvas *c1 = new TCanvas("c1","",800,600);
    TF1 *f1 = new TF1("lin","20*(x)",fit_int[0],fit_int[1]);                // red  = lin
    TF1 *f2 = new TF1("log","0.028*log(x/3e-16 +1)+0.05",fit_int[0],fit_int[1]); // green= log

    c1->SetLogx();
    f2->SetLineColor(kGreen);
    f1->Draw();
    f2->Draw("same");

    return;
}
*/

/*
int plot(string file, string file2)
{
    TCanvas *c1 = new TCanvas("","",800,600);

    TTree *t = new TTree("tree", "tree");
    t->ReadFile(file.c_str(),"V:I:dI");
    int n = t->Draw("-V:-I","","");
    TGraph *g1 = new TGraph(n, t->GetV1(), t->GetV2());
    g1->Draw("ap");
    g1->SaveAs(file2.c_str());
    delete t;
    return 0;
}
*/


int plot_err(string file, string file2,Double_t fit_int[2], Double_t fit_par[NPAR_IV], bool try_fit=0)
{
    TCanvas *c1 = new TCanvas("","",800,600);

    TTree *t = new TTree("tree", "tree");
    t->ReadFile(file.c_str(),"V:I:dI");
    int n = t->Draw("-V:-I:dI","","");
    TGraphErrors *g1 = new TGraphErrors(n, t->GetV1(), t->GetV2(), 0, t->GetV3());
    g1->Draw("ap");

    if(try_fit)
    {
        TF1 *func1 = new TF1("IV_func",IV_func,fit_int[0],fit_int[1],NPAR_IV);
        for(int i=0; i<NPAR_IV; i++) func1->SetParameter(i,fit_par[i]);

//        func1->SetParLimits(0,0,10000);     // a
//        func1->SetParLimits(1,0,10000);     // b
//        func1->SetParLimits(2,0,10000);     // c
        func1->SetParName(0,"a");
        func1->SetParName(1,"b");
        func1->SetParName(2,"c");

        g1->Fit("IV_func","RM");
        func1 = g1->GetFunction("IV_func");

    } // end try_fit_IV

    g1->SaveAs(file2.c_str());
    delete t;
    return 0;
}

int plot_err_inv(string file, string file2,Double_t fit_int[2], Double_t fit_par[NPAR], bool try_fit=0)
{
    TTree *t = new TTree("tree", "tree");
    t->ReadFile(file.c_str(),"V:I:dI");
    int n = t->Draw("-V:-I:300*dI","","");                       // V4 is FAKE for testing!
    TGraphErrors *g1 = new TGraphErrors(n, t->GetV2(), t->GetV1(), t->GetV3(), /*t->GetV4()*/0);
    g1->Draw("ap");

    if(try_fit)
    {
        TF1 *func1 = new TF1("VI_func",VI_func,fit_int[0],fit_int[1],NPAR);
        for(int i=0; i<NPAR; i++) func1->SetParameter(i,fit_par[i]);

        func1->SetParLimits(0,0,10000);     // K
        func1->SetParLimits(1,0,10000);     // Is
        func1->SetParLimits(2,0,10000);     // R
        func1->SetParName(0,"K");
        func1->SetParName(1,"Is");
        func1->SetParName(2,"R");
        func1->SetParName(3,"V0");

        // fit graph in range defined by function
//        func1->SetNpx(1e7);
        g1->Fit("VI_func","RM");

        // access fit results
        func1 = g1->GetFunction("VI_func");
    }

    g1->SaveAs(file2.c_str());
    delete t;
    return 0;
}

/*
int draw(string my_file)
{
    string infile = my_path+my_file+".csv";
    string outfile = my_path+my_file+".root";
    cout << "outfile = " << outfile << endl;

    plot(infile, outfile);

    TFile f1(outfile.c_str(),"READ");
    TGraph *g1 = (TGraph*) f1.Get("Graph");

    TCanvas *c1 = new TCanvas("c1","c1",800,450);
    g1->SetTitle("test title");
    g1->GetYaxis()->SetTitle("I [A]");
    g1->GetXaxis()->SetTitle("V [V]");
    g1->SetMarkerColor(kRed);
    g1->SetMarkerStyle(22);
    g1->GetXaxis()->SetTitleSize(0.05);
    g1->GetXaxis()->SetTitleOffset(0.9);
    g1->GetYaxis()->SetTitleOffset(0.9);
    g1->GetXaxis()->SetLabelSize(0.05);
    g1->GetYaxis()->SetTitleSize(0.05);
    g1->GetYaxis()->SetLabelSize(0.05);

    g1->SetTitle("");
    g1->Draw("ap");

    TLegend * leg = new TLegend(0.2,0.75,0.4,0.9);
    leg->AddEntry(g1,my_file.c_str(),"p");
    leg->Draw();

    c1->SetLogy();
    c1->SaveAs((my_path+"plots/"+my_file+".pdf").c_str());
}
*/

int draw_err(string my_file, Double_t fit_int[2], Double_t fit_par[NPAR_IV], bool try_fit)
{
    string infile = my_path+my_file+".csv";
    string outfile = my_path+"test.root";
    cout << "outfile = " << outfile << endl;

    cout << "fit_int: " << fit_int[0] << " to " << fit_int[1] << endl;
    plot_err(infile, outfile, fit_int, fit_par, try_fit);

    TFile f1(outfile.c_str(),"READ");
    TGraphErrors *g1 = (TGraphErrors*) f1.Get("Graph");

    TCanvas *c1 = new TCanvas("c1","c1",800,600);
    g1->SetTitle("VUV4: Current vs. Voltage (forward bias)");
    g1->GetYaxis()->SetTitle("I [A]");
    g1->GetXaxis()->SetTitle("V [V]");
    g1->SetMarkerColor(kBlack);
    g1->SetMarkerStyle(20);
    g1->SetMarkerSize(0.5);
    g1->GetXaxis()->SetTitleSize(0.05);
    g1->GetXaxis()->SetTitleOffset(0.9);
    g1->GetYaxis()->SetTitleOffset(0.9);
    g1->GetXaxis()->SetLabelSize(0.05);
    g1->GetYaxis()->SetTitleSize(0.05);
    g1->GetYaxis()->SetLabelSize(0.05);

    g1->SetTitle("");
    g1->Draw("ap");

    TLegend * leg = new TLegend(0.2,0.75,0.4,0.9);
    leg->AddEntry(g1,my_file.c_str(),"p");
    leg->Draw();

    gStyle->SetOptFit();
    gStyle->SetStatY(0.4); // Set y-position (fraction of pad size)
    gStyle->SetStatX(0.9); // Set x-position (fraction of pad size)
    gStyle->SetStatW(0.2); // Set width of stat-box (fraction of pad size)
    gStyle->SetStatH(0.15); // Set height of stat-box (fraction of pad size)

    cout << "DRAW FUNCTION!" << endl;
    TF1 *f2 = new TF1("drawFunc1",IV_func,fit_int[0],fit_int[1],NPAR_IV);
    for(int i=0; i<NPAR_IV; i++) f2->SetParameter(i,fit_par[i]);

    f2->SetLineColor(kBlue);
    f2->Draw("same");
    cout << "after drawing" << endl;

    c1->SetLogy();
    c1->SaveAs((my_path+"plots/"+my_file+".pdf").c_str());

}

int draw_err_inv(string my_file, Double_t fit_int[2], Double_t fit_par[NPAR], bool try_fit)
{
    string infile = my_path+my_file+".csv";
    string outfile = my_path+my_file+"_inv.root";
    cout << "outfile = " << outfile << endl;

    plot_err_inv(infile, outfile, fit_int, fit_par, try_fit);

    TFile f1(outfile.c_str(),"READ");
    TGraphErrors *g1 = (TGraphErrors*) f1.Get("Graph");

    TCanvas *c1 = new TCanvas("c1","c1",800,450);
    g1->SetTitle("VUV4: Voltage vs. Current (forward bias)");
    g1->GetXaxis()->SetTitle("I [A]");
    g1->GetYaxis()->SetTitle("V [V]");
    g1->SetMarkerColor(kBlack);
    g1->SetMarkerStyle(20);
    g1->SetMarkerSize(0.5);
    g1->GetXaxis()->SetTitleSize(0.05);
    g1->GetXaxis()->SetTitleOffset(0.9);
    g1->GetYaxis()->SetTitleOffset(0.9);
    g1->GetXaxis()->SetLabelSize(0.05);
    g1->GetYaxis()->SetTitleSize(0.05);
    g1->GetYaxis()->SetLabelSize(0.05);

    g1->Draw("ap");
    g1->GetXaxis()->SetLimits(1e-11,1e-1);
    c1->SetLogx();

    TLegend * leg = new TLegend(0.2,0.75,0.4,0.9);
    leg->AddEntry(g1,my_file.c_str(),"p");
    leg->Draw();

    gStyle->SetOptFit();
    gStyle->SetStatY(0.9); // Set y-position (fraction of pad size)
    gStyle->SetStatX(0.8); // Set x-position (fraction of pad size)
    gStyle->SetStatW(0.2); // Set width of stat-box (fraction of pad size)
    gStyle->SetStatH(0.15); // Set height of stat-box (fraction of pad size)

    TF1 *f2 = new TF1("drawFunc1",VI_func,fit_int[0],fit_int[1],NPAR);
    for(int i=0; i<NPAR; i++) f2->SetParameter(i,fit_par[i]);

    f2->SetLineColor(kBlue);
    f2->Draw("same");

//    c1->SetLogy();
    c1->SaveAs((my_path+"plots/"+my_file+".pdf").c_str());

}

int draw_err_all()
{
    vector<string> my_file;
    vector<string> infile;
    vector<string> outfile;

    my_file.push_back("IV_curve_-110C_fwd");
    my_file.push_back("IV_curve_-95C_fwd");
    my_file.push_back("IV_curve_-80C_fwd");
    my_file.push_back("IV_curve_-60C_fwd");
    my_file.push_back("IV_curve_-40C_fwd");
    my_file.push_back("IV_curve_-20C_fwd");
    my_file.push_back("IV_curve_0C_fwd");
    my_file.push_back("IV_curve_20C_fwd_v2");

    for(int i=0; i<my_file.size(); i++)
    {
        cout << "entry " << i << ": " << my_file[i] << endl;
        infile.push_back(my_path+my_file[i]+".csv");
        outfile.push_back(my_path+my_file[i]+".root");
        cout << outfile[i] << endl;

        continue;
        plot_err(infile[i], outfile[i]);
    }

    TFile f1(outfile[0].c_str(),"READ");
    TGraphErrors * g1 = (TGraphErrors*) f1.Get("Graph");
    TFile f2(outfile[1].c_str(),"READ");
    TGraphErrors * g2 = (TGraphErrors*) f2.Get("Graph");
    TFile f3(outfile[2].c_str(),"READ");
    TGraphErrors * g3 = (TGraphErrors*) f3.Get("Graph");
    TFile f4(outfile[3].c_str(),"READ");
    TGraphErrors * g4 = (TGraphErrors*) f4.Get("Graph");
    TFile f5(outfile[4].c_str(),"READ");
    TGraphErrors * g5 = (TGraphErrors*) f5.Get("Graph");
    TFile f6(outfile[5].c_str(),"READ");
    TGraphErrors * g6 = (TGraphErrors*) f6.Get("Graph");
    TFile f7(outfile[6].c_str(),"READ");
    TGraphErrors * g7 = (TGraphErrors*) f7.Get("Graph");
    TFile f8(outfile[7].c_str(),"READ");
    TGraphErrors * g8 = (TGraphErrors*) f8.Get("Graph");

    TCanvas *c2 = new TCanvas ("c2","c2",800,450);
    g1->SetTitle("IV curve - forward bias");

    g1->GetYaxis()->SetTitle("I [A]");
    g1->GetXaxis()->SetTitle("V [V]");
    g1->GetXaxis()->SetTitleSize(0.05);
    g1->GetXaxis()->SetTitleOffset(0.9);
    g1->GetYaxis()->SetTitleOffset(0.9);
    g1->GetXaxis()->SetLabelSize(0.05);
    g1->GetYaxis()->SetTitleSize(0.05);
    g1->GetYaxis()->SetLabelSize(0.05);

    g1->SetMarkerSize(0.5);
    g2->SetMarkerSize(0.5);
    g3->SetMarkerSize(0.5);
    g4->SetMarkerSize(0.5);
    g5->SetMarkerSize(0.5);
    g6->SetMarkerSize(0.5);
    g7->SetMarkerSize(0.5);
    g8->SetMarkerSize(0.5);

    g1->SetMarkerColor(1);
    g2->SetMarkerColor(2);
    g3->SetMarkerColor(3);
    g4->SetMarkerColor(4);
    g5->SetMarkerColor(6);
    g6->SetMarkerColor(7);
    g7->SetMarkerColor(8);
    g8->SetMarkerColor(9);

    g1->SetMarkerStyle(22);
    g2->SetMarkerStyle(23);
    g3->SetMarkerStyle(24);
    g4->SetMarkerStyle(25);
    g5->SetMarkerStyle(26);
    g6->SetMarkerStyle(27);
    g7->SetMarkerStyle(28);
    g8->SetMarkerStyle(29);

    g1->Draw("ap");
    g2->Draw("samep");
    g3->Draw("samep");
    g4->Draw("samep");
    g5->Draw("samep");
    g6->Draw("samep");
    g7->Draw("samep");
    g8->Draw("samep");

    c2->SetLogy();
    g1->GetYaxis()->SetRangeUser(1e-14,1e-1);
    g1->GetXaxis()->SetRangeUser(0,2); // why does this line not work?

    TLegend * leg = new TLegend(0.6,0.2,0.88,0.5);
        leg->AddEntry(g1,my_file[0].c_str(),"p");
        leg->AddEntry(g2,my_file[1].c_str(),"p");
        leg->AddEntry(g3,my_file[2].c_str(),"p");
        leg->AddEntry(g4,my_file[3].c_str(),"p");
        leg->AddEntry(g5,my_file[4].c_str(),"p");
        leg->AddEntry(g6,my_file[5].c_str(),"p");
        leg->AddEntry(g7,my_file[6].c_str(),"p");
        leg->AddEntry(g8,my_file[7].c_str(),"p");
        leg->Draw();

    c2->SaveAs((my_path+"plots/iv_curves_fwd.pdf").c_str());
}


int run_IV(bool try_fit_IV=0)
{
//    // -40 C
//    Double_t fit_int_IV[2] = {0.3,0.7};
//    Double_t fit_par_IV[NPAR_IV] = {1e-16,1e-16,40};
//    draw_err("IV_curve_-40C_fwd",fit_int_IV,fit_par,try_fit_IV);

    // -60 C
    Double_t fit_int_IV[2] = {0.3,0.7};
    Double_t fit_par_IV[NPAR_IV] = {6e-15,1e-16,40};
    draw_err("IV_curve_-60C_fwd",fit_int_IV,fit_par,try_fit_IV);

    return 0;
}

int run(bool try_fit=0)
{
//    draw("IV_curve_20C_fwd_v2");
//    draw_err("IV_curve_20C_fwd_v2");

    /* Values from Guoqing
     * K = 0.0039
     * Is = 5e-14
     * R = 90
     */

    // ******** //
    // for 20C //
    // ******** //
//    Double_t fit_int[2] = {0.1e-9,1e-2};
//    Double_t fit_par[4] = {0.0321206,1.02663e-11,20.4406};
//    draw_err_inv("IV_curve_20C_fwd_v2",fit_int,fit_par,try_fit);

    // ******** //
    // for -40C //
    // ******** //
    // fit is not so convincing...
//    Double_t fit_int[2] = {0.1e-10,1e-1};
//    Double_t fit_par[4] = {0.025,2e-16,35};
//    draw_err_inv("IV_curve_-40C_fwd",fit_int,fit_par,try_fit);

    // ******** //
    // for -60C //
    // ******** //
//    fit_int[0] = 1e-9;
//    fit_int[1] = 2.5e-2;

    // these parameters look alright in the drawing (don't converge when fitting though)
//    fit_par[0] = 0.038;
//    fit_par[1] = 1.4e-13;
//    fit_par[2] = 30.24;
////    /*fit_par[3] = 0.05;*/
//    // play with these to find any improvement...
////    fit_par[0] = 0.032;
////    fit_par[1] = 1e-11;
////    fit_par[2] = 20.44;
//    draw_err_inv("IV_curve_-60C_fwd_mod",fit_int,fit_par,try_fit);

    // ******** //
    // for -80C //
    // ******** //
    fit_int[0] = 1e-9;
    fit_int[1] = 2.5e-2;
    fit_par[0] = 0.025;
    fit_par[1] = 1e-18;
    fit_par[2] = 60;
    draw_err_inv("IV_curve_-80C_fwd",fit_int,fit_par,try_fit);

//    draw_err("IV_curve_-40C_fwd");

//    draw_err_all();

    return 0;
}

