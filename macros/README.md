*****************************************
*** Required GIT REPOSITORIES:        ***
***    > nexo                         ***
***    > data                         ***
***                                   ***
***   Jens Kroeger, April 2018        ***
***   kroeger@physi.uni-heidelberg.de ***
*****************************************

This file summarized how to use all the macros in
        "[your_path]/nexo/macros"
    and "[your_path]/nexo/macros/IV"

All data must be stored in
        "[your_path]/data"

All output files are stored in
        "[your_path]/data/all_plots"

************************************
*** Dark Noise and Afterpulsing: ***
************************************

    .x plot_DN_AP_final(use_Vov,column,try_fit,draw)

    where use_Vov = 1 --> plot DN/AP vs. Vov
                  = 0 --> plot DN/AP vs. voltage

          column  = 3 --> DN
                  = 5 --> AP

          try_fit = 0 --> plot datapoints w/o fit
                  = 1 --> plot datapoints and fit

          draw    = 0 --> fit datapoints
                  = 1 --> draw fitfunction instead if performing fit

    ------------------------------------------------------------
    For FINAL PLOT use:
    .x plot_DN_AP_final(1,3,0,0) for DN with trendlines (no fit)
    .x plot_DN_AP_final(1,5,0,0) for AP with trendlines (no fit)
    ------------------------------------------------------------

    output files:
    > DarkNoise_Vov.pdf
    > DarkNoise_Vov_fit.pdf
    > NumAfterPulse_1us_Vov.pdf
    > NumAfterPulse_1us_Vov_fit.pdf
    > DarkNoise_Vov_fitparameters.txt
    > NumAfterPulse_1us_Vov_fitparameters.txt


**************************
*** Charge vs. Voltage ***
**************************

    .x plot_gain.C(try_fit)

    where try_fit = 0 --> plot datapoints
                  = 1 --> linear fit (slope = gain // x-intersect = Vbd)

    output files:
    > ChargeVsVoltage.pdf
    > ChargeVsVoltage_fit.pdf
    > ChargeVsVoltage_fitparameters.txt

    and in /data/DarkNoise_AfterPulsing/DATA_DN_AP/gain/:
    > Vbd_vs_Temp.txt   --> needed for plot_Vbd_vs_Temp.C
                            and for plot_C_vs_Temp.C

***********************************
*** Capacitance vs. Temperature ***
***********************************

    .x plot_C_vs_Temp.C(try_fit)

    where try_fit is NOT USED

    -------------------
    For FINAL PLOT use:
    .x plot_C_vs_temp.C

    output file:
    > Capacitance_vs_Temp.pdf

*****************************************
*** Breakdown Voltage vs. Temperature ***
*****************************************

    For FINAL PLOT use:
    .x plot_Vbd_vs_Temp.C

    output files:
    > Breakdown_vs_Temp.pdf


******************
*** IV Reverse ***
******************
This macro is in nexo/macros/IV/

    .x plot_IV_VUV4_final.C(try_fit,draw)

    where try_fit = 0 --> plot datapoints
                  = 1 --> fit with custom function --> not totally convincing yet
          draw    = 0 --> fit datapoints
                  = 1 --> draw fit function instead of performing fit

    output files:
    > IV_reverse_VUV4.pdf
    > IV_reverse_VUV4_fit.pdf
    > IV_reverse_VUV4_fitparameters.txt

******************
*** IV forward ***
******************
This macro is in nexo/macros/IV/

    .x plot_IV_fwd(fit)

    where fit = 0 --> plot datapoints
              = 1 --> fit whole range --> looks crappy (we're using a lin. function here)
              = 2 --> fit only upper 3-4 points --> extract R


    output files:
    > IV_fwd.pdf
    > IV_fwd_fit.pdf
    > IV_fwd_fitparameters.txt
