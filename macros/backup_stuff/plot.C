// -- -- -- -- -- -- -- -- -- -- -- -- -- -- //
// Get nice hitmaps which all look the same! //
// -- -- -- -- -- -- -- -- -- -- -- -- -- -- //

int draw_hitmap(string infile, string outfile)
{
    TCanvas *c1 = new TCanvas("","",800,600);
    TFile f1(infile.c_str(),"READ");
    TH2F *h1 = (TH2F*)f1.Get("pixmap");

    h1->SetTitle("");
    h1->GetXaxis()->SetTitle("physical column address");
    h1->GetYaxis()->SetTitle("physical row address");

    h1->GetXaxis()->SetTitleSize(0.05);
    h1->GetYaxis()->SetTitleSize(0.05);

    h1->GetXaxis()->SetTitleOffset(0.9);
    h1->GetYaxis()->SetTitleOffset(1.0);

    h1->GetXaxis()->SetLabelSize(0.05);
    h1->GetYaxis()->SetLabelSize(0.05);

    h1->GetZaxis()->SetTitle("entries");
    h1->GetZaxis()->SetTitleSize(0.05);
    h1->GetZaxis()->SetLabelSize(0.04);
    h1->GetZaxis()->SetTitleOffset(1.2);
    // Do I really want this?
    h1->GetXaxis()->SetRangeUser(0,127);
    h1->GetYaxis()->SetRangeUser(0,199);
//    h1->GetZaxis()->SetLabelOffset(0.1);
    gStyle->SetOptStat(0);

    h1->Draw("colz");
// fucking colour palette!
//    c1->Update();
//    TPaletteAxis *palette = new TPaletteAxis(282.0059,-0.4999994,297.7752,199.5,h1);
//    palette->SetLabelOffset(0.005);
//    palette->SetLabelSize(0.035);
//    palette->SetTitleOffset(1);
//    palette->SetTitleSize(0.035);
//    palette->SetFillColor(100);
//    palette->SetFillStyle(1001);
//    pixmap->GetListOfFunctions()->Add(palette,"br");

//    c1->Modified();
//    c1->Update();

    c1->SetRightMargin(0.17);
    c1->SaveAs(outfile.c_str());

    delete c1;
    delete h1;

    return 0;
}

int run(){
    string infile = "single_run_000001.blck.root";
    string outfile = "plots/test.pdf";
    draw_hitmap(infile, outfile);
    return 0;
}
