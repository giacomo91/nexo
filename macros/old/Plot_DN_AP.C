
/*

 This macro plots After Pulse/After Pulse 1micro and Dark Noise. The file with the data must be formatted in this way:

 Overvoltage(Or Voltage)    DarkNoise   AfterPulse  AfterPulse1micro
                                        (lower 1us) (lower 1ms)
                                        --> are the same ATM

 --->Function parameters:

 aWhich select the file to analyze from the list in fileName

 aColumn select which column in the file it is necessary to plot

 if aSame=1 leave the Canvas open in order to plot more plots in the same canvas from different files


 Created by
    G.Gallina
    giacomo@triumf.ca

 Modified by
    Jens Kroeger (January 2018)
    kroeger@physi.uni-heidelberg.de

 ________________________________________________________________

 Run like this:

    run(column)

    where column =
        2 -> DarkNoise
        3 -> AfterPulse (within 1 us)
        4 -> AfterPulse (within 1 ms) -- same as 2 at the moment...will change later

________________________________________________________________
 */

#define NFILE 6;
string filePath = "../../data/DarkNoise_AfterPulsing/DATA_DN_AP/Nov2017/";
//#define NFILE 1;
//string filePath = "../../data/DarkNoise_AfterPulsing/DATA_DN_AP/Jan2018/";

// if you add a file here,
// don't forget to increment NFILE!!
string fileName[NFILE] = {"minus_20",
                          "minus_40",
                          "minus_60",
                          "minus_80",
                          "minus_95",
                          "minus_110"
                         };

//_____________________________________________________________________

Double_t my_func(double *x, Double_t *par)
{
    Double_t k = par[0];
    Double_t p = par[1];

    Double_t Vov = x[0];

    Double_t DCR = k*(1-exp(p*Vov));
    return DCR;
}

void Plot_DN_AP(bool use_Vov, int aColumn=1, int try_fit=0){

    if (aColumn<3 || aColumn==4) {

        cout << "\t First column is voltage, second is over-voltage!" << endl;
        cout << "\t Please select another one: " << endl;
        cout << "\t 3rd column: DN" << endl;
        cout << "\t 4th column: AP (INVALID)" << endl;
        cout << "\t 5th column: AP1micro" << endl;

        return;

    }

    //Declare variables
    Int_t nV=100;
    Double_t tV, tVov, tDN, tAP, tAP1micro;

    ifstream in1[NFILE];
    TGraph *g1[NFILE];

    //Loop to import all files
    for(int aWhich=0; aWhich<NFILE; aWhich++)
    {
        in1[aWhich].open((filePath+fileName[aWhich]+".txt").c_str());
        g1[aWhich] = new TGraph();

        cout << "Opening file: " << filePath + fileName[aWhich]<<endl;

        int i=0;
        while(1){

            in1[aWhich]>>tV>>tVov>>tDN>>tAP>>tAP1micro;

            if (!in1[aWhich].good()) break;


            //Dark Noise
            if(aColumn==3)
            {
                if(i==0) cout<<"Plotting Dark Noise..!"<<endl;
                if(!use_Vov){
                    g1[aWhich]->SetPoint(i,tV,tDN);
                    g1[aWhich]->GetXaxis()->SetRangeUser(46,57);
                } else {
                    g1[aWhich]->SetPoint(i,tVov,tDN);
                }
            }
            //After Pulse
            else if(aColumn==4)
            {
                if(i==0) cout<<"Plotting After Pulse..!"<<endl;
                if(!use_Vov){
                    g1[aWhich]->SetPoint(i,tV,tAP);
                } else {
                    g1[aWhich]->SetPoint(i,tVov,tAP);
                }
            }
            //After Pulse 1 micro
            else if(aColumn==5)
            {
                if(i==0) cout<<"Plotting After Pulse 1 micro...!"<<endl;
                if(!use_Vov){
                    g1[aWhich]->SetPoint(i,tV,tAP1micro);
                } else {
                    g1[aWhich]->SetPoint(i,tVov,tAP1micro);
                }
            }
            else
            {
                cout << "ERROR: Column " << aColumn << " does not exist!" << endl;
                return;
            }

            cout<<"tV: "<<tV<< " tVov: "<< tVov <<" tDN: "<<tDN<<" tAP: "<<tAP<<" tAP1micro: "<<tAP1micro<<endl;
            i++;
        } // end while(1)

        if(aWhich==0) // first plot
        {
            TCanvas *CDplot = new TCanvas("c1","",10,10,1000,600);

            CDplot->SetBorderMode(0);
            CDplot->SetFillColor(0);
            CDplot->SetLeftMargin(0.12);
            CDplot->SetRightMargin(0.04);
            CDplot->SetBottomMargin(0.1);
            CDplot->SetTopMargin(0.03);
            CDplot->Draw();

            if(!use_Vov){
                g1[aWhich]->GetXaxis()->SetTitle("Voltage[V]");
            } else {
                g1[aWhich]->GetXaxis()->SetTitle("Overvoltage[V]");
            }
            g1[aWhich]->GetXaxis()->CenterTitle();
            g1[aWhich]->GetXaxis()->SetTitleOffset(1.2);
            g1[aWhich]->SetTitle("Dark Noise Rate vs. Overvoltage");

            cout << "aColumn = " << aColumn << endl;

            if(aColumn==3)
            {
                g1[aWhich]->SetTitle("Dark Noise");
                g1[aWhich]->GetYaxis()->SetTitle("Rate [Hz/mm^{2}]");
                g1[aWhich]->GetYaxis()->SetRangeUser(0.01,1e5);
                CDplot->SetLogy();
            }
            if(aColumn==4)
            {
                g1[aWhich]->GetYaxis()->SetTitle("Probability [%] ???");
                g1[aWhich]->GetYaxis()->SetRangeUser(0,1.4);
            }
            if(aColumn==5)
            {
                g1[aWhich]->GetYaxis()->SetTitle("Probability [%] ???");
                g1[aWhich]->GetYaxis()->SetRangeUser(0,1.4);
            }

            g1[aWhich]->GetYaxis()->CenterTitle();
            g1[aWhich]->GetYaxis()->SetTitleOffset(1.5);
            g1[aWhich]->SetMarkerStyle(22);
            g1[aWhich]->SetTitle("");

            g1[aWhich]->Draw("ap");
            TLegend * leg = new TLegend(0.2,0.8,0.4,0.95);

        } // end if(aWhich==0)

        else // n-th plot (n>1)
        {
            g1[aWhich]->SetMarkerStyle(22+aWhich);

            // avoid yellow
            if(aWhich<4)
                g1[aWhich]->SetMarkerColor(1+aWhich);
            else
                g1[aWhich]->SetMarkerColor(2+aWhich);

            g1[aWhich]->Draw("samep");
        }

        // add fit
        if(try_fit == 1 && aColumn==3)
        {
            cout << " *** *** *** *** ** *** " << endl;
            cout << " *** TRY LINEAR FIT *** " << endl;
            cout << " *** *** *** *** ** *** " << endl;

            g1[aWhich]->Fit("pol1","","");
            TF1 *f1 = g1[aWhich]->GetFunction("pol1");
            if(aWhich<4) f1->SetLineColor(1+aWhich); // avoid yellow
            else         f1->SetLineColor(2+aWhich);

//            // obtain fit results
//            f1 = g1->GetFunction("pol2")
//            f1->SetLineWidth(1);

//            gStyle->SetOptFit();
        }
        else if(try_fit == 2 && aColumn==3)
        {
            cout << " *** *** *** *** * *** " << endl;
            cout << " *** TRY 1-EXP FIT *** " << endl;
            cout << " *** *** *** *** * *** " << endl;

            Double_t fit_int[2] = {1,10};
            Double_t fit_par[2];

            if (aWhich==0) {        // -20C
                fit_par[0] = 2e4;
                fit_par[1] = 0.01;
            } else if (aWhich==1) { // -40C
                fit_par[0] = 3e4;
                fit_par[1] = 0.01;
            } else if (aWhich==2) { // -60C
                fit_par[0] = 3e4;
                fit_par[1] = 0.01;
            } else if (aWhich==3) { // -80C
                fit_par[0] = 3e4;
                fit_par[1] = 0.01;
            } else if (aWhich==4) { // -95C
                fit_par[0] = 3e4;
                fit_par[1] = 0.01;
            } else if (aWhich==5) { // -110C
                fit_par[0] = 3e4;
                fit_par[1] = 0.01;
            } else {
                cout << "aWhich == " << aWhich << " is out of range!" << endl;
            }

            TF1 *func1 = new TF1("func1",my_func, fit_int[0],fit_int[1],2);
            for(int i=0; i<2; i++)
                func1->SetParameter(i,fit_par[i]);

            func1->SetParName(0,"k");
            func1->SetParName(1,"p");

            if(aWhich<4) func1->SetLineColor(1+aWhich); // avoid yellow
            else         func1->SetLineColor(2+aWhich);

            g1[aWhich]->Fit("func1","R");
            func1 = g1[aWhich]->GetFunction("func1");
        }


        // add entry to legend:
        leg->AddEntry(g1[aWhich],(fileName[aWhich]).c_str(),"p");
        leg->Draw();

    } // end for(aWhich)

    // save to file
    if(aColumn == 3) {
        if(!use_Vov){
            CDplot->SaveAs((filePath+"plots/DarkNoise.pdf").c_str());
        } else {
            CDplot->SaveAs((filePath+"plots/DarkNoise_Vov.pdf").c_str());
        }
    }
    else if(aColumn == 4) {
        if(!use_Vov){
            CDplot->SaveAs((filePath+"plots/AfterPulse_1ms.pdf").c_str());
        } else {
            CDplot->SaveAs((filePath+"plots/AfterPulse_1ms_Vov.pdf").c_str());
        }
    }
    else if(aColumn == 5) {
        if(!use_Vov) {
            CDplot->SaveAs((filePath+"plots/AfterPulse_1us.pdf").c_str());
        } else {
            CDplot->SaveAs((filePath+"plots/AfterPulse_1us_Vov.pdf").c_str());
        }
    }
    else
        cout << "ERROR: column " << aColumn << " does not exist!" << endl;
}

int run(bool use_Vov=0, int column = 3, int try_fit =0)
{
    /*
     * try_fit = 0 --> no fit
     *         = 1 --> linear fit
     *         = 2 --> 1-exp fit as Guoqing suggested
     */

    Plot_DN_AP(use_Vov,column,try_fit);

//    Plot_DN_AP(3);
//    Plot_DN_AP(4);
//    Plot_DN_AP(5);

//    system("evince ../../data/DarkNoise.pdf &");
//    system("evince ../../data/AfterPulse_1us.pdf &");
//    system("evince ../../data/AfterPulse_1ms.pdf &");

    return 0;
}
