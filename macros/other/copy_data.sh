#####################################
#           Jens Kroeger            #
#  kroeger@physi.uni-heidelberg.de  #
#           January 2018            #
#####################################

# This script copies the data from an IV scan to your local machine

# Make sure you've got a directory
# [path_to_dir]/nexo/macros/../../data/IV_curves
#

path=../../data/IV_curves
echo "Path to data:" $path

echo "Save to subdirectory? [y/n]"
read answ1
if [ "$answ1" = "y" ]; then
    echo "Enter name of subdirectory:"
    read subdir
    if [ -d "$path/$subdir" ]; then
        echo "$subdir exisits already. Continue? [y/n]"
        read answ1
        if [ "$answ1" = "n" ]; then
            mkdir $path/$subdir
            mkdir $path/$subdir/plots
            echo "Created $path/$subdir and $path/$subdir/plots"
        fi
    fi
fi

# Copy file to your local machine:
# passwd: nexo_daq
echo "Enter passw: nexo_daq"
scp exo@142.90.111.172:/home/exo/online/scpico/IV_curve.txt $path

echo "Enter temperature: [e.g. 20C]"
read temp

new_name=IV_curve_$temp.csv
echo $new_name
#Check if such name exists already:
if [ -f $path/$subdir/$new_name ]; then
    echo "File exists! Continue and overwrite? [y/n]"
    read answ2

    if [ "$answ2" = "n" ]; then
        while [ -f $path$subdir/$new_name ]; do
        echo "Re-enter new temp or extension:"
        read temp
        new_name=IV_curve_$temp.csv
        done
    fi
fi

# Rename file:
echo "Renaming file to" $new_name
mv $path/IV_curve.txt $path/$subdir/$new_name
# Note: I'm using .csv here so that ROOT can read the file in easier
# because here the delimiter is automatically set to be a comma!

echo -e "\n############# \n############# \n### Done. ### \n############# \n#############"
