
/*

 This macro plots After Pulse/After Pulse 1micro and Dark Noise.
 The file with the data must be formatted in this way:

 COLUMN     VALUE
 ------------------
 1          voltage
 2          overvoltage
 3          DarkNoise Rate
 4          Error(DarkNoise)
 5          AfterPulsing Probability
 6          Error(AfterPulsing)

 --->Function parameters:

 aWhich select the file to analyze from the list in fileName

 aColumn select which column in the file it is necessary to plot

 if aSame=1 leave the Canvas open in order to plot more plots in the same canvas from different files


 Created by
    G.Gallina
    giacomo@triumf.ca

 Modified by
    Jens Kroeger (March 2018)
    kroeger@physi.uni-heidelberg.de
________________________________________________________________
 */

#define NFILE 5;
string filePath = "../../data/DarkNoise_AfterPulsing/DATA_DN_AP/Jan2018/";

// if you add a file here,
// don't forget to increment NFILE!!
string fileName[NFILE] = {
                          "minus_40",
                          "minus_60",
                          "minus_80",
                          "minus_95",
                          "minus_110"
                         };

vector<string> temp;
vector<int> temp_int;

//_____________________________________________________________________

Double_t DN_new(double *x, Double_t *par)
{
    // 2018-03-27
    // this should be the correct function
    Double_t Re  = par[0];
    Double_t ke  = par[1];
    Double_t ke2 = par[2];
    Double_t Rh  = par[3];
    Double_t k   = par[4];

    Double_t Vov = x[0];

    Double_t Pe = 1-exp(-ke*exp(-ke2/Vov));
    Double_t Ph = 1-(1-Pe)**(k*Vov);

    Double_t DN = Re*Pe + Re*Ph;
    return DN;
}

Double_t AP_exp4(double *x, Double_t *par)
{
    // 28-03-28 // THIS SHOULD BE CORRECT
    Double_t APe0 = par[0];
    Double_t ke  = par[1];
    Double_t ke2 = par[2];
    Double_t APh0 = par[3];
    Double_t kh  = par[4];
    Double_t kh2 = par[5];

    Double_t Vov = x[0];

    Double_t AlphaEDeltaS = ke*exp(-ke2/Vov);
    Double_t P1e = 1-exp(-AlphaEDeltaS);
    Double_t P2e = P1e-AlphaEDeltaS*exp(-AlphaEDeltaS);
    Double_t P3e = P2e-AlphaEDeltaS**2 * exp(-AlphaEDeltaS)/2;
    Double_t P4e = P3e-AlphaEDeltaS**3 * exp(-AlphaEDeltaS)/6;
    Double_t P5e = P4e-AlphaEDeltaS**4 * exp(-AlphaEDeltaS)/24;
    Double_t P6e = P5e-AlphaEDeltaS**5 * exp(-AlphaEDeltaS)/120;
    Double_t P7e = P6e-AlphaEDeltaS**6 * exp(-AlphaEDeltaS)/720;
    Double_t P8e = P7e-AlphaEDeltaS**7 * exp(-AlphaEDeltaS)/5040;
    Double_t P9e = P8e-AlphaEDeltaS**8 * exp(-AlphaEDeltaS)/40320;
    Double_t P10e = P9e-AlphaEDeltaS**9 * exp(-AlphaEDeltaS)/362880;

    Double_t AlphaHDeltaS = kh*exp(-kh2/Vov);
    Double_t P1 = 1-exp(-AlphaHDeltaS);
    Double_t P2 = P1-AlphaHDeltaS*exp(-AlphaHDeltaS);
    Double_t P3 = P2-AlphaHDeltaS**2 * exp(-AlphaHDeltaS)/2;
    Double_t P4 = P3-AlphaHDeltaS**3 * exp(-AlphaHDeltaS)/6;
    Double_t P5 = P4-AlphaHDeltaS**4 * exp(-AlphaHDeltaS)/24;
    Double_t P6 = P5-AlphaHDeltaS**5 * exp(-AlphaHDeltaS)/120;
    Double_t P7 = P6-AlphaHDeltaS**6 * exp(-AlphaHDeltaS)/720;
    Double_t P8 = P7-AlphaHDeltaS**7 * exp(-AlphaHDeltaS)/5040;
    Double_t P9 = P8-AlphaHDeltaS**8 * exp(-AlphaHDeltaS)/40320;
    Double_t P10 = P9-AlphaHDeltaS**9 * exp(-AlphaHDeltaS)/362880;

    Double_t AP =
            APe0*Vov*(
                  (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)
                + (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**2
                + (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**3
                + (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**4
                + (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**5
                + (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**6
                + (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**7
                + (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**8
                + (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**9
                + (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**10)
             + APh0*Vov*(
                  (P1+P2+P3+P4+P5+P6+P7+P8+P9+P10)
                + (P1+P2+P3+P4+P5+P6+P7+P8+P9+P10)**2
                + (P1+P2+P3+P4+P5+P6+P7+P8+P9+P10)**3
                + (P1+P2+P3+P4+P5+P6+P7+P8+P9+P10)**4
                + (P1+P2+P3+P4+P5+P6+P7+P8+P9+P10)**5
                + (P1+P2+P3+P4+P5+P6+P7+P8+P9+P10)**6
                + (P1+P2+P3+P4+P5+P6+P7+P8+P9+P10)**7
                + (P1+P2+P3+P4+P5+P6+P7+P8+P9+P10)**8
                + (P1+P2+P3+P4+P5+P6+P7+P8+P9+P10)**9
                + (P1+P2+P3+P4+P5+P6+P7+P8+P9+P10)**10
                );
    return AP;
}

void plot_DN_AP_new(bool use_Vov=1, int aColumn=3, int try_fit=0, int draw=0){
//    temp.push_back("-20 #circC"); // ignore the -20C --> they are shit
    temp.push_back("-40 #circC");
    temp.push_back("-60 #circC");
    temp.push_back("-80 #circC");
    temp.push_back("-95 #circC");
    temp.push_back("-110 #circC");
    temp_int.push_back(-40);
    temp_int.push_back(-60);
    temp_int.push_back(-80);
    temp_int.push_back(-95);
    temp_int.push_back(-110);

    cout << "*********************************************************\n"
            "*** HOW TO RUN:                                       ***\n"
            "*** .x Plot_DN_AP_err2.C(use_Vov,column,try_fit,draw) ***\n"
            "*** where column = 3 --> Dark Noise                   ***\n"
            "***       column = 5 --> After Pulsing                ***\n"
            "*********************************************************\n";

    if (aColumn!=3 && aColumn!=5)
    {
        cout << "\t ERROR:::try different column!" << endl;
        return;
    }

    //Declare variables
    Int_t nV=100;
    Double_t tV, tVov, tVov_err, tDN, tDN_err, tAP, tAP_err;

    ifstream in1[NFILE];
    TGraphErrors *g1[NFILE];

    FILE *myfile,*rates,*rates2;
    if(aColumn==3){
        myfile = fopen("/home/jens/Documents/data/all_plots/DarkNoise_Vov_fitparameters.txt","w");
        rates  = fopen("/home/jens/Documents/data/all_plots/temp/DN_rates.txt","w");
    }
    else {
        rates2 = fopen("/home/jens/Documents/data/all_plots/temp/AP_rates.txt","w");
        myfile = fopen("/home/jens/Documents/data/all_plots/NumAfterPulse_1us_Vov_fitparameters.txt","w");
    }

    //Loop to import all files
    for(int aWhich=0; aWhich<NFILE; aWhich++)
    {
        in1[aWhich].open((filePath+fileName[aWhich]+".txt").c_str());
        g1[aWhich] = new TGraphErrors();

//        cout << "Opening file: " << filePath + fileName[aWhich]<<endl;

        int i=0;
        while(1){

            in1[aWhich]>>tV>>tVov>>tVov_err>>tDN>>tDN_err>>tAP>>tAP_err;

            if (!in1[aWhich].good()) break;

            //Dark Noise
            if(aColumn==3)
            {
//                if(i==0) cout<<"Plotting Dark Noise..!"<<endl;
                if(!use_Vov){
                    g1[aWhich]->SetPoint(i,tV,tDN);
                    g1[aWhich]->SetPointError(i,0,tDN_err);
                } else {
                    g1[aWhich]->SetPoint(i,tVov,tDN);
                    g1[aWhich]->SetPointError(i,tVov_err,tDN_err);
                }
            } // if(aColumn==3)
            //Average After Pulses in 1us
            else if(aColumn==5)
            {
//                if(i==0) cout<<"Plotting Average # After Pulses in 1us...!"<<endl;
                if(!use_Vov){
                    g1[aWhich]->SetPoint(i,tV,tAP);
                    g1[aWhich]->SetPointError(i,0,tAP_err);
                } else {
                    g1[aWhich]->SetPoint(i,tVov,tAP);
                    g1[aWhich]->SetPointError(i,tVov_err,tAP_err);
                }
            } // if(aColumn==5)
            else
            {
                cout << "ERROR: Column " << aColumn << " cannot be plotted!" << endl;
                return;
            }
            i++;
        } // end while(1)

        gStyle->SetStatFont(132);
        gStyle->SetTitleFont(132); // has no effect

        g1[aWhich]->GetXaxis()->SetTitleOffset(1.0);
        g1[aWhich]->GetYaxis()->SetTitleOffset(1.0);

        g1[aWhich]->GetYaxis()->CenterTitle();
        g1[aWhich]->GetXaxis()->CenterTitle();
        g1[aWhich]->SetMarkerColor(kBlack);
        g1[aWhich]->SetFillColorAlpha(kBlue,0.1);
        g1[aWhich]->SetFillStyle(4050);

        g1[aWhich]->GetXaxis()->SetLabelFont(132); // times not bold
        g1[aWhich]->GetYaxis()->SetLabelFont(132);
        g1[aWhich]->GetXaxis()->SetTitleFont(132);
        g1[aWhich]->GetYaxis()->SetTitleFont(132);
        g1[aWhich]->GetXaxis()->SetTitleSize(0.0425);
        g1[aWhich]->GetYaxis()->SetTitleSize(0.0425);

        g1[aWhich]->SetMarkerStyle(20);
        g1[aWhich]->SetMarkerSize(0.5);

        if(aWhich==0) // first plot
        {
            TCanvas *CDplot = new TCanvas("c1","c1"/*,10,10*/,1000,600);

            CDplot->SetGrid();
            CDplot->SetBorderMode(0);
            CDplot->SetFillColor(0);
            CDplot->SetLeftMargin(0.12);
            CDplot->SetRightMargin(0.04);
            CDplot->SetBottomMargin(0.1);
            CDplot->Draw();

            if(!use_Vov){
                g1[aWhich]->GetXaxis()->SetTitle("Voltage [V]");
                g1[aWhich]->GetXaxis()->SetLimits(46,58);
            } else {
                g1[aWhich]->GetXaxis()->SetTitle("Overvoltage [V]");
                g1[aWhich]->GetXaxis()->SetLimits(0,9);
            }
            g1[aWhich]->GetXaxis()->CenterTitle();

            cout << "aColumn = " << aColumn << endl;

            if(aColumn==3)
            {
                g1[aWhich]->GetYaxis()->SetTitle("Dark Noise Rate [Hz/mm^{2}]");
                g1[aWhich]->GetYaxis()->SetRangeUser(0.01,1e5);
                g1[aWhich]->Draw("ap");
                CDplot->SetLogy();
                TLegend * leg = new TLegend(0.175,0.7,0.375,0.9);
            }
            if(aColumn==5)
            {
                g1[aWhich]->GetYaxis()->SetTitle("Average # Afterpulses in 1us");
                g1[aWhich]->GetYaxis()->SetRangeUser(0,1.4);

                if(try_fit==0) g1[aWhich]->Draw("alp");
                else           g1[aWhich]->Draw("ap");
                TLegend * leg = new TLegend(0.25,0.65,0.45,0.9);
            }

        } // end if(aWhich==0)

        else // n-th plot (n>1)
        {
            continue;
            g1[aWhich]->SetLineColor(22+aWhich);

            // avoid yellow
            if(aWhich<4) {
                g1[aWhich]->SetMarkerColor(1+aWhich);
                g1[aWhich]->SetLineColor(1+aWhich);
            }
            else {
                g1[aWhich]->SetMarkerColor(2+aWhich);
                g1[aWhich]->SetLineColor(2+aWhich);
            }

            if(aColumn==5 && try_fit==0)
                g1[aWhich]->Draw("samelp");
            else
                g1[aWhich]->Draw("samep");
        } // n-th plot

////////////////////////////////
/// BEGIN FITTING DARK NOISE ///
////////////////////////////////

        if(try_fit==1 && aColumn==3)
        {
            cout << endl;
            cout << " *** *** *** *** * ***  ***" << endl;
            cout << " *** FITTING Dark Noise at " << temp[aWhich] << endl;
            cout << " *** *** *** *** * ***  ***" << endl;
            if(aWhich==0){
                fprintf(myfile,"FIT: DN = new \n\n"
                    "T [C]\t"
                    "Re\t\tke\t\tke2\t\t"
                    "Rh\t\tkh\t\tkh2\t"
                    "\tChi^2/NDF\n"
                    "\tRe_err\t\tke_err\t\tke2_err\t\tRh_err\t\tkh_err\t\tkh2_err\n"
                    "-----------------------------------"
                    "------------------------"
                    "-----------------------------------------------------\n");
            }
            Int_t NumCONST = 5;
            Double_t fit_int[2];
            ROOT::Math::MinimizerOptions::SetDefaultMaxFunctionCalls(1e7);

                /* * * * * * * * * * * * * * *
                 * NEW APPROACH: 1-exp(...)  *
                 * * * * * * * * * * * * * * */
                fit_int[0] = 2;
                fit_int[1] = 8;
                 TF1 *func1 = new TF1("func1",DN_new, fit_int[0],fit_int[1],NumCONST);

                double Re, Rh, ke, ke2, k;
                if(aWhich==0) { // -40C
                    Re  = 0.2e7;
                    Rh  = 0.8e3;
//                    ke  = 4.07;
//                    ke2 = 2.351;
//                    ke = 0.438;
//                    ke2 = 1.54;
                    ke  = 3.87983e-05;
                    ke2 = 1.65896e-01;
                    k = 0.1;
                }
                else continue;
//                if(aWhich==1) { // -60C
//                    Re  = 1e2;
//                    Rh  = 1e2;
//                    ke  = 4.38314e-01;
//                    ke2 = 1.54353e+00;
//                    kh  = 1.71159e+00;
//                    kh2 = 6.61284e+00;
//                }
//                else if(aWhich==2) { // -80C
//                    Re  = 1e1;
//                    Rh  = 1e1;
//                    ke  = 4.38314e-01;
//                    ke2 = 1.54353e+00;
//                    kh  = 2.14554e+00;
//                    kh2 = 9.16645e+00;
//                }
//                else if(aWhich==3) { // -95C
//                    Re  = 1e-1;
//                    Rh  = 1e-1;
//                    ke  = 4.38314e-01;
//                    ke2 = 1.54353e+00;
//                    kh  = 2.47294e+00;
//                    kh2 = 1.01884e+01;
//                }
//                else if(aWhich==4) { // -110C
//                    Re  = 1e-1;
//                    Rh  = 1e-1;
//                    ke  = 4.38314e-01;
//                    ke2 = 1.54353e+00;
//                    kh  = 4.31197e+00;
//                    kh2 = 1.37075e+01;
//                }
//                else break;

//                func1->SetParLimits(1,2/3*ke,2*ke);
//                func1->SetParLimits(4,2/3*kh,2*kh);
//                func1->SetParLimits(2,2/3*ke2,2*ke2);
//                func1->SetParLimits(5,2/3*kh2,2*kh2);
                func1->SetParLimits(0,0,1e9);
//                func1->SetParLimits(3,0,1e9);
                func1->SetParLimits(1,ke-4.59561e-05,ke+4.59561e-05);
//                func1->SetParLimits(2,ke2-2*8.60713e-02,ke2+2*8.60713e-02);
                func1->SetParLimits(4,0,10000);

                func1->SetParameter(0,Re);     // Re
                func1->FixParameter(3,Rh);     // Rh
                func1->SetParameter(1,ke);  // ke  --> fixed
                func1->SetParameter(2,ke2);   // ke2 --> fixed
                func1->FixParameter(4,/*k*/1);

                func1->SetParName(0,"Re");
                func1->SetParName(1,"ke");
                func1->SetParName(2,"ke2");
                func1->SetParName(3,"Rh");
                func1->SetParName(4,"k");

                if(aWhich<4) func1->SetLineColor(1+aWhich); // avoid yellow
                else         func1->SetLineColor(2+aWhich);

                if(draw==1)
                    func1->Draw("same");
                else {
                    g1[aWhich]->Fit("func1","R");
                    func1 = g1[aWhich]->GetFunction("func1");
                }
                cout << "Chi^2/NDF = " << func1->GetChisquare() << "/"
                     << func1->GetNDF() << endl
                     << "Re/Rh = "
                     << func1->GetParameter(0)/func1->GetParameter(3) << endl
                     << "Rh/Re = "
                     << func1->GetParameter(3)/func1->GetParameter(0)
                     << endl << endl;


//                fprintf(myfile,"%i",temp_int[aWhich]);
//                for(int j=0;j<NumCONST;j++)
//                    fprintf(myfile,"\t%.2e",func1->GetParameter(j));
//                fprintf(myfile,"\n");
//                for(int k=0;k<NumCONST;k++)
//                    fprintf(myfile,"\t%.2e",func1->GetParError(k));

//                fprintf(myfile,"\t%.2f/%i\n\n",func1->GetChisquare(),func1->GetNDF());

//                if(aWhich==0)
//                    fprintf(rates,"#T[C]\tRe\t\tRe_err\t\tRh\t\tRh_err\n");
//                fprintf(rates,"%i\t%.2e\t%.2e\t%.2e\t%.2e\n",
//                        temp_int[aWhich],
//                        func1->GetParameter(0),
//                        func1->GetParError(0),
//                        func1->GetParameter(3),
//                        func1->GetParError(3));

        } // end if(try_fit)
//////////////////////////////
/// END FITTING DARK NOISE ///
//////////////////////////////

//////////////////////////////////
/// BEGIN FITTING AFTERPULSING ///
//////////////////////////////////
//        if(try_fit!=0 && aColumn==5)
//        {
//            cout << endl;
//            cout << " *** *** *** *** ***  ***  *** *** *** ***" << endl;
//            cout << " *** FITTING After Pulsing at " << temp_int[aWhich] << " C ***" << endl;
//            cout << " *** *** *** *** ***  ***  *** *** *** ***" << endl;
//                if(aWhich==0){
//                fprintf(myfile,"FIT:"
//                       " APe0*Vov*(\n"
//                       "\t   (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)\n"
//                       "\t+ (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**2\n"
//                       "\t+ (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**3\n"
//                       "\t+ (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**4\n"
//                       "\t+ (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**5\n"
//                       "\t+ (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**6\n"
//                       "\t+ (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**7\n"
//                       "\t+ (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**8\n"
//                       "\t+ (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**9\n"
//                       "\t+ (P1e+P2e+P3e+P4e+P5e+P6e+P7e+P8e+P9e+P10e)**10)\n"
//                        "    + APh0*Vov*(\n"
//                        "\t   (P1h+P2h+P3h+P4h+P5h+P6h+P7h+P8+P9h+P10h)\n"
//                        "\t+ (P1h+P2h+P3h+P4h+P5h+P6h+P7h+P8h+P9+P10h)**2\n"
//                        "\t+ (P1h+P2h+P3h+P4h+P5h+P6h+P7h+P8h+P9+P10h)**3\n"
//                        "\t+ (P1h+P2h+P3h+P4h+P5h+P6h+P7h+P8h+P9+P10h)**4\n"
//                        "\t+ (P1h+P2h+P3h+P4h+P5h+P6h+P7h+P8h+P9+P10h)**5\n"
//                        "\t+ (P1h+P2h+P3h+P4h+P5h+P6h+P7h+P8h+P9+P10h)**6\n"
//                        "\t+ (P1h+P2h+P3h+P4h+P5h+P6h+P7h+P8h+P9+P10h)**7\n"
//                        "\t+ (P1h+P2h+P3h+P4h+P5h+P6h+P7h+P8h+P9+P10h)**8\n"
//                        "\t+ (P1h+P2h+P3h+P4h+P5h+P6h+P7h+P8h+P9+P10h)**9\n"
//                        "\t+ (P1h+P2h+P3h+P4h+P5h+P6h+P7h+P8h+P9+P10h)**10)\n\n"

//                        "T [C]\t"
//                        "APe0\t\tke\t\tke2\t\t"
//                        "APh0\t\tkh\t\tkh2\t"
//                        "\tChi^2/NDF\n"
//                               "\tAPe0_err\tke_err\t\tke2_err\t"
//                               "\tAPh0_err\tkh_err\t\tkh2_err\n"
//                        "---------------------------------------------"
//                        "---------------------------------------------"
//                        "-----------------------\n");
//                }// aWhich==0 --> print function

//            g1[aWhich]->SetTitle("");

//            Double_t fit_int[2] = {1,7};

//            ROOT::Math::MinimizerOptions::SetDefaultMaxFunctionCalls(1e5);

//            if(try_fit==1) {
//                double APe0, APh0, ke, ke2, kh, kh2;
//                TF1 *func1 = new TF1("func1",AP_exp4, fit_int[0],fit_int[1],6);
//                if(aWhich==0) { // -40C
//                    APe0 = 5.79174e-02;
//                    APh0 = 3.99963e-02;
//                    ke  = 4.38314e-01;
//                    ke2 = 1.54353e+00;
//                    kh  = 1.58e+00;
////                    kh2 = 7.14e+00;
//                    kh2 = 1.43e+01;

//                }
//                else if(aWhich==1) { // -60C
//                    APe0 = 5.79174e-02;
//                    APh0 = 3.99963e-02;
//                    ke  = 4.38314e-01;
//                    ke2 = 1.54353e+00;
//                    kh  = 1.71e+00;
////                    kh2 = 6.61e+00;
//                    kh2 = 1.16e+01;
//                }
//                else if(aWhich==2) { // -80C
//                    APe0 = 5.79174e-02;
//                    APh0 = 3.99963e-02;
//                    ke  = 4.38314e-01;
//                    ke2 = 1.54353e+00;
//                    kh  = 2.15e+00;
////                    kh2 = 9.17e+00;
//                    kh2 = 1.44e+01;
//                }
//                else if(aWhich==3) { // -95C
//                    APe0 = 5.79174e-02;
//                    APh0 = 3.99963e-02;
//                    ke  = 4.38314e-01;
//                    ke2 = 1.54353e+00;
//                    kh  = 2.47e+00;
////                    kh2 = 1.02e+01;
//                    kh2 = 1.69e+01;
//                }
//                else if(aWhich==4) { // -110C
//                    APe0 = 5.79174e-02;
//                    APh0 = 3.99963e-02;
//                    ke  = 4.38314e-01;
//                    ke2 = 1.54353e+00;
//                    kh  = 4.31e+00;
////                    kh2 = 1.37e+01;
//                    kh2 = 1.87e+01;
//                }
//            } // end try_fit==1

////            func1->SetParLimits(0,0,1e8);
//            func1->SetParLimits(1,2/3*ke,2*ke);
//            func1->SetParLimits(4,2/3*kh,2*kh);
//            func1->SetParLimits(2,2/3*ke2,2*ke2);
//            func1->SetParLimits(5,2/3*kh2,2*kh2);

//            func1->SetParameter(0,APe0);     // APe0
//            func1->FixParameter(1,ke);     // ke  --> fixed
//            func1->FixParameter(2,ke2);     // ke2 --> fixed
//            func1->SetParameter(3,APh0);     // APh0
//            func1->FixParameter(4,kh);     // kh
//            func1->FixParameter(5,kh2);              // kh2

//            func1->SetParName(0,"APe0");
//            func1->SetParName(1,"ke");
//            func1->SetParName(2,"ke2");
//            func1->SetParName(3,"APh0");
//            func1->SetParName(4,"kh");
//            func1->SetParName(5,"kh2");

//            g1[aWhich]->Fit("func1","R");
//            TF1 *func1 = g1[aWhich]->GetFunction("func1");
//            if(aWhich<4) func1->SetLineColor(1+aWhich); // avoid yellow
//            else         func1->SetLineColor(2+aWhich);

////            func1->SetLineColor(kOrange);
////            func1->Draw("same");

//            cout << "  Chi^2/NDF = " << func1->GetChisquare() << "/"
//                 << func1->GetNDF() << endl
//                 << "APe0/APh0 = "
//                 << func1->GetParameter(0)/func1->GetParameter(3) << endl
//                 << "APh0/APe0 = "
//                 << func1->GetParameter(3)/func1->GetParameter(0)
//                 << endl << endl;

//            fprintf(myfile,"%i",temp_int[aWhich]);
//            for(int j=0;j<NumCONST;j++)
//                fprintf(myfile,"\t%.2e",func1->GetParameter(j));
//            fprintf(myfile,"\n");
//            for(int k=0;k<NumCONST;k++)
//                fprintf(myfile,"\t%.2e",func1->GetParError(k));

//            fprintf(myfile,"\t%.2f/%i\n\n",func1->GetChisquare(),func1->GetNDF());

//            g1[0]->GetXaxis()->SetLimits(0,7);

//            if(aWhich==0)
//                fprintf(rates2,"#T[C]\tRe\t\tRe_err\t\tRh\t\tRh_err\n");
//            fprintf(rates2,"%i\t%.2e\t%.2e\t%.2e\t%.2e\n",
//                    temp_int[aWhich],
//                    func1->GetParameter(0),
//                    func1->GetParError(0),
//                    func1->GetParameter(3),
//                    func1->GetParError(3));

//        } // end "new" fit for afterpulsing
////////////////////////////////
/// END FITTING AFTERPULSING ///
////////////////////////////////
        // for all fits:
        gStyle->SetOptFit(0);

        // add entry to legend:
        leg->AddEntry(g1[aWhich],(temp[aWhich]).c_str(),"p");
        leg->SetTextFont(132);
        leg->Draw();

    } // end for(aWhich)
    fclose(myfile);
    if(aColumn==3)
        fclose(rates);
    if(aColumn==5)
        fclose(rates2);

    // save to file
    if(aColumn == 3)
    {
        if(!use_Vov){
            if(try_fit==0)
                CDplot->SaveAs("/home/jens/Documents/data/all_plots/DarkNoise_TEST.pdf");
            else
                CDplot->SaveAs("/home/jens/Documents/data/all_plots/DarkNoise_fit_TEST.pdf");
        } else {
            if(try_fit==0)
                CDplot->SaveAs("/home/jens/Documents/data/all_plots/DarkNoise_Vov_TEST.pdf");
            else
                CDplot->SaveAs("/home/jens/Documents/data/all_plots/DarkNoise_Vov_fit_TEST.pdf");
        }
    }
//    else if(aColumn == 5)
//    {
//        if(!use_Vov){
//            if(try_fit==0)
//                CDplot->SaveAs("/home/jens/Documents/data/all_plots/NumAfterPulse_1us.pdf");
//            else
//                CDplot->SaveAs("/home/jens/Documents/data/all_plots/NumAfterPulse_1us_fit.pdf");
//        } else {
//            if(try_fit==0)
//                CDplot->SaveAs("/home/jens/Documents/data/all_plots/NumAfterPulse_1us_Vov.pdf");
//            else
//                CDplot->SaveAs("/home/jens/Documents/data/all_plots/NumAfterPulse_1us_Vov_fit.pdf");
//        }
//    }
    else
        cout << "ERROR: column " << aColumn << " does not exist!" << endl;

} // end void Plot_DN_AP_new();
